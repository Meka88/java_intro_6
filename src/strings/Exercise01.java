package strings;

public class Exercise01 {
    public static void main(String[] args) {
        /*
        string is a reference type (object) that is used to store a sequence of characters
        texts
         */

        String name = "Jonh";
        String address = "Brooklyn, NY";
        System.out.println(name);
        System.out.println(address);

        String favMovie = "Gone with the Wind";
        System.out.println("My favorite movie = " + favMovie);
    }
}
