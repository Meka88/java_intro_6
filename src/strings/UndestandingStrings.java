package strings;

public class UndestandingStrings {
    public static void main(String[] args) {
        String s1; // declaration
        s1 = "TechGlobal School"; // initializing
        String s2 = "is the best";
        System.out.println("-------------------Concat using + sign-----------------");

        String s3 = s1 + " " + s2; // concatenation using + sign
        System.out.println(s3);


        System.out.println("-------------------Concat using concat() method -----------------");

        String s4 = s1.concat(" ").concat(s2);
        System.out.println(s4);

        System.out.println("-------------------Exercise1 -----------------");
        String wordPart1 = "le";
        String wordPart2 = "ar";
        String wordPart3 = "ning";
        String word = wordPart1 + wordPart2 + wordPart3;

        System.out.println(word);

        System.out.println("-------------------Exercise2 -----------------");
        String sentencePart1 = "I can";
        String sentencePart2 = "learn Java";
        String sentence = sentencePart1.concat(" ").concat(sentencePart2);

        System.out.println(sentence);
    }
}
