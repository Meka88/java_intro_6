package primitives;

public class Characters {
    public static void main(String[] args) {
        /*
        char => 2 bytes
        it could be single character
        letter, digit, space, specials
         */

        char c1 = 'A';
        char c2 = ' '; // need to have space
        System.out.println(c1);
        System.out.println(c2);

        char myFavCharacter = '@';
        System.out.println("My favorite character " + myFavCharacter);
    }
}
