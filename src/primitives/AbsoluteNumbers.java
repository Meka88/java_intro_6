package primitives;

public class AbsoluteNumbers {
    public static void main(String[] args) {
        /*
        Absolute numbers : 1, 5, -3, -125, 23476576
        byte => -128 to 127
        short => 32767 to - 32768
        int => -2147483648 to 2147483647
        long => -9223372036854775808 to 9223372036854775807

        dataType variableName = value
         */
        System.out.println("\n------------byte----------\n");
        byte myNumber = 45;
        System.out.println(myNumber);

        System.out.println("The max value of byte = " + Byte.MAX_VALUE);
        System.out.println("The min value of byte = " + Byte.MIN_VALUE);


        System.out.println("\n------------short----------\n");
        short numberShort = 150;
        System.out.println(numberShort);
        System.out.println("The max value of short = " + Short.MAX_VALUE);
        System.out.println("The min value of short = " + Short.MIN_VALUE);

        System.out.println("\n------------int----------\n");
        int myInteger = 20000000;
        System.out.println(myInteger);
        System.out.println("The max value of int = " + Integer.MAX_VALUE);
        System.out.println("The max value of int = " + Integer.MIN_VALUE);

        System.out.println("\n------------long----------\n");
        long myBigNumber = 7647389372478475L;
        System.out.println(myBigNumber);
        System.out.println("The max value of long = " + Long.MAX_VALUE);
        System.out.println("The min value of long = " + Long.MIN_VALUE);

        long l1 = 5;
        long l2 = 284737457566877L;
        System.out.println(l1);
        System.out.println(l2);
    }
}
