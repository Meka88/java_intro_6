package primitives;

public class FloatingNumbers {
    public static void main(String[] args) {
        /*
        Floating numbers: 10.5, 23.5757, 1020.4986

        float => 4 bytes
        double => 8 bytes
         */
        System.out.println("\n------------floating numbers----------\n");
        float myFloat1 = 20.5F;
        double myDouble1 = 20.5;
        System.out.println(myFloat1);
        System.out.println(myDouble1);

        float myFloat2 = 10;//10.0
        double myDouble2 = 234235;//234235.0
        System.out.println(myFloat2);
        System.out.println(myDouble2);

        System.out.println("\n------------floating numbers precision----------\n");
        float f1 = 3464.5876454F;
        double d1 = 3464.5876454;
        System.out.println(f1);
        System.out.println(d1);
    }
}
