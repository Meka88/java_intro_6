package concatenation;

public class Exercise01 {
    public static void main(String[] args) {
        System.out.println("\n------------Task1----------\n");
        System.out.println("Hello World");
        System.out.println("Hello " + "World");
        System.out.println("Hello" + " " + "World");
        System.out.println("Today" + " " + "is" + " " + "Sunday");

        System.out.println("\n------------Task2----------\n");
        System.out.println("123" + " " + "12");
        System.out.println("\t" + "12" + "\t\nab");

    }
}
