
import java.util.HashMap;
import java.util.Map;

public class Mock {
    public static void main(String[] args) {
        String[] array = {"Apple", "Apple", "Orange", "Apple", "Kiwi"};
        Map<String, Integer> elementCountMap = countUniqueElements(array);
        System.out.println(elementCountMap);
    }
    public static Map<String, Integer> countUniqueElements(String str[]){
        Map<String, Integer> map = new HashMap<>();
        for(String e : str){
            if(map.containsKey(e)){
                int count = map.get(e);
                map.put(e, count+1);
            }else{
                map.put(e, 1);
            }
        }
        return map;
    }
}
