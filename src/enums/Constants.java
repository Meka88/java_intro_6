package enums;

public class Constants {
    // create multiple enums inside class
    // in order to use we className.enumName.info
    // Constants.Gender.MALE

    public enum Gender{
        FEMALE,
        MALE,
        OTHER
    }

    public enum TShirtSize{
        X_SMALL,
        SMALL,
        MEDIUM,
        LARGE,
        X_LARGE
    }


    public enum Direction{
        NORTH,
        SOUTH,
        WEST,
        EAST
    }
}
