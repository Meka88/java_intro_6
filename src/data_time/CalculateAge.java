package data_time;

import utilities.ScannerHelper;

import java.time.LocalDate;
import java.time.Month;
import java.time.temporal.ChronoUnit;
import java.util.Scanner;

public class CalculateAge {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Please enter your year of birth:");
        int yearOfBirth = input.nextInt();

        System.out.println(LocalDate.now().getYear() - yearOfBirth);
        System.out.println(ChronoUnit.YEARS.between(LocalDate.of(yearOfBirth, Month.JANUARY, 1), LocalDate.now()));


    }
}
