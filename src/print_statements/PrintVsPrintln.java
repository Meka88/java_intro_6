package print_statements;

public class PrintVsPrintln {
    public static void main(String[]args){
        //println
        System.out.println("Hello World");
        System.out.println("Today is Sunday");
        System.out.println("Jane Doe");


        // print
        System.out.print("Hello World");
        System.out.print("Today is Sunday");
        System.out.print("Jane Doe");
    }

}
