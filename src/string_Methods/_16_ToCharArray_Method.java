package string_Methods;

import java.util.Arrays;

public class _16_ToCharArray_Method {
    public static void main(String[] args) {
        String name = "John";
        char[] charOfName = name.toCharArray();
        System.out.println(Arrays.toString(charOfName));

        // print element at the index of 1

        System.out.println(charOfName[1]);
        System.out.println(charOfName.length);
    }
}
