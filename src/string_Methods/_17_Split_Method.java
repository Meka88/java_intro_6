package string_Methods;

import java.lang.reflect.Array;
import java.util.Arrays;

public class _17_Split_Method {
    public static void main(String[] args) {
        String str = "Hello World";
        String[] arr1 = str.split(" ");

        String str2 = "John - Doe - 11/11/1999 - johndoe@gmail.com - Chicago";
        String[] arr2 = str2.split(" - ");

        System.out.println(Arrays.toString(arr1));
        System.out.println(Arrays.toString(arr2));
    }
}
