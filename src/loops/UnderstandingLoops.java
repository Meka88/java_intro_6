package loops;

public class UnderstandingLoops {
    public static void main(String[] args) {
        /*
        for loop syntax
         1.for(initialization; termination; update){
          block of code
         }

         initialization -> start point
         termination -> stop point
         update -> increasing or decreasing

         */
        // print Hello world 20 times
        for(int num = 0; num < 5; num++){
            System.out.println("Hello world");
        }
    }
}
