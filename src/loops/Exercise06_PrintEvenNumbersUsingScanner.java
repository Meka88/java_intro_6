package loops;

import utilities.ScannerHelper;

public class Exercise06_PrintEvenNumbersUsingScanner {
    public static void main(String[] args) {
        /*
        Write a program that asks user to enter 2 different positive numbers
        Print all the even numbers bt given numbers by user in ascending order
        3, 10 ->
        4
        6
        8
        10

        7, 2  ->
        2
        4
        6


         */

        int num1 = ScannerHelper.getNumber();
        int num2 = ScannerHelper.getNumber();

        for(int i = Math.min(num1, num2); i <= Math.max(num1, num2); i++){
            if(i % 2 == 0) System.out.println(i);
        }

         /*
        if(num1 >= num2){
            for(int j = i2; j <= i1; j++){
                if(j % 2 == 0){
                    System.out.println(j);
                }
            }
        }
        else{
            for(int j = i1; j <= i2; j++){
                if(j % 2 == 0){
                    System.out.println(j);
                }
            }
        }
         */
    }
}
