package loops.practices;

import utilities.ScannerHelper;

public class Exercise04 {
    public static void main(String[] args) {
        /*
        Write a program that asks user to enter a String.
        Count the number of vowels in the given String and print it.
        Vowels are A, E, O, U, I, a, e, o, u, I
         */

        String str = ScannerHelper.getString();
        int count = 0;
        for (int i = 0; i < str.length(); i++) {
            char a = Character.toLowerCase(str.charAt(i));
           if(a == 'a' || a == 'e' || a == 'i' || a == 'o' || a == 'u') count++;
        }
        System.out.println(count);
    }
}
