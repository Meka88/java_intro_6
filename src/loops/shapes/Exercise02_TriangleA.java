package loops.shapes;

public class Exercise02_TriangleA {
    public static void main(String[] args) {
        /*
        Write a program that prints below triangle

        A
        AA
        AAA
        AAAA
        AAAAA
        AAAAAA



         */

        for(int x = 1; x <= 6; x++){
            for(int y = 0; y < x; y++){
                System.out.print("A");
            }
            System.out.println();
        }
    }
}
