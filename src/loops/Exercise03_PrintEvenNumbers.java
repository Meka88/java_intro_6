package loops;

public class Exercise03_PrintEvenNumbers {
    public static void main(String[] args) {
        /*
        Write a Java program to print only even numbers from 0 to 10 (both included)
        expected output:
        0
        2
        4
        6
        8
        10
         */

        for (int i = 0; i <= 10; i+=2) {
            System.out.println(i);
        }

        // solution 2

        for (int i = 0; i <= 10; i++) {
            if(i % 2 == 0) System.out.println(i);
        }

        // solution 3
        // not in the requirements
        for (int i = 0; i <= 5; i++) {
            System.out.println(i * 2);
        }
    }
}
