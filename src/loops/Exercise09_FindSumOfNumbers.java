package loops;

public class Exercise09_FindSumOfNumbers {
    public static void main(String[] args) {
        int count = 0;
        for(int i = 10; i <= 15; i++){
            count += i;
        }
        System.out.println(count);
    }
}
