package loops;

import utilities.ScannerHelper;

public class Exercise10_SumOfNumbersByUser {
    public static void main(String[] args) {
         /*
        Write a program that asks user to enter 5 numbers
        Find sum of the given numbers by user.
        NOTE: Ask user to enter numbers one by one.

        2, 3, 4, 5, 6

        Output:
        20

        11, 15, 23, -7, 8

        Output:
        50
         */

//        int n1 = ScannerHelper.getNumber(); *only thing repeating is getting numbers=>
//        int n2 = ScannerHelper.getNumber(); => so we use loop for it
//        int n3 = ScannerHelper.getNumber();
//        int n4 = ScannerHelper.getNumber();
//        int n5 = ScannerHelper.getNumber();
        // for loop
        int sum = 0;
        for(int i = 1; i <= 5; i ++){
            sum += ScannerHelper.getNumber();
        }
        System.out.println(sum);

        // while loop
        int j = 1;
        int sum1 = 0;
        while(j <= 5){
            sum1 += ScannerHelper.getNumber();
            j++;
        }
        System.out.println(sum1);
    }
}
