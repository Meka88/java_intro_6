package loops.while_loops;

public class WhileLoop {
    public static void main(String[] args) {

        for (int i = 1; i <= 5; i++) {
            System.out.println(i);
        }

        // while loop
        int num = 1;
        while(num <= 5) {
            System.out.println(num);
            num++;
        }
    }
}
