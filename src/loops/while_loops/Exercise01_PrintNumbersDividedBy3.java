package loops.while_loops;

public class Exercise01_PrintNumbersDividedBy3 {
    public static void main(String[] args) {
        /*
        Write a program that prints all the numbers divided by 3, starting from 1 to 100 (both included)

        3
        6
        9
        .
        .
        .
        .
        96
        99
         */

        int i = 1;

        while(i <= 100){
            if(i % 3 == 0) System.out.println(i);
            i++;
        }
    }
}
