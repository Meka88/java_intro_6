package loops;

import utilities.ScannerHelper;

public class Exercise12_ReverseString {
    public static void main(String[] args) {
        String str = ScannerHelper.getString();
        String result = "";

        for(int i = 0; i < str.length(); i++){
            result = str.charAt(i) + result;
        }
        System.out.println(result);
    }
}
