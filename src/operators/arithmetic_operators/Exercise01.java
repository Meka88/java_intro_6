package operators.arithmetic_operators;

public class Exercise01 {
    public static void main(String[] args) {
        /*
        Rectangle -
        Create two variables and store two values as a and b
        Find the area with multiplying = a * b
        Find the perimeter with formula (2 * (a + b))

        Print them out with below messages
        Area =
        Perimeter =
         */

        int a = 5, b = 8;

        int area = a * b;
        int perimeter = 2 * (a + b);

        System.out.println("Area = " + area);
        System.out.println("Perimeter = " + perimeter);

    }
}
