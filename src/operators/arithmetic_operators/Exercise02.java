package operators.arithmetic_operators;

public class Exercise02 {
    public static void main(String[] args) {
        /*
        An annual average salary for an SDET in the Unites States is 90k.
        Write a Java program that calculates and prints the monthly and bi-weekly and
        weekly average amount that an SDETs makes in the United States.

        90000

        Monthly = 90k / 12
        Weekly = 90k / 52
        Bi-weekly = 90k / 26

         */

        double annual = 90000, monthly = annual / 12, bi_weekly = annual / 26, weekly = annual / 52;
//        double monthly = annual / 12;
//        double bi_weekly = annual / 26;
//        double weekly = annual / 52;


        System.out.println("Monthly = $" + monthly);
        System.out.println("Bi-weekly = $" + bi_weekly);
        System.out.println("Weekly = $" + weekly);

    }
}
