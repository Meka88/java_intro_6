package operators.arithmetic_operators;

public class DivisionPractices {
    public static void main(String[] args) {
        int i1 = 5, i2 = 2;
        double d1 = 5, d2 = 2;

        int divide1 = i1 / i2;
        double divide2 = d1 / d2;
        System.out.println(divide1);
        System.out.println(divide2);

        double s1 = 15, s2 = 2;
        double divide3 = s1 / s2;
        System.out.println(divide3);
    }
}
