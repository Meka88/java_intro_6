package operators.increment_decrement_operators;

import java.util.Scanner;

public class Exercise01 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Please enter a number: ");
        int num = input.nextInt();
        int i = 1;

        System.out.println(num + " * " + i + " = " + num * i++);
        System.out.println(num + " * " + i + " = " + num * i++);
        System.out.println(num + " * " + i + " = " + num * i++);
        System.out.println(num + " * " + i + " = " + num * i++);
        System.out.println(num + " * " + i + " = " + num * i++);
    }
}
