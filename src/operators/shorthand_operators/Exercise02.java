package operators.shorthand_operators;

import java.util.Scanner;

public class Exercise02 {
    public static void main(String[] args) {

        /*
        Write a Java program that asks user to enter their balance and one day transactions.
        Subtract each transaction from balance and return new balance using shorthand operator


        Requirements:
        Use Scanner class to read input from user

        Test data:
        Balance = $100.00
        1st transaction = $25.75
        2nd transaction =  $12.50
        3rd transaction = $7.25

        Expected output:
        Balance after 1st transaction = $74.25
        Balance after 2nd transaction = $61.75
        Balance after 3rd transaction = $54.5

        1. Create Scanner object
        2. ask user to enter their balance
        3. ask user to enter the transaction
        4. print balance
         */

        Scanner input = new Scanner(System.in);

        System.out.println("Please enter your balance: ");
        double myBalance = input.nextDouble();

        System.out.println("The initial balance is $" + myBalance);

        System.out.println("What is your first transaction amount?");
        double firstTrans = input.nextDouble();

        myBalance -= firstTrans;

        System.out.println("The balance after first transaction is $" + myBalance);

        System.out.println("Please enter your second transaction: ");
//        double secondTrans = input.nextDouble();
//
//        myBalance -= secondTrans;

        System.out.println("The balance after second transaction is $" + (myBalance -= input.nextDouble()));

        System.out.println("Please enter your third transaction: ");
        double thirdTrans = input.nextDouble();

        myBalance -= thirdTrans;

        System.out.println("The balance after third and final transaction is $" + myBalance);
    }

}
