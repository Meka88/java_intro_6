package operators.shorthand_operators;

public class ShortHandPractice {
    public static void main(String[] args) {
        int age = 25;
        System.out.println("Age in 2023 = " + age);

        age += 5; // shorthand of adding
        age -= 10;
        age /= 4;
        age %= 2;
        age *= 3;

    }
}
