package scannerClass;

import java.util.Scanner;

public class Recap01 {
    public static void main(String[] args) {

        /*
        Write a program that asks user to enter their firstName, address, favNumber
        Then, print them all in a way as below
        {firstName}'s address is {address} and their fav number is {favNumber}.
        FirstName = John        -> next()
        Address = Chicago IL    -> nextLine()
        Fav Number = 7          -> nextInt()
        John's address is Chicago IL and their fav number is 7.
         */

        Scanner input = new Scanner(System.in);

        System.out.println("Please enter your first name:");
        String firstName = input.next();

        input.nextLine(); // empty line to skip the problem of Scanner

        System.out.println("Please enter your address:");
        String address = input.nextLine();


        System.out.println("Please enter your favorite number:");
        int favNumber = input.nextInt();

        System.out.println(firstName + "'s address is" + " " + address + " " + "and their favorite number is" + " " + favNumber + ".");
    }
}
