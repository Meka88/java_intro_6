package scannerClass;

import java.util.Scanner;

public class Exercise02 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Please enter number 1");
        int num1 = input.nextInt();

        System.out.println("Please enter number 2");
        int num2 = input.nextInt();

        System.out.println("Please enter number 3");
        int num3 = input.nextInt();
        //int sum = num3 + num2 + num1; // can be done this way or

        System.out.println("The sum of the numbers you entered is " + (num1+num2+num3)); // this
    }
}
