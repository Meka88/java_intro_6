package scannerClass;

import java.util.Scanner;

public class Exercise01 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        String name, lastName, address;

        System.out.println("Please enter your name?");
        name = input.next();
        //input.nextLine();

        System.out.println("Please enter your last name?");
        lastName = input.next(); // or use nextLine here
        input.nextLine(); // needed to put out next request

        System.out.println("Your full name is " + name + " " + lastName);


        System.out.println("--------------------task 2----------------------");

        System.out.println("Please enter your address:");
        address = input.nextLine();

        System.out.println("Address: " + address);


    }
}
