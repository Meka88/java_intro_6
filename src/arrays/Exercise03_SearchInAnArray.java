package arrays;

import java.util.Arrays;

public class Exercise03_SearchInAnArray {
    public static void main(String[] args) {
        String[] objects = {"Remote", "Mouse", "Mouse", "Keyboard", "iPad"};

        Arrays.sort(objects);
        System.out.println(Arrays.binarySearch(objects, "Mouse") >= 0);

        boolean hasMouse = false;
        for (String object : objects) {
            if(object.equals("Mouse")) hasMouse = true;
            break;
        }
        System.out.println(hasMouse);

    }
}
