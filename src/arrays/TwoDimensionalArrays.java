package arrays;

import java.util.Arrays;

public class TwoDimensionalArrays {
    public static void main(String[] args) {
        String[][] students = {
                {"Meerim", "Alina", "Carmela", "Ayat"},
                {"Yahya", "Adam", "Louie"},
                {"Dima", "Lesia", "Pinar"}
        };
        // to print whole array
        System.out.println(Arrays.deepToString(students));

        // to print an inner array
        System.out.println(Arrays.toString(students[0]));
        System.out.println(Arrays.toString(students[1]));
        System.out.println(Arrays.toString(students[2]));

        // to print element from inner array
        System.out.println(students[1][2]);

        // to get length of two-dimensional array
        System.out.println(students.length);

        // how to get length of inner array
        System.out.println(students[0].length);

        // how to loop 2-dimensional array

        for (int i = 0; i < students.length; i++) {
            //System.out.println(Arrays.toString(students[i]));
            for (int j = 0; j < students[i].length; j++) {
                System.out.println(students[i][j]);
            }
        }

        // create a container that will store 5 groups of 3 numbers
        int[][] numbers = new int[5][3];
        System.out.println(Arrays.deepToString(numbers));

        numbers[3][1] = 9;
        System.out.println(Arrays.deepToString(numbers));


        int[][] numbers1 = new int[5][3];
        System.out.println(Arrays.deepToString(numbers1));

        numbers1[3][1] = 9;
        numbers1[2][0] = 5;
        numbers1[0][0] = 7;

        System.out.println(Arrays.deepToString(numbers1));
    }
}
