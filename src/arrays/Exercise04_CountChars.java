package arrays;

import utilities.ScannerHelper;

public class Exercise04_CountChars {
    public static void main(String[] args) {
        String str = ScannerHelper.getString();

        char[] chars = str.toCharArray();
        int count = 0;
        for (char x : chars) {
            if(Character.isLetter(x)) count++;
        }
        System.out.println(count);
    }
}
