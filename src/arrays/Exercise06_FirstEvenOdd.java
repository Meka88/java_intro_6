package arrays;

public class Exercise06_FirstEvenOdd {
    public static void main(String[] args) {
        int[] numbers = {0, 5, 3, 2, 4, 7, 10};

        int even = 1;
        for (int number : numbers) {
            if(number % 2 == 0){
                System.out.println(number);
                break;
            }
        }

        for (int number : numbers) {
            if(number % 2 != 0){
                System.out.println(number);
                break;
            }
        }

        boolean isEven = false;
        boolean isOdd = false;
        for (int number : numbers) {
            if(!isEven && number % 2 == 0){
                System.out.println(number);
                isEven = true;
            }
            else if(!isOdd && number % 2 != 0){
                System.out.println(number);
                isOdd = true;
            }
            if(isEven && isOdd) break;
        }
    }
}
