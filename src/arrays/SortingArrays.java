package arrays;

import java.util.Arrays;

public class SortingArrays {
    public static void main(String[] args) {
       int[] numbers = {5, 3, 10};
       String[] words = {"Alex", "ali", "James", "John"};

       /*
       static: if you are able to invoke the method with class name -> className.method
        */
        Arrays.sort(numbers);
        Arrays.sort(words);

        System.out.println(Arrays.toString(numbers));
        System.out.println(Arrays.toString(words));
    }
}
