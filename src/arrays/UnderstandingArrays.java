package arrays;

import java.util.Arrays;

public class UnderstandingArrays {
    public static void main(String[] args) {
       String[] cities = {"Chicago", "Miami", "Toronto", "New York", "Bishkek"};

       // the number of elements in the array

        //int sizeOfTheArray = cities.length; optional to create variable
        System.out.println(cities.length);

        // get a certain element

        //String city = cities[1]; optional to create variable
        System.out.println(cities[1]);
        System.out.println(cities[0]);
        System.out.println(cities[2]);

        //index out of bound exception
        //System.out.println(cities[5]);

        //how to print an array
        // 1. convert to string
        // 2. print wit print method
        System.out.println(Arrays.toString(cities));

        // how to loop an Array
        for (int i = 0; i < cities.length; i++) {
            System.out.println(cities[i]);
        }

        // enhanced loop for arrays
        for(String element : cities){ // element can be replaced with city
            System.out.println(element);
        }

    }
}
