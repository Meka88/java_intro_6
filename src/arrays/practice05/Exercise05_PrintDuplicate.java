package arrays.practice05;

public class Exercise05_PrintDuplicate {
    public static void main(String[] args) {
        /*
        Write a program to print duplicated characters in a
        String, ignore cases
        Test data 1:
        String str = ”baNana”;
        Expected output 1:
        a
        N
        Test data 2:
        String str = ”aPple”;
        Expected output 1:
        P
        NOTE: Make your code dynamic that works for any
        given String.
         */
        String str = "baNana";
        getDuplicateCharacter(str);
    }
    public static void getDuplicateCharacter(String str){

        String result = "";

        for (int i = 0; i < str.length() - 1; i++) {
            for (int j = i + 1; j < str.length(); j++) {
               if(str.toLowerCase().charAt(i) == str.toLowerCase().charAt(j) &&
                       !result.contains("" + str.charAt(i))) result += str.charAt(i);
            }
        }
        char[] resultArr = result.toCharArray();
        for (char c : resultArr) {
            System.out.println(c);
        }

    }
}
