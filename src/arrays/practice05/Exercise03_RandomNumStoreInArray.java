package arrays.practice05;

import utilities.RandomGenerator;

import java.util.Arrays;

public class Exercise03_RandomNumStoreInArray {
    public static void main(String[] args) {
        /*
        Write a program to generate 5 random numbers
        between 1 to 10 (1 and 10 are included) and store those
        numbers in an int array.
        Find the max and min numbers among the random
        numbers and print them.
        Solve this question with sort and without sort.
         */
        getMaxAndMinSort();
        getMaxAndMin();
    }
    public static void getMaxAndMinSort(){
        int[] numbers = new int[5];

        for(int i = 0; i < numbers.length; i++) {
            numbers[i] = (int)(Math.random()*10 + 1);
        }
        Arrays.sort(numbers);
        System.out.println(Arrays.toString(numbers));
        System.out.println("The max number is " + numbers[0]);
        System.out.println("The min number is " + numbers[numbers.length - 1]);
    }
    public static void getMaxAndMin(){
        int[] randomArr = new int[5];
        for(int i = 0; i < randomArr.length; i++) {
            randomArr[i] = (int)(Math.random() * 10 + 1);
        }
        // second way of putting random numbers in an array
//        int[] randArr = {RandomGenerator.getRandomNumber(1,10),
//                RandomGenerator.getRandomNumber(1,10),
//                RandomGenerator.getRandomNumber(1,10),
//                RandomGenerator.getRandomNumber(1,10),
//                RandomGenerator.getRandomNumber(1,10)};
        System.out.println(Arrays.toString(randomArr));
        int min = 10;
        int max = 1;
        for (int i : randomArr) {
            if(i > max) max = i;
            else if(i < min) min = i;
        }
        System.out.println("The max number is " + max);
        System.out.println("The min number is " + min);

    }
}
