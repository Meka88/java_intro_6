package arrays.practice05;

public class Exercise04_FindApple {
    public static void main(String[] args) {
        /*
        Write a program to find if String array contains “apple”
        as an element, ignore cases.
        Test data 1:
        String[] list = {“banana”, “orange”, “Apple”};
        Expected output:
        true
        Test data 2:
        String[] list = {“pineapple”, “banana”, “orange”,
        “grapes”};
        Expected output:
        false
        NOTE: Make your code dynamic that works for any
         */
        //String[] list = {"banana", "orange", "Apple"};
        String[] list = {"pineapple", "banana", "orange", "grapes"};
        getAWordInAnArray(list);
    }
    public static void getAWordInAnArray(String[] list){
        boolean isApple = false;
        //String[] list = {"banana", "orange", "Apple"};
        for (String s : list) {
            if(s.equalsIgnoreCase("apple")) {
                isApple = true;
                break;
            }
        }
        System.out.println(isApple);
    }
}
