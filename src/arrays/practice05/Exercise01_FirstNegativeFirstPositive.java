package arrays.practice05;

public class Exercise01_FirstNegativeFirstPositive {
    public static void main(String[] args) {
        /*
        Write a program to find the first positive and negative
        numbers in an int array
        Test data:
        int[] numbers = {0, -4, -7, 0, 5, 10, 45};
        Expected output:
        First positive number is: 5
        First negative number is: -4
        NOTE: Make your code dynamic that works for any
        given int array.
         */
        int[] arr = {0, -4, -7, 0, 5, 10, 45};
        getFirstPosAndNeg(arr);
    }
    // created a method to get the answer
    public static void getFirstPosAndNeg(int[] arr){
        int positive = 0;
        int negative = 0;
        for (int i : arr) {
            if(i > 0) {
                positive = i;
                break;
            }
        }
        for (int i : arr) {
            if(i < 0) {
                negative = i;
                break;
            }
        }
        System.out.println("First positive number is: " + positive);
        System.out.println("First positive number is: " + negative);
    }
}
