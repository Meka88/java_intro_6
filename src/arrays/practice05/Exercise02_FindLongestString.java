package arrays.practice05;

public class Exercise02_FindLongestString {
    public static void main(String[] args) {
        /*
        Write a program to find the longest and shortest strings
        in an String array
        Test data:
        String[] words = {"red", "blue", "yellow", "white"};
        Expected output:
        The longest word is = yellow
        The shortest word is = red
        NOTE: Make your code dynamic that works for any
        given String array.
         */
        String[] words = {"red", "blue", "yellow", "white"};
        getLongestString(words);

    }
    public static void getLongestString(String[] words){
        int strLen = 0;
        String longest = null;
        for (String word : words) {
            if(word.length() > strLen){
                strLen = word.length();
                longest = word;
            }
        }
        String shortest = null;
        for (String word : words) {
            if(word.length() < strLen){
                strLen = word.length();
                shortest = word;
            }
        }
        System.out.println("The longest word is = " + longest);
        System.out.println("The shortest word is = " + shortest);

        /*
       ////////// Bilals solution
         public static void getShortestLongest(String[] words){
        String shortest = words[0];
        String longest = words[0];

        for (String s : words) {
            if(s.length() > longest.length()) longest = s;
            else if(s.length() < shortest.length()) shortest = s;
        }
        System.out.println("The longest word is = " + longest);
        System.out.println("The shortest word is = " + shortest);

        }
         */
    }
}
