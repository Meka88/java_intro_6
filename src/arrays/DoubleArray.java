package arrays;

import java.util.Arrays;

public class DoubleArray {
    public static void main(String[] args) {
        // 1. create an array to store -> 5.5, 6, 10.3, 25
        // 2. print the array
        // 3. print the size of array
        // 4. print each element of the array

        Double[] numbers = {5.5, 6.0, 10.3, 25.0};
        System.out.println(Arrays.toString(numbers));
        System.out.println("The length is " + numbers.length);
        for(double number : numbers){
            System.out.println(number);
        }

    }
}
