package arrays;

import utilities.ScannerHelper;

public class Exercise05_CountWords {
    public static void main(String[] args) {
        /*
        Write a program that asks user to enter a String
        And count how many words you have in the given String

        "Hello World"           -> 2
        "Java is fun"           -> 3
        "Today is a nice class with technical issues" -> 8


         */
        String str = ScannerHelper.getString();
        String[] str1 = str.split(" ");
        System.out.println(str1.length);

    }
}
