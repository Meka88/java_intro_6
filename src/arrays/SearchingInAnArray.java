package arrays;

import java.util.Arrays;

public class SearchingInAnArray {
    public static void main(String[] args) {
        int[] numbers = {3, 10, 8, 5, 5};

        // Check if this arrays has 7 => true , otherwise => false
        System.out.println("\n---------------Loop version---------------\n");
        boolean num7 = false;
        for (int number : numbers) {
            if(number == 7) num7= true;
            break;
        }
        System.out.println(num7);

        System.out.println("\n---------------Binary search---------------\n");
        // you need to SORT YOUR ARRAY before searching

        Arrays.sort(numbers);
        System.out.println(Arrays.binarySearch(numbers, 5)); // returns index of that number
        System.out.println(Arrays.binarySearch(numbers, 3));
        System.out.println(Arrays.binarySearch(numbers, 10));

        System.out.println(Arrays.binarySearch(numbers, 7)); // returns negative number
        System.out.println(Arrays.binarySearch(numbers, 15));


    }
}
