package arrays;

import java.lang.reflect.Array;
import java.util.Arrays;

public class Exercise02_CountStrings {
    public static void main(String[] args) {
        // 1. declare a String array called countries and assign size 3
        String[] countries = new String[3];

        // 2. assign Spain to index 1
        countries[1] = "Spain";

        // 3. print the values at index 1 and 2
        System.out.println(countries[1]);
        System.out.println(countries[2]);

        // 4. assign "Belgium" at index of 0 and "Italy" index of 2
        countries[0] = "Belgium";
        countries[2] = "Italy";

        // 5. print the array
        System.out.println(Arrays.toString(countries));

        // 6. sort this array
        Arrays.sort(countries);
        System.out.println(Arrays.toString(countries));

        // 7. 8. print each element with fori and for each loop
        for (int i = 0; i < countries.length; i++) {
            System.out.println(countries[i]);
        }

        for (String country : countries) {
            System.out.println(country);
        }

        // 9. count how many countries have 5 characters

        int count = 0;
        for (String country : countries) {
            if(country.length() == 5) count++;
        }
        System.out.println(count);

        // 10. count how many countries has i or I
        int countI = 0;
        for (String country : countries) {
            if(country.contains("I") || country.contains("i")) countI++;
        }
        System.out.println(countI);
    }
}
