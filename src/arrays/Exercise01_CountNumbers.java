package arrays;

public class Exercise01_CountNumbers {
    public static void main(String[] args) {
         /*

        negatives
        positives
        neutral

        evens
        odds

        divisible by 3
        divisible by 5
        divisible by 10

        sum
        average
        product

        max
        min
        abs difference bt min and max
        the biggest difference bt respective 2 numbers -> 17

        closest number to 9 (take the smallest in case you have 2 on the left and the right) ->

        find how many unique elements -> 7
        find how many numbers are represented in fibonacci series -> 6
        find how many numbers are prime numbers -> 2


         */
        // write a program that counts how many negative numbers you in array -> 2
        int[] numbers = {-1, 3, 0, 5, -7, 10, 8, 0, 10, 0};

        /*
        pseudocode:
        1. check each numbers one by one
        2. store them in variable
        3. print that variable
         */

        int countNegatives = 0;
        for(int number : numbers){
            if(number < 0) countNegatives++;
        }
        System.out.println(countNegatives);


        // write a program that count how many even numbers in the array
        int countEven = 0;
        for (int number : numbers) {
            if(number % 2 == 0) countEven++;
        }
        System.out.println(countEven);

        // write a program tha find sum of all the numbers in the array
        int sum = 0;
        for (int number : numbers) {
            sum += number;
        }
        System.out.println(sum);


    }
}
