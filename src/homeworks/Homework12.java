package homeworks;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Homework12 {
    public static void main(String[] args) {
        System.out.println("\n--------------Task1--------------\n");

        System.out.println(noDigit("Java"));
        System.out.println(noDigit("123Hello"));
        System.out.println(noDigit("123Hello World149"));

        System.out.println("\n--------------Task2--------------\n");

        System.out.println(noVowel(""));
        System.out.println(noVowel("Java"));
        System.out.println(noVowel("xyz"));
        System.out.println(noVowel("TechGlobal"));

        System.out.println("\n--------------Task3--------------\n");

        System.out.println(sumOfDigits(""));
        System.out.println(sumOfDigits("Java"));
        System.out.println(sumOfDigits("John’s age is 29"));
        System.out.println(sumOfDigits("$125.0"));

        System.out.println("\n--------------Task4--------------\n");

        System.out.println(hasUpperCase(""));
        System.out.println(hasUpperCase("java"));
        System.out.println(hasUpperCase("John’s age is 29"));
        System.out.println(hasUpperCase("$125.0"));

        System.out.println("\n--------------Task5--------------\n");

        System.out.println(middleInt(1, 1, 1));
        System.out.println(middleInt(1, 2, 2));
        System.out.println(middleInt(5, 5, 8));
        System.out.println(middleInt(5, 3, 5));
        System.out.println(middleInt(-1, 25, 10));

        System.out.println("\n--------------Task6--------------\n");

        System.out.println(Arrays.toString(no13(new int[]{1, 2, 3, 4})));
        System.out.println(Arrays.toString(no13(new int[]{13, 2, 3})));
        System.out.println(Arrays.toString(no13(new int[]{13, 13, 13, 13, 13})));

        System.out.println("\n--------------Task7--------------\n");

        System.out.println(Arrays.toString(arrFactorial(new int[]{1, 2, 3, 4})));
        System.out.println(Arrays.toString(arrFactorial(new int[]{0, 5})));
        System.out.println(Arrays.toString(arrFactorial(new int[]{5, 0, 6})));
        System.out.println(Arrays.toString(arrFactorial(new int[]{})));

        System.out.println("\n--------------Task8--------------\n");

        System.out.println(Arrays.toString(categorizeCharacters("     ")));
        System.out.println(Arrays.toString(categorizeCharacters("abc123$#%")));
        System.out.println(Arrays.toString(categorizeCharacters("12ab$%3c%")));

    }

    /*
    Requirement:
    -Create a method called noDigit()
    -This method will take one String argument, and it will
    return a new String with all digits removed from the
    original String
    Test Data 1: “”
    Expected Result 1: “”
    Test Data 2: “Java”
    Expected Result 2: “Java”
    Test Data 3: “123Hello”
    Expected Result 3: “Hello”
    Test Data 4: “123Hello World149”
    Expected Result 4: “Hello World”
    Test Data 5: “123Tech456Global149”
    Expected Result 5: “TechGlobal”
     */

    public static String noDigit(String str){
        String result = str.replaceAll("\\d", "");
        return result;
    }

    /*
    Requirement:
    -Create a method called noVowel()
    -This method will take one String argument and it will
    return a new String with all vowels removed from the
    original String
    Test Data 1: “”
    Expected Result 1: “”
    Test Data 2: “xyz”
    Expected Result 2: “xyz”
    Test Data 3: “JAVA”
    Expected Result 3: “JV”
    Test Data 4: “125$”
    Expected Result 4: “125$”
    Test Data 5: “TechGlobal”
    Expected Result 5: “TchGlbl”
     */

    public static String noVowel(String str){
        String result = str.replaceAll("[aeiouAEIOU]", "");
        return result;
    }

    /*
    Requirement:
    -Create a method called sumOfDigits()
    -This method will take one String argument, and it will return an int sum
    of all digits from the original String.
    NOTE: Return zero if there is no digits in the String
    Test Data 1: “”
    Expected Result 1: 0
    Test Data 2: “Java”
    Expected Result 2: 0
    Test Data 3: “John’s age is 29”
    Expected Result 3: 11
    Test Data 4: “$125.0”
    Expected Result 4: 8
     */

    public static int sumOfDigits(String str){
        int sum = 0;

        for (int i = 0; i < str.length(); i++) {
            char ch = str.charAt(i);
            if (Character.isDigit(ch)) {
                sum += Character.getNumericValue(ch);
            }
        }

        return sum;

//        int sum = 0;
//        Pattern pattern = Pattern.compile("\\d");
//        Matcher matcher = pattern.matcher(str);
//
//        while(matcher.find()){
//            sum += Integer.parseInt(matcher.group());
//        }
//        return sum;
    }

    /*
    Requirement:
    -Create a method called hasUpperCase()
    -This method will take one String argument, and it will return boolean
    true if there is an uppercase letter and false otherwise.
    Test Data 1: “”
    Expected Result 1: false
    Test Data 2: “java”
    Expected Result 2: false
    Test Data 3: “John’s age is 29”
    Expected Result 3: true
    Test Data 4: “$125.0”
    Expected Result 4: false
     */

    public static boolean hasUpperCase(String str){
        for (int i = 0; i < str.length(); i++) {
            if(Character.isUpperCase(str.charAt(i))){
                return true;
            }
        }
        return false;
    }

    /*
    Requirement:
    -Create a method called middleInt()
    -This method will take three int arguments, and it will return the middle
    int.
    Test Data 1: 1, 1, 1
    Expected Result 1: 1
    Test Data 2: 1, 2, 2
    Expected Result 2: 2
    Test Data 3: 5, 5, 8
    Expected Result 3: 5
    Test Data 4: 5, 3, 5
    Expected Result 4: 5
    Test Data 4: -1, 25, 10
    Expected Result 4: 10
     */

    public static int middleInt(int a, int b, int c){
        if ((a >= b && a <= c) || (a <= b && a >= c)) {
            return a;
        } else if ((b >= a && b <= c) || (b <= a && b >= c)) {
            return b;
        } else {
            return c;
        }
    }

    /*
    Requirement:
    -Create a method called no13()
    -This method will take an int array as argument, and it will return a new
    array with all 13 replaced with 0.
    NOTE: Assume length will always be more than zero.
    Test Data 1: [1, 2, 3 ,4]
    Expected Result 1: [1, 2, 3 ,4]
    Test Data 2: [13, 2, 3 ]
    Expected Result 2: [0, 2, 3 ]
    Test Data 3:[13, 13, 13 , 13, 13]
    Expected Result 3: [0, 0, 0, 0, 0]
         */

    public static int[] no13(int[] arr){
        int [] result = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            if(arr[i] == 13){
                result[i] = 0;
            }else{
                result[i] = arr[i];
            }
        }
        return result;
    }

    /*
    Requirement:
    -Create a method called arrFactorial()
    -This method will take an int array as argument, and it will return the
    array with every number replaced with their factorials.
    NOTE: If given array is empty, then return it back empty.
    NOTE: 0! equals to 1
    Test Data 1: [1, 2, 3, 4]
    Expected Result 1: [1, 2, 6, 24]
    Test Data 2: [0, 5]
    Expected Result 2: [1, 120]
    Test Data 3:[5 , 0, 6]
    Expected Result 3: [120, 1, 720]
    Test Data 4:[]
    Expected Result 4: []
     */
    public static int[] arrFactorial(int[] arr){
        int[] result = new int[arr.length];
        if(arr.length == 0){
            return result;
        }
        for (int i = 0; i < arr.length; i++) {
            result[i] = factorial(arr[i]);
        }
        return result;
    }

    public static int factorial(int n){
        if(n == 0) return 1;
        int fact = 1;
        for (int i = 1; i <= n; i++) {
            fact *= i;
        }
        return fact;
    }

    /*
    Requirement:
    -Create a method called categorizeCharacters()
    -This method will take String as an argument and return a String array as
    letters at index of 0, digits at index of 1 and specials at index of 2.
    NOTE: IGNORE SPACES
    NOTE: Assume length will always be more than zero.
    Test Data 1: “     ”
    Expected Result 3: [ , , ]
    Test Data 2: “abc123$#%”
    Expected Result 2: [abc, 123, $#%]
    Test Data 3: “12ab$%3c%”
    Expected Result 3: [abc, 123, $%%]
     */

    public static String[] categorizeCharacters(String str){
        String[] result = new String[3];
        result[0] = "";
        result[1] = "";
        result[2] = "";

        for (int i = 0; i < str.length(); i++) {
            char ch = str.charAt(i);
            if (Character.isLetter(ch)) {
                result[0] += ch;
            } else if (Character.isDigit(ch)) {
                result[1] += ch;
            } else if (!Character.isWhitespace(ch)) {
                result[2] += ch;
            }
        }

        return result;
    }

}
