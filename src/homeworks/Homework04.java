package homeworks;

import utilities.ScannerHelper;

import java.util.Locale;

public class Homework04 {
    public static void main(String[] args) {
        System.out.println("\n----------------Task1----------------\n");
        /*
        Requirement:
        Write a program that asks user to enter their names
        1. Print out the length of name
        2. Find the first character in the name and print it
        3. Find the last character in the name and print it
        4. Find the first 3 characters in the name and print them
        5. Find the last 3 characters in the name and print them
        6. Check if name starts with character A (or a) or not and print
        messages below
        If name starts with A or a, print “You are in the club!”
        If name does not start with A or a, print “Sorry, you are not in
        the club”
         */
        String name = ScannerHelper.getFirstName();
        int nameLen = name.length();
        System.out.println(nameLen);
        System.out.println(name.charAt(0));
        System.out.println(name.charAt(nameLen - 1));
        System.out.println(name.substring(0, 3));
        System.out.println(name.substring(nameLen - 3));
        if(name.charAt(0) == 'A' || name.charAt(0) == 'a'){
            System.out.println("You are in the club!");
        } else {
            System.out.println("Sorry, you are not in the club");
        }


        System.out.println("\n----------------Task2----------------\n");
        /*
        Requirement:
        Write a program that asks user to enter their full address
        Check if city is Chicago (ignore cases) in the address
        If city is Chicago, then print “You are in the club”
        If city is Des Plaines, then print “You are welcome to join to
        the club”
        If city is any other city, then print “Sorry, you will never be in
        the club”
         */

        String address = ScannerHelper.getAddress();
        if (address.contains("Chicago")){
            System.out.println( "You are in the club");
        }
        else if (address.contains("Des Plaines")) {
            System.out.println("You are welcome to join the club");
        }
        else{
            System.out.println("Sorry, you will never be in the club");
        }

        System.out.println("\n----------------Task3----------------\n");
        /*
        Requirement:
        Write a program that asks user to enter their fav country
        Check If the country contains “a” and “i” ignoring upper or
        lower cases.
        If the country contains “a”, then print “A is there”
        If the country contains “i”, then print “I is there”
        If country contains both “a” and “i”, then print “A and i are
        there”
        If country does not contain both “a” and “i”, then print “A and i
        are not there”
         */

        String favCountry = ScannerHelper.getFavCountry().toLowerCase();
        if(favCountry.contains("a")){
            System.out.println("A is there");
        } else if(favCountry.contains("i")){
            System.out.println("I is there");
        } else if(favCountry.contains("a") && favCountry.contains("i")){
            System.out.println("A and i are there");
        } else{
            System.out.println("A and i are not there");
        }


        System.out.println("\n----------------Task4----------------\n");
        /*
           Requirement:
        Assume that you are given below String
        String str = “   Java is FUN   ”;
        Create 3 new Strings and assign each word from str to those
        new Strings
        Finally print the results as below
         */
        String str = "   Java is FUN   ";
        str = str.trim();
        System.out.println("The first word in the str is = " + str.substring(0,4));
        System.out.println("The second word in the str is = " + str.substring(5,7));
        System.out.println("The third word in the str is = " + str.substring(8,11));

    }
}
