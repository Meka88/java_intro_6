package homeworks;

public class Homework01 {
    public static void main(String[] args) {
        System.out.println("\n--------------Task1--------------\n");
        /*
        JAVA = 01001010010000010101011001000001
        SELENIUM = 0101001101000101010011000100010101001110010010010101010101001101
         */
        System.out.println("\n--------------Task2--------------\n");
        /*
        01001101 = w
        01101000 = h
        01111001 = y
        01010011 = S
        01101100 = i
         */
        System.out.println("\n--------------Task3--------------\n");

        /*
        -Write a program that prints below sentences
        -Double quotations must be displayed in your console
        when you run the code
        -Every line below must be printed with a separate
        println statements

        I start to practice "JAVA" today, and I like it.
        The secret of getting ahead is getting started.
        "Don't limit yourself. "
        Invest in your dreams. Grind now. Shine later.
        It’s not the load that breaks you down, it’s the way you carry
        it.
        The hard days are what make you stronger.
        You can waste your lives drawing lines. Or you can live your
        life crossing them.
         */

        System.out.println("I start to practice \"JAVA\" today, and I like it.");
        System.out.println("The secret of getting ahead is getting started.");
        System.out.println("\"Don't limit yourself.\"");
        System.out.println("Invest in your dreams. Grind now. Shine later.");
        System.out.println("It's not the load that breaks you down, it's the way you carry");
        System.out.println("it.");
        System.out.println("The hard days are what make you stronger.");
        System.out.println("You can waste your lives drawing lines. Or you can live your");
        System.out.println("life crossing them.");


        System.out.println("\n--------------Task4--------------\n");

        /*
            Write a program that prints the whole below text in
        ONLY 1 println statement (you can use escape
        sequences to provide line or insert a tab)

             Java is easy to write and easy to run—this is the
        foundational strength of Java and why many developers
        program in it. When you write Java once, you can run it
        almost anywhere at any time.
             Java can be used to create complete applications
        that can run on a single computer or be distributed
        across servers and clients in a network.
              As a result, you can use it to easily build mobile
        applications or run-on desktop applications that use
        different operating systems and servers, such as Linux
        or Windows.
         */

        System.out.println("\tJava is easy to write and easy to run—this is the" +
                "\nfoundational strength of Java and why many developers" +
                "\nprogram in it. When you write Java once, you can run it" +
                "\nalmost anywhere at any time." +
                "\n\n\tJava can be used to create complete applications" +
                "\nthat can run on a single computer or be distributed" +
                "\nacross servers and clients in a network." +
                "\n\n\tAs a result, you can use it to easily build mobile" +
                "\napplications or run-on desktop applications that use" +
                "\ndifferent operating systems and servers, such as Linux" +
                "\nor Windows.");


        System.out.println("\n--------------Task5--------------\n");

        int myAge = 34;
        System.out.println("I am " + myAge + " years old.");

        int myFavoriteNumber = 13;
        System.out.println("My favorite number is " + myFavoriteNumber + ".");

        double myHeight = 5.6;
        System.out.println("My height is " + myHeight + ".");

        int myWeight = 130;
        System.out.println("I am " + myWeight + " lb.");

        char myFavoriteLetter = 'L';
        System.out.println("My favorite letter is " + myFavoriteLetter + ".");
    }
}
