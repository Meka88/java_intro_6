package homeworks;



import java.util.Arrays;

import static sun.nio.cs.Surrogate.MAX;


public class Homework19 {
    public static void main(String[] args) {

        System.out.println("\n--------------Task1--------------\n");

        int[] arr = {1, 5, 10};
        boolean isEven = true;
        System.out.println(sum(arr, isEven));

        int[] arr1 = {3, 7, 2, 5, 10};
        boolean isEven1 = false;
        System.out.println(sum(arr1, isEven1));

        System.out.println("\n--------------Task2--------------\n");

        System.out.println(nthChars("Java", 2));
        System.out.println(nthChars("JavaScript", 5));
        System.out.println(nthChars("Java", 3));
        System.out.println(nthChars("Hi", 4));

        System.out.println("\n--------------Task3--------------\n");

        System.out.println(canFormString("Hello", "Hi"));
        System.out.println(canFormString("halogen", "hello"));
        System.out.println(canFormString("programming", "gaming"));
        System.out.println(canFormString("CONVERSATION", "voices rant on"));

        System.out.println("\n--------------Task4--------------\n");

        System.out.println(isAnagram("Apple", "Peach"));
        System.out.println(isAnagram("listen", "silent"));
        System.out.println(isAnagram("astronomer", "moon starer"));
        System.out.println(isAnagram("CINEMA", "iceman"));

    }

    /** Task 1
     * Requirement:
     * Write a method called as sum() that takes an int array and a
     * boolean  and returns the sum of the numbers positioned at
     * even-odd indexes based on boolean value.
     * NOTE: if the boolean value is true, the method should
     * return the sum of the elements that have even
     * indexes. If the boolean value is false, the method
     * should return the sum of the elements that have odd
     * indexes.
     * Test Data 1:
     * ([1, 5, 10], true)
     * Expected Output 1:
     * 11
     * Test Data 2:
     * ([3, 7, 2, 5, 10], false)
     * Expected Output 2:
     * 12
     */

    public static int sum(int[] arr, boolean isEven){
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
           if((isEven && i % 2 == 0) || (!isEven && i % 2 != 0)){
               sum += arr[i];
           }
        }
        return sum;
    }
    /** Task 2
     * Requirement:
     * Write a method called as nthChars() that takes a String and
     * an int arguments and returns the String back with every nth
     * characters.
     * NOTE: your method should return empty String if the length of
     * given String is less than the given number.
     * Test Data 1:
     * (”Java", 2)
     * Expected Output 1:
     * “aa”
     * Test Data 2:
     * (”JavaScript", 5)
     * Expected Output 2:
     * “St”
     * Test Data 3:
     * (”Java", 3)
     * Expected Output 3:
     * “v”
     * Test Data 4:
     * (”Hi", 4)
     * Expected Output 4:
     * “”
     */

    public static String nthChars(String str, int x){
        if(str.length() < x) {
            return "";
        }
        StringBuilder result = new StringBuilder();
        for (int i = x - 1; i < str.length(); i+=x) {
            result.append(str.charAt(i));
        }
        return result.toString();
    }

    /** Task 3
     * Requirement:
     * Write a method canFormString() that takes 2 String
     * arguments and returns true if the second String can be
     * formed by rearranging the characters of first String. Return
     * false otherwise.
     * NOTE: This method is case-insensitive and should ignore the
     * white spaces!
     * Test Data 1:
     * (“Hello”, “Hi”)
     * Expected Output 1:
     * false
     * Test Data 2:
     * (“halogen”, “hello”)
     * Expected Output 2:
     * false
     * Test Data 3:
     * (“programming”, “gaming”)
     * Expected Output 3:
     * true
     * Test Data 4:
     * (“CONVERSATION”, “voices
     * rant on”)
     * Expected Output 4:
     * true
     */

    public static boolean canFormString(String str1, String str2){
        str1 = str1.replaceAll("\\s", "").toLowerCase();
        str2 = str2.replaceAll("\\s", "").toLowerCase();

        int[] count = new int[MAX];
        char []str3 = str1.toCharArray();
        for (int i = 0; i < str3.length; i++)
            count[str3[i]]++;

        // Now traverse through str2 to check
        // if every character has enough counts

        char []str4 = str2.toCharArray();
        for (int i = 0; i < str4.length; i++) {
            if (count[str4[i]] == 0)
                return false;
            count[str4[i]]--;
        }

        return true;
    }

    /** Task 4
     * Requirement:
     * Write a method isAnagram() that takes 2 String arguments and
     * returns true if the given Strings are anagram. Return false otherwise.
     * NOTE:An anagram is a word or phrase formed by rearranging the
     * letters of another word or phrase. In the context of strings, checking if
     * two strings are anagrams of each other means verifying if they
     * contain the same characters in the same quantities, regardless of the
     * order of those characters.
     * NOTE: This method is case-insensitive and should ignore the white
     * spaces!
     * Test Data 1:
     * (“Apple”, “Peach”)
     * Expected Output 1:
     * false
     * Test Data 2:
     * (“listen”, “silent”)
     * Expected Output 2:
     * true
     * Test Data 3:
     * (“astronomer”, “moon
     * starer”)
     * Expected Output 3:
     * true
     * Test Data 4:
     * (“CINEMA”, “iceman”)
     * Expected Output 4:
     * true
     */

    public static boolean isAnagram(String str1, String str2){
        str1 = str1.replaceAll("\\s", "").toLowerCase();
        str2 = str2.replaceAll("\\s", "").toLowerCase();

        if (str1.length() != str2.length()) {
            return false;
        }

        char[] charArray1 = str1.toCharArray();
        char[] charArray2 = str2.toCharArray();

        Arrays.sort(charArray1);
        Arrays.sort(charArray2);
        for (int i = 0; i < charArray1.length; i++) {
            if(charArray1[i] != charArray2[i]){
                return false;
            }
        }
        return true;
    }
}
