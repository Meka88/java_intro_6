package homeworks;

import java.util.Arrays;

public class Homework20 {
    public static void main(String[] args) {
        System.out.println("\n--------------Task1--------------\n");

        int[] arr = {1, 5, 10};
        boolean isEven = true;
        System.out.println(sum(arr, isEven));

        int[] arr1 = {3, 7, 2, 5, 10};
        boolean isEven1 = false;
        System.out.println(sum(arr1, isEven1));

        System.out.println("\n--------------Task2--------------\n");

        System.out.println(sumDigitsDouble("Java"));
        System.out.println(sumDigitsDouble("ab12"));
        System.out.println(sumDigitsDouble("23abc45"));
        System.out.println(sumDigitsDouble("Hi-23"));

        System.out.println("\n--------------Task3--------------\n");

        System.out.println(countOccurrence("Hello", "World"));
        System.out.println(countOccurrence("Hello", "l"));
        System.out.println(countOccurrence("Can I can a can", "anc"));
        System.out.println(countOccurrence("IT conversations", "IT"));

    }

    /** Task 1
     * Requirement:
     * Write a method called as sum() that takes an int array and a
     * boolean  and returns either the count of even or odd
     * numbers based on boolean value.
     * NOTE: if the boolean value is true, the method should
     * return the count of the even elements. If the boolean
     * value is false, the method should return the count of
     * the odd elements.
     * Test Data 1:
     * ([1, 5, 10], true)
     * Expected Output 1:
     * 1
     * Test Data 2:
     * ([3, 7, 2, 5, 10], false)
     * Expected Output 2:
     * 3
     */

    public static int sum(int[] arr, boolean isEven){
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            if((isEven && arr[i] % 2 == 0) || (!isEven && arr[i] % 2 != 0)){
                count++;
            }
        }
        return count;
    }

    /** Task 2
     *
     Requirement:
     Write a method called as sumDigitsDouble() that takes a
     String and returns the sum of the digits in the given String
     multiplied by.
     NOTE: your method should return -1 if the given String does
     not have any digits. Ignore negative numbers.
     Test Data 1:
     ("Java")
     Expected Output 1:
     -1
     Test Data 2:
     ("ab12")
     Expected Output 2:
     6
     Test Data 3:
     ("23abc45")
     Expected Output 3:
     28
     Test Data 4:
     (”Hi-23")
     Expected Output 4:
     10
     */

    public static int sumDigitsDouble(String input) {
        int sum = 0;
        boolean hasDigits = false;

        for (int i = 0; i < input.length(); i++) {
            char ch = input.charAt(i);
            if (Character.isDigit(ch)) {
                hasDigits = true;
                sum += Character.getNumericValue(ch);
            }
        }
        return hasDigits ? sum * 2 : -1;
    }

    /** Task 3
     * Requirement:
     * Write a method countOccurrence() that takes 2 String
     * arguments and returns how many times that first String can
     * form the second String.
     * NOTE: This method is case-insensitive and should ignore the
     * white spaces!
     * Also ignore the position of the characters.
     * Test Data 1:
     * (“Hello”, “World”)
     * Expected Output 1:
     * 0
     * Test Data 2:
     * (“Hello”, “l”)
     * Expected Output 2:
     * 2
     * Test Data 3:
     * (“Can I can a can”, “anc”)
     * Expected Output 3:
     * 3
     * Test Data 4:
     * (“IT conversations”, “IT”)
     * Expected Output 4:
     * 2
     */

    public static int countOccurrence(String str1, String str2) {//
        int count = 0;
        char[] str2Arr = str2.toLowerCase().toCharArray();
        Arrays.sort(str2Arr);
        str1 = str1.toLowerCase();

        for (int i = 0; i <= str1.length()-str2.length(); i++) {
            char[] sub = str1.substring(i, i + str2.length()).toCharArray();
            Arrays.sort(sub);

            if(Arrays.equals(str2Arr, sub)){
                count++;
                i += str2.length()-1;
            }

        }
        return count;

    }

    // public static int countOccurrence(String str1, String str2) {
    //     // Remove white spaces and convert to lowercase for case-insensitive comparison
    //     str1 = str1.replaceAll("\\s", "").toLowerCase();
    //     str2 = str2.replaceAll("\\s", "").toLowerCase();

    //     int count = 0;
    //     int index = 0;

    //     while (index != -1) {
    //         index = str1.indexOf(str2, index);
    //         if (index != -1) {
    //             count++;
    //             index += str2.length(); // Move to the next position after the match
    //         }
    //     }
    //     return count;
    // }
}
