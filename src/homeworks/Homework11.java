package homeworks;

import utilities.ScannerHelper;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;


public class Homework11 {
    public static void main(String[] args) {

        System.out.println("\n--------------Task1--------------\n");

        System.out.println(noSpace("    Hello    "));
        System.out.println(noSpace(" Hello World   "));
        System.out.println(noSpace("Tech Global"));

        System.out.println("\n--------------Task2--------------\n");

        System.out.println(replaceFirstLast("A"));
        System.out.println(replaceFirstLast("Hello"));
        System.out.println(replaceFirstLast("Tech Global"));

        System.out.println("\n--------------Task3--------------\n");

        System.out.println(hasVowel(""));
        System.out.println(hasVowel("Java"));
        System.out.println(hasVowel("1234"));
        System.out.println(hasVowel("JAVA"));

        System.out.println("\n--------------Task4--------------\n");

        checkAge(2006);
        checkAge(2010);
        checkAge(1918);

        System.out.println("\n--------------Task5--------------\n");

        System.out.println(averageOfEdges(0, 0, 0));
        System.out.println(averageOfEdges(-2, -2, 10));
        System.out.println(averageOfEdges(10, 13, 20));

        System.out.println("\n--------------Task6--------------\n");

        System.out.println(Arrays.toString(noA(new String[]{"appium", "123", "ABC", "java"})));
        System.out.println(Arrays.toString(noA(new String[]{"java", "hello", "123", "xyz"})));
        System.out.println(Arrays.toString(noA(new String[]{"apple", "appium", "ABC", "Alex", "A"})));

        System.out.println("\n--------------Task7--------------\n");

        System.out.println(Arrays.toString(no3no5(new int[]{7, 4, 11, 23, 17})));
        System.out.println(Arrays.toString(no3no5(new int[]{3, 4, 5, 6})));
        System.out.println(Arrays.toString(no3no5(new int[]{10, 11, 12, 13, 14, 15})));

        System.out.println("\n--------------Task8--------------\n");

        int[] arr ={-10, -3, 0, 1};
        System.out.println(countPrimes(arr));

        int[] arr1 = {7, 4, 11, 23, 17};
        System.out.println(countPrimes(arr1));

        int[] arr2 = {41, 53, 19, 47, 67};
        System.out.println(countPrimes(arr2));
    }
    /*
    Task-1
    Requirement:
    -Create a method called noSpace()
    -This method will take one String argument, and it will
    return a new String with all spaces removed from the
    original String
    Test Data 1: “”
    Expected Result 1: “”
    Test Data 2: “Java”
    Expected Result 2: “Java”
    Test Data 3: “    Hello    ”
    Expected Result 3: “Hello”
    Test Data 4: “ Hello World   ”
    Expected Result 4: “HelloWorld”
    Test Data 5: “Tech Global”
    Expected Result 5: “TechGlobal”
     */

    public static String noSpace(String str){
        return str = str.replaceAll("\\s", "");
    }

    /*
    Task-2
    Requirement:
    -Create a method called replaceFirstLast()
    -This method will take one String argument, and it will return a new
    String with first and last characters replaced
    NOTE: if the length is less than 2, then return empty String
    NOTE: Ignore all before and after spaces (get actual String only)
    Test Data 1: “”
    Expected Result 1: “”
    Test Data 2: “A”
    Expected Result 2: “”
    Test Data 3: “    A       ”
    Expected Result 3: “”
    Test Data 4: “Hello”
    Expected Result 4: “oellH”
    Test Data 5: “Tech Global”
    Expected Result 5: “lech GlobaT”
     */

    public static String replaceFirstLast(String str){
        if(str.length() < 2) return "";
        return (str.substring(str.length()-1) + str.substring(1, str.length()-1) + str.substring(0, 1));
    }

    /*
    Task-3
    Requirement:
    -Create a method called hasVowel()
    -This method will take one String argument, and it will return
    a boolean checking if String has any vowel or not
    NOTE: Vowels are = a, e, o, u, I
    NOTE: Ignore cases
    Test Data 1: “”
    Expected Result 1: false
    Test Data 2: “Java”
    Expected Result 2: true
    Test Data 3: “1234”
    Expected Result 3: false
    Test Data 4: “ABC”
    Expected Result 4: true
     */

    public static boolean hasVowel(String str){
        String  str1 = str.toLowerCase();
        boolean hasVowel = false;
        for (int i = 0; i < str1.length(); i++) {
            if(str1.charAt(i) == 'a' || str1.charAt(i) == 'e' || str1.charAt(i) == 'i' || str1.charAt(i) == 'o' || str1.charAt(i) == 'u') hasVowel = true;
        }
        return hasVowel;
    }

    /*
    Task-4
    Requirement:
    -Create a method called checkAge()
    -This method will take an int yearOfBirth as  argument, and it will print
    message below based on the entry
    If the age is less than 16, then print “AGE IS NOT ALLOWED”
    If the age is 16 or more, then print “AGE IS ALLOWED”
    If the age is more than 100 or a future year entered, print “AGE IS
    NOT VALID”
    NOTE: Calculate the age taking base year as current year in a
    dynamic way. You can use Date class.
    Test Data 1: 2010
    Expected Result 1: AGE IS NOT ALLOWED
    Test Data 2: 2006
    Expected Result 2: AGE IS ALLOWED
    Test Data 3: 2050
    Expected Result 3: AGE IS NOT VALID
    Test Data 4: 1920
    Expected Result 4: AGE IS NOT VALID
    Test Data 5: 1800
    Expected Result 5: AGE IS NOT VALID
     */

    public static void checkAge(int yearOfBirth){
        LocalDate today = LocalDate.now();
        LocalDate birthDate = LocalDate.of(yearOfBirth, 1, 1);
        Period period = Period.between(birthDate, today);
        int age = period.getYears();

        if(age < 16){
            System.out.println("AGE IS NOT ALLOWED");
        } else if(age >= 16 && age <= 100){
            System.out.println("AGE IS ALLOWED");
        } else System.out.println("AGE IS NOT VALID");
    }

    /*
    Task-5
    Requirement:
    -Create a method called averageOfEdges()
    -This method will take three int arguments, and it will return
    average of min and max numbers
    Test Data 1: 0, 0 ,0
    Expected Result 1: 0
    Test Data 2: 0, 0, 6
    Expected Result 2: 3
    Test Data 3: -2, -2, 10
    Expected Result 3: 4
    Test Data 4: -3, 15, -3
    Expected Result 4: 6
    Test Data 4: 10, 13, 20
    Expected Result 4: 15
     */

    public static int averageOfEdges(int a, int b, int c){

       return (Math.max(Math.max(a, b), c) + Math.min(Math.min(a, b), c)) / 2;
      // return average;
    }
//
//    public static double averageOfEdges(int a, int b, int c) {
//        int min = Math.min(Math.min(a, b), c);
//        int max = Math.max(Math.max(a, b), c);
//        return (double) (min + max) / 2;
//    }
    /*
    Task-6
    Requirement:
    -Create a method called noA()
    -This method will take a String array argument, and it
    will return a new array with all elements starting with A
    and  replace with “###”
    NOTE: Assume length will always be more than zero
    NOTE: Ignore cases
    Test Data 1: [“java”, “hello”, “123”, “xyz”]
    Expected Result 1: [“java”, “hello”, “123”, “xyz”]
    Test Data 2: [“appium”, “123”, “ABC”, “java”]
    Expected Result 2: [“###”, “123”, “###”, “java”]
    Test Data 3: [“apple”, “appium”, “ABC”, “Alex”, “A”]
    Expected Result 3: [“###”, “###”, “###”, “###”, “###”]
     */

    public static String[] noA(String[] arr){
        String[] result = new String[arr.length];
        for (int i = 0; i < arr.length; i++) {
            if (arr[i].toLowerCase().startsWith("a")) {
                result[i] = "###";
            } else {
                result[i] = arr[i];
            }
        }
        return result;
    }

    /*
    Task-7
    Requirement:
    -Create a method called no3or5()
    -This method will take an int array argument, and it will
    return a new array with some elements replaced as below
    If element can be divided by 5, replace it with 99
    If element can be divided by 3, replace it with 100
    If element can be divided by both 3 and 5, replace it with
    101
    NOTE: Assume length will always be more than zero
    Test Data 1: [7, 4, 11, 23, 17]
    Expected Result 1: [7, 4, 11, 23, 17]
    Test Data 2: [3, 4, 5, 6]
    Expected Result 2: [100, 4, 99, 100]
    Test Data 3: [10, 11, 12, 13, 14, 15]
    Expected Result 3:  [99, 11, 100, 13, 14, 101]
     */

    public static int[] no3no5(int[] arr){
        int[] result = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 3 == 0 && arr[i] % 5 == 0) {
                result[i] = 101;
            } else if (arr[i] % 5 == 0) {
                result[i] = 99;
            } else if (arr[i] % 3 == 0) {
                result[i] = 100;
            } else {
                result[i] = arr[i];
            }
        }
        return result;
    }

    /*
    Task-8
    Requirement:
    -Create a method called countPrimes()
    -This method will take an int array argument, and it will return
    how many elements in the array are prime numbers
    NOTE: Prime number is a number that can be divided only by 1
    and itself
    NOTE: Negative numbers cannot be prime
    Examples: 2,3,5,7,11,13,17,19,23,29,31,37 etc.
    NOTE: Smallest prime number is 2
    Test Data 1: [-10, -3, 0, 1]
    Expected Result 1: 0
    Test Data 2: [7, 4, 11, 23, 17]
    Expected Result 2: 4
    Test Data 3: [41, 53, 19, 47, 67]
    Expected Result 3:  5
     */

    public static int countPrimes(int[] arr) {
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > 1 && isPrime(arr[i])) {
                count++;
            }
        }
        return count;
    }

    public static boolean isPrime(int n) {
        if (n <= 1) {
            return false;
        }
        for (int i = 2; i <= Math.sqrt(n); i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }

}
