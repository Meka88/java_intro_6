package homeworks;

import utilities.ScannerHelper;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Homework08 {
    public static void main(String[] args) {
        System.out.println("\n--------------Task1--------------\n");

        System.out.println(countConsonants("Java"));
        System.out.println(countConsonants("hello"));

        System.out.println("\n--------------Task2--------------\n");

        System.out.println(Arrays.toString(wordArray("Hello, nice to meet you!!")));

        System.out.println("\n--------------Task3--------------\n");

        System.out.println(removeExtraSpaces("Hello,    nice to   meet     you!!"));

        System.out.println("\n--------------Task4--------------\n");

        String input = ScannerHelper.getSentence();
        System.out.println(count3OrLess(input));


        System.out.println("\n--------------Task5--------------\n");

        System.out.println(isDateFormatValid("01/21/1999"));

        System.out.println("\n--------------Task6--------------\n");

        System.out.println(isEmailFormatValid("abc@student.techglobal.com"));

    }
    /* Task1
    Requirement:
    Write a method named countConsonants() that takes a
    String as an argument a returns how many consonants
    are in the String.
    NOTE: You must use regex
    Example program1:
    String str = “hello”;
    Program: 3
    Example program2:
    String str = “JAVA”;
    Program: 2
    Example program2:
    String str = “”;
    Program: 0
     */
    public static int countConsonants(String str){

        /*
        int count = 0;
        Pattern pattern = Pattern.compile("[a-zA-Z&&[^aeiouAEIOU]]");

        for(char c : str.toCharArray()){
            Matcher matcher = pattern.matcher(String.valueOf(c));
            if(matcher.matches()) count++;
        }
        return count;*/
        //str = str.replaceAll("[^bcdfghjklmnpqrstvwxz]", "");
        //return str.length();

        str = str.replaceAll("[^a-zA-Z]", "");
        str = str.replaceAll("[aeoiuAEOIU]", "");
        return str.length();

    }

    /*Task2
    Requirement:
    Write a method named wordArray() that takes a String
    as an argument a returns a String array with all the
    words in the String
    NOTE: Make as dynamic as possible.
    Example program1:
    String str = “hello”;
    Program: [hello]
    Example program2:
    String str = “java  is    fun”;
    Program: [java, is, fun]
    Example program2:
    String str = “Hello, nice to meet you!!”;
    Program: [Hello, nice, to, meet, you]
     */

    public static String[] wordArray(String str1){

        str1 = str1.replaceAll("[ ]{1}", " ");// "[\\s+]"
        str1 = str1.replaceAll("[^a-zA-Z ]", "");
        return str1.split(" ");
    }

    /*Task3
    Requirement:
    Write a method named removeExtraSpaces() that takes
    a String as an argument a returns the String back with
    all extra spaces removed.
    NOTE: Make sure you use regex
    Example program1:
    String str = “hello”;
    Program: hello
    Example program2:
    String str = “java  is    fun”;
    Program: java is fun
    Example program2:
    String str = “Hello,    nice to   meet     you!!”;
    Program: Hello, nice to meet you!!
     */

    public static String removeExtraSpaces(String str2){
        return str2.replaceAll("\\s+ ", " ");
    }

    /*Task4
    Requirement:
    Write a method named count3OrLess() that asks the
    user to enter a sentence. Return a count of how many
    words are 3 characters long or less.
    NOTE: You must use regex
    Example program1:
    String str = “I go to TechGlobal”;
    Program: 3
    Example program2:
    String str = “Hi, my name is John Doe”;
    Program: 4
    Example program2:
    String str = “Hello guys”;
    Program: 0
     */

    public static int count3OrLess(String input){

        Pattern pattern = Pattern.compile("\\b\\w{1,3}\\b");
        Matcher matcher =  pattern.matcher(input);

        int counter = 0;

        while(matcher.find()) counter++;
        return counter;
    }

    /*Task5
    Requirement:
    Write a method named isDateFormatValid() that takes a
    String dateOfBirth as an argument and checks if the given
    date matches the given DOB requirements.
    This method would return a true or false boolean
    Format: nn/nn/nnnn
    Example program1:
    String dateOfBirth = “01/21/1999”;
    Program: true
    Example program2:
    String dateOfBirth = “1/20/1991”;
    Program: false
    Example program3:
    String dateOfBirth = “10/2/1991”;
    Program: false
    Example program4:
    String dateOfBirth = “12-20 2000”;
    Program: false
    Example program5:
    String dateOfBirth = “12/16/19500”;
    Program: false
     */

    public static boolean isDateFormatValid(String dateOfBirth){
        return Pattern.matches("\\d{2}/\\d{2}/\\d{4}",dateOfBirth);
    }

    /*Task6
    Requirement:
    Write a method named isEmailFormatValid() that takes a String
    email as an argument and checks if the given email matches
    the given email requirements.
    This method would return a true or false boolean.
    Format: <2+chars>@<2+chars>.<2+chars>
    NOTE: Do not use any type of loop. Use Regex
    Example program1:
    String email = “abc@gmail.com”;
    Program: true
    Example program2:
    String email = “abc@student.techglobal.com”;
    Program: true
    Example program3:
    String email = “a@gmail.com”;
    Program: false
    Example program4:
    String email = “abcd@@gmail.com”;
    Program: false
    Example program5:
    String email = “abc@gmail”;
    Program:false
     */

    public static boolean isEmailFormatValid(String email){
        return Pattern.matches("[\\w.#$-]{2,}@" + //before the @
                "[\\w.]{2,}\\." + //After the @ but before the .com
                "[\\w]{2,}", email); // after the .
    }
}
