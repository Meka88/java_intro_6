package homeworks;

import java.util.Scanner;

public class Homework02 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("\n--------------Task1--------------\n");

        System.out.println("Please enter first number: ");
        int firstNum = input.nextInt();

        System.out.println("Please enter the second number: ");
        int secondNum = input.nextInt();

        int sum = firstNum + secondNum;
        System.out.println("The sum of number 1 and 2 user entered is " + sum);

        System.out.println("\n--------------Task2--------------\n");

        System.out.println("Please enter first number: ");
        int num1 = input.nextInt();

        System.out.println("Please enter the second number: ");
        int num2 = input.nextInt();

        int product = num1 * num2;
        System.out.println("The product of the given 2 numbers is " + product);

        System.out.println("\n--------------Task3--------------\n");

        System.out.println("Please enter a number: ");
        double number1 = input.nextDouble();

        System.out.println("Please enter a second number: ");
        double number2 = input.nextDouble();

        System.out.println("The sum of the given numbers is " + (number1 + number2));
        System.out.println("The product of the given numbers is " + (number1 * number2));
        System.out.println("The subtraction of the given numbers is " + (number1 - number2));
        System.out.println("The division of the given numbers is " + (number1 / number2));
        System.out.println("The remainder of the given numbers is " + (number1 % number2));

        System.out.println("\n--------------Task4--------------\n");

        System.out.println("Result is " + (-10 + 7 * 5));
        System.out.println("Result is " + ((72 + 24) % 24));
        System.out.println("Result is " + (10 + -3 * 9 / 9));
        System.out.println("Result is " + (5 + 18 / 3 * 3 - (6 % 3)));

        System.out.println("\n--------------Task5--------------\n");

        System.out.println("Please enter a number: ");
        int a = input.nextInt();

        System.out.println("Please enter a second number: ");
        int b = input.nextInt();

        int average = (a + b) / 2;
        System.out.println("The average of the given numbers is " + average);

        System.out.println("\n--------------Task6--------------\n");

        System.out.println("Please enter a number 1: ");
        int x = input.nextInt();

        System.out.println("Please enter a number 2: ");
        int y = input.nextInt();

        System.out.println("Please enter a number 3: ");
        int z = input.nextInt();

        System.out.println("Please enter a number 4: ");
        int c = input.nextInt();

        System.out.println("Please enter a number 5: ");
        int d = input.nextInt();

        int average2 = (x + y + z + c + d) / 5;
        System.out.println("The average of the given numbers is " + average2);

        System.out.println("\n--------------Task7--------------\n");

        System.out.println("Please enter a number: ");
        int num3 = input.nextInt();

        System.out.println("Please enter next number: ");
        int num4 = input.nextInt();

        System.out.println("Please enter last number: ");
        int num5 = input.nextInt();

        System.out.println("The square of " + num3 + " is " + (num3 * num3));
        System.out.println("The square of " + num4 + " is " + (num4 * num4));
        System.out.println("The square of " + num5 + " is " + (num5 * num5));

        System.out.println("\n--------------Task8--------------\n");

        System.out.println("Please enter the side of the square: ");
        int sqSide = input.nextInt();

        int sqArea = sqSide * sqSide;
        int sqPerimeter = 4 * sqSide;
        System.out.println("Area of the square = " + sqArea);
        System.out.println("Perimeter of the square = " + sqPerimeter);

        System.out.println("\n--------------Task9--------------\n");

        double annual = 90000, triple = annual * 3;
        System.out.println("A Software Engineer in Test can earn $" + triple + " in 3 years.");

        System.out.println("\n--------------Task10--------------\n");

        System.out.println("Please enter your favorite book: ");
        String favBook = input.nextLine();

        System.out.println("Please enter your favorite color: ");
        String favColor = input.nextLine();

        System.out.println("Please enter your favorite number: ");
        int favNum = input.nextInt();

        System.out.println("User's favorite book is " + favBook + "." +
                "\nUser's favorite color is " + favColor + "." +
                "\nUser's favorite number is " + favNum + ".");

        System.out.println("\n--------------Task11--------------\n");

        System.out.println("Please enter your first name: ");
        String firstName = input.nextLine();


        System.out.println("Please enter your last name: ");
        String lastName = input.nextLine();

        System.out.println("Please enter your age: ");
        int age = input.nextInt();

        input.nextLine();

        System.out.println("Please enter your email address: ");
        String email = input.nextLine();

        System.out.println("Please enter your phone number: ");
        String phoneNumber = input.nextLine();

        System.out.println("Please enter your address: ");
        String address = input.nextLine();

        System.out.println("\tUser who joined the program is " + firstName + " " + lastName + ". " +
                firstName + "'s age is \n" + age + ". " + firstName + "'s email address is " + email +
                "," + " phone number is \n" + phoneNumber + ", and address is " + address + ".");
    }
}
