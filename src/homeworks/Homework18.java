package homeworks;

import java.util.Arrays;


public class Homework18 {
    public static void main(String[] args) {

        System.out.println("\n--------------Task1--------------\n");
        System.out.println(Arrays.toString(doubleOrTriple(new int[]{1, 5, 10}, true)));
        System.out.println(Arrays.toString(doubleOrTriple(new int[]{3, 7, 2}, false)));

        System.out.println("\n--------------Task2--------------\n");

        System.out.println(splitString("Java", 2));
        System.out.println(splitString("Javascript", 5));
        System.out.println(splitString("Hello", 3));

        System.out.println("\n--------------Task3--------------\n");

        System.out.println(countPalindrome("Mom and Dad"));
        System.out.println(countPalindrome("Kayak races attracts racecar drivers"));
    }

    /** TASK 1
     * Write a method called as doubleOrTriple() that takes an int
     * array and a boolean  and returns the array elements either
     * doubled or tripled based on boolean value.
     * NOTE: if the boolean value is true, the elements in the
     * array should be doubled. If the boolean value is false,
     * the elements should be tripled.
     * Test Data 1:
     * ([1, 5, 10], true)
     * Expected Output 1:
     * [2, 10, 20]
     * Test Data 2:
     * ([3, 7, 2], false)
     * Expected Output 2:
     * [3, 21, 6]
     */

    public static int[] doubleOrTriple(int[] arr, boolean isDouble){
        int[] result = new int[arr.length];

        for (int i = 0; i < arr.length; i++) {
            if(isDouble == true){
                result[i] = arr[i] * 2;
            } else {
                result[i] = arr[i] * 3;
            }
        }
        return result;
    }

    /** TASK 2
     * Write a method called as splitString() that takes a String and an int
     * arguments and returns the String back split by the given int.
     * NOTE: your method should return empty String if the length of given
     * String cannot be divided to given number.
     * Test Data 1:
     * (”Java", 2)
     * Expected Output 1:
     * “Ja va”
     * Test Data 2:
     * (”JavaScript", 5)
     * Expected Output 2:
     * “JavaS cript”
     * Test Data 3:
     * (”Hello", 3)
     * Expected Output 3:
     * “”
     */

    public static String splitString(String str, int n){
        if(str.length() % n != 0) return "";

        StringBuilder result = new StringBuilder();
        int start = 0;
        int end = n;
        while(end <= str.length()){
            String subStr = str.substring(start, end);
            result.append(subStr).append(" ");
            start = end;
            end += n;
        }
        return result.toString().trim();
    }

    /** TASK 3
     * Write a method countPalindrome() that takes String and
     * returns how many words in the String are palindrome words.
     * NOTE: A palindrome word is a word that reads the
     * same forwards and backwards. Example: level, radar,
     * deed, refer...
     * NOTE: This method is case-insensitive!
     * Test Data 1:
     * “Mom and Dad”
     * Expected Output 1:
     * 2
     * Test Data 2:
     * “Kayak races attracts racecar drivers”
     * Expected Output 2:
     * 2
     */

    public static int countPalindrome(String str){
        String[] splitStr = str.split(" ");
        int count = 0;

        for(String n : splitStr){
            if(isPalindrome(n)){
                count++;
            }
        }
        return count;
    }
    public static boolean isPalindrome(String str){
        str = str.toLowerCase();
        int left = 0;
        int right = str.length()-1;
        while(left < right){
            if(str.charAt(left) != str.charAt(right)){
                return false;
            }
            left++;
            right--;
        }
        return true;
    }

}
