package homeworks;

import java.util.Arrays;

public class Homework06 {
    public static void main(String[] args) {
        System.out.println("\n--------------Task1--------------\n");
        /*
        Requirement:
        -Create an int array having size of 10
        -Assign 23 to index of 2
        -Assign 12 to index of 4
        -Assign 34 to index of 7
        -Assign 7 to index of 9
        -Assign 15 to index of 6
        -Assign 89 to index of 0
        THEN:
        -Print element at index of 3
        -Print element at index of 0
        -Print element at index of 9
        -Print the entire array
        Expected Result:
        0
        89
        7
        [89, 0, 23, 0, 12, 0, 15, 34, 0, 7]
         */

        int[] numbers = new int[10];
        numbers[2] = 23;
        numbers[4] = 12;
        numbers[7] = 34;
        numbers[9] = 7;
        numbers[6] = 15;
        numbers[0] = 89;
        System.out.println(numbers[3]);
        System.out.println(numbers[0]);
        System.out.println(numbers[9]);
        System.out.println(Arrays.toString(numbers));


        System.out.println("\n--------------Task2--------------\n");
        /*
        Requirement:
        -Create a String array having size of 5
        -Assign “abc” to index of 1
        -Assign “xyz” to index of 4
        THEN:
        -Print element at index of 3
        -Print element at index of 0
        -Print element at index of 4
        -Print the entire array
        Expected Result:
        null
        null
        xyz
        [null, abc, null, null, xyz]
         */

        String[] strings = new String[5];
        strings[1] = "abc";
        strings[4] = "xyz";
        System.out.println(strings[3]);
        System.out.println(strings[0]);
        System.out.println(strings[4]);
        System.out.println(Arrays.toString(strings));

        System.out.println("\n--------------Task3--------------\n");
        /*
        Requirement:
        -Create an int array that stores numbers below
        23, -34, -56, 0, 89, 100
        THEN:
        -Print the entire array
        -Print the entire array sorted in ascending order
        Expected Result:
        [23, -34, -56, 0, 89, 100]
        [-56, -34, 0, 23, 89, 100
         */

        int[] numbers3 = {23, -34, -56, 0, 89, 100};
        System.out.println(Arrays.toString(numbers3));
        Arrays.sort(numbers3);
        System.out.println(Arrays.toString(numbers3));

        System.out.println("\n--------------Task4--------------\n");
        /*
        Requirement:
        -Create a String array that stores countries below
        Germany, Argentina, Ukraine, Romania
        THEN:
        -Print the entire array
        -Print the entire array sorted lexicographically
        Expected Result:
        [Germany, Argentina, Ukraine, Romania]
        [Argentina, Germany, Romania, Ukraine
         */

        String[] countries = {"Germany", "Argentina", "Ukraine", "Romania"};
        System.out.println(Arrays.toString(countries));
        Arrays.sort(countries);
        System.out.println(Arrays.toString(countries));

        System.out.println("\n--------------Task5--------------\n");
        /*
        Requirement:
        -Create a String array that stores cartoon dogs below
        Scooby Doo, Snoopy, Blue, Pluto, Dino, Sparky
        THEN:
        -Print the entire array
        -Then, check if it contains Pluto
        if it contains Pluto, then print true
        if it does not contain Pluto, print false
        Expected Result:
        [Scooby Doo, Snoopy, Blue, Pluto, Dino, Sparky]
        true
         */

        String[] dogs = {"Scooby Doo", "Snoopy", "Blue", "Pluto", "Dino", "Sparky"};
        System.out.println(Arrays.toString(dogs));
        Arrays.sort(dogs);
        System.out.println(Arrays.binarySearch(dogs, "Pluto") >= 0);
        // second way
        boolean hasPluto = false;
        for (String dog : dogs) {
            if(dog.equals("Pluto")){
                hasPluto = true;
                break;
            }
        }
        System.out.println(hasPluto);

        System.out.println("\n--------------Task6--------------\n");
        /*
        Requirement:
        -Create a String array that stores cartoon cats
        below
        Garfield, Tom, Sylvester, Azrael
        THEN:
        -Print the entire array sorted lexicographically
        -Then, check if it contains Garfield and Felix
        if it contains both, then print true
        if it does not contain both, print false
        Expected Result:
        [Azrael, Garfield, Sylvester, Tom]
        false
         */

        String[] cats = {"Garfield", "Tom", "Sylvester", "Azrael"};
        Arrays.sort(cats);
        System.out.println(Arrays.toString(cats));
        boolean hasGandF = false;
        for (String cat : cats) {
            if(cats.equals("Garfield") && cats.equals("Felix")){
                hasGandF = true;
                break;
            }
        }
        System.out.println(false);

        System.out.println("\n--------------Task7--------------\n");
        /*
        Requirement:
        -Create a double array that stores numbers below
        10.5, 20.75, 70, 80, 15.75
        THEN:
        -Print the entire array
        -Print each element
        Expected Result:
        [10.5, 20.75, 70.0, 80.0, 15.75]
        10.5
        20.75
        70.0
        80.0
        15.75
         */

        double[] numbers7 = {10.5, 20.75, 70, 80, 15.75};
        System.out.println(Arrays.toString(numbers7));
        for (double v : numbers7) {
            System.out.println(v);
        }


        System.out.println("\n--------------Task8--------------\n");
        /*
        Requirement:
        -Create a char array that stores characters below
        A, b, G, H, 7, 5, &, *, e, @, 4
        THEN:
        -Print the entire array
        -Print the total count of the letters
        -Print the total count of lowercase letters
        -Print the total count of uppercase letters
        -Print the total count of digits
        -Print the total count of special characters
        Expected Result:
        [A, b, G, H, 7, 5, &, *, e, @, 4]
        Letters = 5
        Uppercase letters = 3
        Lowercase letters = 2
        Digits = 3
        Special characters = 3
         */

        char[] chars = {'A', 'b', 'G', 'H', '7', '5', '&', '*', 'e', '@', '4'};

        System.out.println(Arrays.toString(chars));

        int lLetters = 0;
        int uLetters = 0;
        int digits = 0;
        int specChars = 0;

        for (char x : chars) {
            if(Character.isUpperCase(x)) uLetters++;
            else if(Character.isLowerCase(x)) lLetters++;
            else if(Character.isDigit(x)) digits++;
            else if(x != ' ') specChars++;
        }

        System.out.println("Letters = " + (uLetters + lLetters));
        System.out.println("Uppercase letters = " + uLetters);
        System.out.println("Lowercase letters = " + lLetters);
        System.out.println("Digits = " + digits);
        System.out.println("Special characters = " + specChars);

        System.out.println("\n--------------Task9--------------\n");
        /*
        Requirement:
        -Create a String array that stores objects below
        Pen, notebook, Book, paper, bag, pencil, Ruler
        THEN:
        -Print the entire array
        -Print how many elements starts with uppercase
        -Print how many elements starts with lowercase
        -Print how many elements starts with B or P, ignoring cases
        -Print how many elements has “book” or “pen” in it, ignoring
        cases
        Expected Result:
        [Pen, notebook, Book, paper, bag, pencil, Ruler]
        Elements starts with uppercase = 3
        Elements starts with lowercase = 4
        Elements starting with B or P = 5
        Elements having ”book” or “pen”= 4
         */

        String[] strArr = {"Pen", "notebook", "Book", "paper", "bag", "pencil", "Ruler"};
        int startsWithUpper = 0;
        int startsWithLower = 0;
        int startsWithBorP = 0;
        int haveBookOrPen = 0;

        for (String str : strArr) {
            if(Character.isUpperCase(str.charAt(0))) startsWithUpper++;
            else startsWithLower++;
            if(str.toLowerCase().charAt(0) == 'b' || str.toLowerCase().charAt(0) == 'p') startsWithBorP++;
            if(str.toLowerCase().contains("book") || str.toLowerCase().contains("pen")) haveBookOrPen++;
        }
        System.out.println(Arrays.toString(strArr));
        System.out.println("Elements starts with uppercase = " + startsWithUpper);
        System.out.println("Elements starts with lowercase = " + startsWithLower);
        System.out.println("Elements starts with B or P = " + startsWithBorP);
        System.out.println("Elements having ”book” or “pen”= " + haveBookOrPen);


        System.out.println("\n--------------Task10--------------\n");
        /*
        Requirement:
        -Create an int array that stores numbers below
        3, 5, 7, 10, 0, 20, 17, 10, 23, 56, 78
        THEN:
        -Print the entire array
        -Print how many elements are more than 10
        -Print how many elements are less than 10
        -Print how many elements are 10
        Expected Result:
        [3, 5, 7, 10, 0, 20, 17, 10, 23, 56, 78]
        Elements that are more than 10 = 5
        Elements that are less than 10 = 4
        Elements that are 10 = 2
         */

        int[] numArr = {3, 5, 7, 10, 0, 20, 17, 10, 23, 56, 78};
        int more10 = 0;
        int less10 = 0;
        int count10 = 0;

        for (int i : numArr) {
            if(i > 10) more10++;
            else if(i < 10) less10++;
            else count10++;
        }
        System.out.println(Arrays.toString(numArr));
        System.out.println("Elements that are more than 10 = " + more10);
        System.out.println("Elements that are less than 10 = " + less10);
        System.out.println("Elements that are 10 = " + count10);

        System.out.println("\n--------------Task11--------------\n");
        /*
        Requirement:
        -Create 2 int arrays that stores numbers below
        First -> 5, 8, 13, 1, 2
        Second -> 9, 3, 67, 1, 0
        THEN:
        -Print both arrays
        –Then, create a new array that will take greatest
        of same index from first 2 arrays
        -Print third array as well
        Expected Result:
        1st array is =  [5, 8, 13, 1, 2]
        2nd array is = [9, 3, 67, 1, 0]
        3rd array is =  [9, 8, 67, 1, 2]
         */

        int[] arr1 = {5, 8, 13, 1, 2};
        int[] arr2 = {9, 3, 67, 1, 0};
        int[] arr3 = new int[5];

        for(int i = 0; i < arr3.length; i++){
            arr3[i] = Math.max(arr1[i], arr2[i]);
        }
        System.out.println("1st array is = " + Arrays.toString(arr1));
        System.out.println("2nd array is = " + Arrays.toString(arr2));
        System.out.println("3rd array is = " + Arrays.toString(arr3));
    }
}
