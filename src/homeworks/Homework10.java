package homeworks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Homework10 {
    public static void main(String[] args) {
        System.out.println("\n--------------Task1--------------\n");

        System.out.println(countWords("Selenium is the most common UI automation tool.   "));

        System.out.println("\n--------------Task2--------------\n");

        System.out.println(countA("QA stands for Quality Assurance"));

        System.out.println("\n--------------Task3--------------\n");

        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(-45, 0, 0, 34, 5, 67));
        System.out.println(countPos(list));
        System.out.println(countPos(new ArrayList<>(Arrays.asList(-23, -4, 0, 2, 5, 90, 123))));

        System.out.println("\n--------------Task4--------------\n");

        System.out.println(removeDuplicateNumbers(new ArrayList<>(Arrays.asList(10, 20, 35, 20, 35, 60, 70, 60))));

        System.out.println("\n--------------Task5--------------\n");

        System.out.println(removeDuplicateElements(new ArrayList<>(Arrays.asList("java", "C#", "ruby", "JAVA", "ruby", "C#", "C++"))));

        System.out.println("\n--------------Task6--------------\n");

        System.out.println(removeExtraSpaces("   I   am      learning     Java      "));

        System.out.println("\n--------------Task7--------------\n");

        int[] arr1 =  {3, 0, 0, 7, 5, 10};
        int[] arr2 = {6, 3, 2};
        System.out.println(Arrays.toString(add(arr1, arr2)));

        System.out.println("\n--------------Task8--------------\n");

        int[] arr = {10, -13, 8, 12, 15, -20};
        System.out.println(findClosestTo10(arr));
    }

    /*
    Task 1
    Requirement:
    Write a method countWords() that takes a String as an
    argument, and returns how many words there are in the
    the given String
    Test data 1:
    String str = “      Java is fun       ”;
    Expected output 1:
    3
    Test data 2:
    String str = “Selenium is the most common UI automation tool.   ”;
    Expected output 2:
    8
    NOTE: Make your code dynamic that works for any
    given String
    NOTE: Be careful about empty String
    NOTE: Be careful about before and after white spaces
     */

    public static int countWords(String str){
        if(str == null || str.isEmpty()){
            return 0;
        } else{
            String[] arr = str.split("\\s+");
            return arr.length;
        }
        //return str.trim().split("[\\s]+").length;
    }

    /*
    Task 2
     Requirement:
    Write a method countA() that takes a String as an
    argument, and returns how many A or a there are in the the
    given String
    Test data 1:
    String str = “TechGlobal is a QA bootcamp”;
    Expected output 1:
    4
    Test data 2:
    String str = “QA stands for Quality Assurance”;
    Expected output 2:
    5
    NOTE: Make your code dynamic that works for any given
    String
    NOTE: Be careful about empty String
    NOTE: Be careful about uppercase and lowercase
     */

    public static int countA(String str){
        int count = 0;
        if(str == null || str.isEmpty()){
            return 0;
        } else {
            for (int i = 0; i < str.length(); i++) {
                if(str.toLowerCase().charAt(i) == 'a') count++;
            }
            return count;
        }
        //return str.replaceAll("[^aA]", "").length();
    }

    /*
    Task 3
    Requirement:
    Write a method countPos() that takes an ArrayList of
    Integer as an argument, and returns how many
    elements are positive
    Test data 1:
    [-45, 0, 0, 34, 5, 67]
    Expected output 1:
    3
    Test data 2:
    [-23, -4, 0, 2, 5, 90, 123]
    Expected output 2:
    4
    NOTE: Make your code dynamic that works for any
    given ArrayList of Integer
     */

    public static int countPos(ArrayList<Integer> list){
        int count = 0;
        for (Integer integer : list) {
            if(integer > 0) count++;
        }
        return count;
        //return list.stream().filter(e -> e > 0).count();
    }

    /*
    Task 4
    Requirement:
    Write a method removeDuplicateNumbers() that takes
    an ArrayList of Integer as an argument, and returns it
    back with removed duplicates
    Test data 1:
    [10, 20, 35, 20, 35, 60, 70, 60]
    Expected output 1:
    [10, 20, 35, 60, 70]
    Test data 2:
    [1, 2, 5, 2, 3]
    Expected output 2:
    [1, 2, 5, 3]
    NOTE: Make your code dynamic that works for any
    given ArrayList of Integer
     */

    public static ArrayList<Integer> removeDuplicateNumbers(ArrayList<Integer> list){
        ArrayList<Integer> newList = new ArrayList<>();
        for (Integer element : list) {
            if(!newList.contains(element)) newList.add(element);
        }
        return newList;
        // second way
        //List<Integer> newList = list.stream().distinct().collect(Collectors.toList());
        //return (ArrayList<Integer>) newList;
    }

    /*
    Task 5
    Requirement:
    Write a method removeDuplicateElements() that takes an
    ArrayList of String as an argument, and returns it back with
    removed duplicates
    Test data 1:
    [“java”, “C#”, “ruby”, “JAVA”, “ruby”, “C#”, “C++”]
    Expected output 1:
    [“java”, “C#”, “ruby”, “JAVA”,  “C++”]
    Test data 2:
    [“abc”, “xyz”, “123”, “ab”, “abc”, “ABC”]
    Expected output 2:
    [“abc”, “xyz”, “123”, “ab”, “ABC”]
    NOTE: Make your code dynamic that works for any given
    ArrayList of String
    NOTE: Be careful about lowercase and uppercase's, this
    method is case-sensitive
     */

    public static ArrayList<String> removeDuplicateElements(ArrayList<String> list){
        List<String> newList = list.stream().distinct().collect(Collectors.toList());
        return (ArrayList<String>) newList;
    }

    /*
    Task 6
    Requirement:
    Write a method removeExtraSpaces() that takes a String as
    an argument, and returns a String with removed extra spaces
    Test data 1:
    String str = “   I   am      learning     Java      ”;
    Expected output 1:
    I am learning Java
    Test data 2:
    String str = “Java  is fun    ”;
    Expected output 2:
    Java is fun
    NOTE: Make your code dynamic that works for any given
    String
    NOTE: Be careful about empty String
    NOTE: Be careful about before and after white
     */

    public static String removeExtraSpaces(String str){
        return str.replaceAll("\\s+", " ").trim();
    }

    /*
    Task 7
    Requirement:
    Write a method add() that takes 2 int[] arrays as arguments and
    returns a new array with sum of given arrays elements.
    Test data 1:
    int[] arr1 = {3, 0, 0, 7, 5, 10};
    int[] arr2 = {6, 3, 2};
    Expected output 1:
    [9, 3, 2, 7, 5, 10]
    Test data 2:
    int[] arr1 =  {10, 3, 6, 3, 2};
    int[] arr2 = {6, 8, 3, 0, 0, 7, 5, 10, 34};
    Expected output 1:
    [16, 11, 9,  3, 2, 7, 5, 10, 34]
    NOTE: Make your code dynamic that works for any given arrays
    NOTE: You will assume that length of arrays are always more than
    zero
    NOTE: Be careful about arrays length, if they were equal, it would
    be an easy task, but they can be different as given in the example
    above.
     */

    public static int[] add(int[] x, int[] y){

        int[] z = new int[Math.max(x.length, y.length)];

        for (int i = 0; i < z.length; i++) {
            if(x.length == y.length) z[i] = x[i] + y[i];
            if (x.length >= y.length){
                    if (i < y.length) {
                        z[i] = x[i] + y[i];
                    }
                    else{
                    z[i] = x[i];
                }
            } else {
                    if (i < x.length) {
                        z[i] = x[i] + y[i];
                    }
                    else {
                        z[i] = y[i];
                    }
                }
            }
        return z;
        /*for (int i = 0; i < Math.min(arr1.length, arr2.length); i++) {
            if(arr1.length> arr2.length) arr1[i] += arr2[i];
            else arr2[i] += arr1[i];
        }

        return arr1.length > arr2.length ? arr1 : arr2;*/
    }

    /*
    Task 8
    Requirement:
    Write a method findClosestTo10() that takes an int[] array as
    an argument, and returns the closest element to 10 from given
    array
    Test data 1:
    int[] numbers = {10, -13, 5, 70, 15, 57};
    Expected output 1:
    5
    Test data 2:
    int[] numbers = {10, -13, 8, 12, 15, -20};
    Expected output 2:
    8
    NOTE: You will assume that length of array is always more than
    zero
    NOTE: Make your code dynamic that works for any given int[]
    array
    NOTE: Be careful about element to be 10 (ignore it)
    NOTE: Be careful about 2 numbers to be closest ( 8 and 12) in
    this case return smallest which is 8
     */

    public static int findClosestTo10(int[] arr){
        Arrays.sort(arr);
        int i = 0, j = arr.length, mid = 0;
        while(i < j){
            mid = (i + j) / 2;
            if(arr[mid] == 10) return arr[mid - 1];
            if(10 < arr[mid]){
                if(mid > 0 && 10 > arr[mid - 1]) Math.min(arr[mid], arr[mid-1]);
                j = mid;
            }
            else{
                if(mid < j -1 && 10 < arr[mid+1]) Math.min(arr[mid], arr[mid+1]);
                i = mid + 1;
            }
        }
        return arr[mid];

        /*
        Arrays.sort(arr);
        int closest = Integer.MAX_VALUE;
        for (int number : arr) {
            if (number != 10 && Math.abs(number - 10) < Math.abs(closest - 10)) {
                closest = number;
            }
        }
        return closest;
         */
    }

}
