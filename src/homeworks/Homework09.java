package homeworks;


import java.util.*;

public class Homework09 {
    public static void main(String[] args) {
        System.out.println("\n--------------Task1--------------\n");

        int[] arr = {-8, 56, 7, 8, 65};
        System.out.println(firstDuplicatedNumber(arr));

        System.out.println("\n--------------Task2--------------\n");

        String[] words = {"z", "abc", "Z", "123", "#"};
        System.out.println(firstDuplicatedString(words));

        System.out.println("\n--------------Task3--------------\n");

        int[] numbers = new int[]{1, 2, 5, 0, 7};
        System.out.println(duplicatedNumbers(numbers));

        System.out.println("\n--------------Task4--------------\n");

        String[] myArr = {"A", "foo", "bar", "Foo", "12", "a", "java"};
        System.out.println(duplicatedStrings(myArr));

        System.out.println("\n--------------Task5--------------\n");

        String[] words1 = {"java", "python", "ruby"};
        System.out.println(Arrays.toString(words1));

        System.out.println("\n--------------Task6--------------\n");

        String str4 = "Today is a fun day";
        System.out.println(reverseStringWords(str4));

    }

    /* TASK1
        Requirement:
        Write a method called as firstDuplicatedNumber to find the first
        duplicated number in an int array.
        NOTE: Make your code dynamic that works for any given int array
        and return -1 if there are no duplicates in the array.
        Test data 1:
        int[] numbers = {-4, 0, -7, 0, 5, 10, 45, 45};

        Expected output 1:
        0
        Test data 2:
        int[] numbers = {-8, 56, 7, 8, 65};

        Expected output 2:
        -1
        Test data 3:
        int[] numbers = {3, 4, 3, 3, 5, 5, 6, 6, 7};

      public static int firstDuplicatedNumber(int[] arr){
        int min = -1;
        HashSet<Integer> set = new HashSet<>();

        for (int i = arr.length-1; i >= 0 ; i--) {
            if(set.contains(arr[i])) min = i;
            else set.add(arr[i]);
        }
        if(min != -1) return arr[min];
        else return -1;
    }
    /*
     */

    public static int firstDuplicatedNumber(int[] arr){
        ArrayList<Integer> container = new ArrayList<>();

        for (int i : arr) {
            if(container.contains(i)){
                return i;
            }else  container.add(i);
        }
        return -1;
    }

    /* TASK 2
        Requirement:
        Write a method called as firstDuplicatedString to find the first duplicated
        String in a String array, ignore cases.
        NOTE: Make your code dynamic that works for any given String array and
        return “There is no duplicates” if there are no duplicates in the array.
        Test data 1:
        String[] words = {“Z”, “abc”, “z”, “123”, “#” };

        Expected output 1:
        Z
        Test data 2:
        String[] words = {“xyz”, “java”, ”abc”};

        Expected output 2:
        There is no duplicates
        Test data 3:
        String[] words = {“a”, “b”, “B”, “XYZ”, “123”};

        Expected output 2:
        b
         */

    public static String firstDuplicatedString(String[] str){
        for (int i = 0; i < str.length; i++) {
            for (int j = i + 1; j < str.length; j++) {
                if (str[i].equalsIgnoreCase(str[j])) return str[i];
            }
        }
        return "There is no duplicates";
    }

        /* TASK 3
        Requirement:
        Write a method called as duplicatedNumbers to find the
        all duplicates in an int array.
        NOTE: Make your code dynamic that works for any
        given int array and return empty ArrayList if there are
        no duplicates in the array.
        Test data 1:
        int[] numbers = {0, -4, -7, 0, 5, 10, 45, -7, 0};

        Expected output 1:
        [0, -7]
        Test data 2:
        int[] numbers = {1, 2, 5, 0, 7};

        Expected output 2:
        []
         */

    public static ArrayList<Integer> duplicatedNumbers(int[] arr){
        ArrayList<Integer> list = new ArrayList<>();

        for (int i = 0; i < arr.length; i++) {
            for (int j = i+1; j < arr.length; j++) {
                if(arr[i] == arr[j] && i != j) list.add(arr[i]);
            }
        }
        return list;
    }

        /* TASK 4
        Requirement:
        Write a method called as duplicatedStrings to find the all
        duplicates in a String array, ignore cases.
        NOTE: Make your code dynamic that works for any given
        String array and return and empty ArrayList if there are
        no duplicates in the array.
        Test data 1:
        String[] words = {“A”, “foo”, “12” , “Foo”, “bar”, “a”, “a”,
        “java”};

        Expected output 1:
        [A, foo]
        Test data 2:
        String[] words = {“python”, “foo”, “bar”, “java”, “123” };

        Expected output 2:
        []
         */

    public static ArrayList<String> duplicatedStrings(String[] str){
        ArrayList<String> list = new ArrayList<>();

        for (int i = 0; i < str.length; i++) {
            for (int j = i+1; j < str.length; j++) {
                if (str[i].equalsIgnoreCase(str[j])) list.add(str[i]);
            }
        }
        return list;
    }


        /* TASK5
        Requirement:
        Write a method called as reversedArray to reverse
        elements in an array, then print array.
        NOTE: Make your code dynamic that works for any
        given array.
        Test data 1:
        String[] words1 = {“abc”, “foo”, “bar”};

        Expected output 1:
        [bar, foo, abc]
        Test data 2:
        String[] words2 = {“java”, “python”, “ruby”};

        Expected output 2:
        [ruby, python, java]
         */

    public static String[] reversedArray(String[] arr){
        List<String> list = Arrays.asList(arr);
        Collections.reverse(list);

        return list.toArray(arr);
    }


    /* TASK6
        Requirement:
        Write a method called as reverseStringWords to reverse
        each word in a given String
        NOTE: Make your code dynamic that works for any
        given String.
        Test data 1:
        String str = “Java is fun”;

        Expected output 1:
        avaJ si nuf
        Test data 2:
        String str = “Today is a fun day”;

        Expected output 2:
        yadoT si a nuf yad
     */

    public static String reverseStringWords(String str){
        String[] arrStr = str.split("\\s");
        String reversed = "";
        for (String s : arrStr) {
            reversed += new StringBuilder(s).reverse() + " ";
        }
        return reversed.trim();
    }


}


/*
////////////////////////////////TASK01/////////////////////////////
    public static int firstDuplicatedNumber(int[] arr){
        ArrayList<Integer> container = new ArrayList<>();

        for (int i : arr) {
            if(container.contains(i)){
                return i;
            }else  container.add(i);
        }
        return -1;
    }

    ////////////////////////////////TASK02/////////////////////////////
    public static String firstDuplicatedString (String[] arr){
        ArrayList<String> container = new ArrayList<>();

        for (String s : arr) {
            if(container.contains(s.toLowerCase())){
                return s;
            }else  container.add(s.toLowerCase());
        }
        return "There is no duplicates";
    }
    ////////////////////////////////TASK03/////////////////////////////
    public static ArrayList<Integer> duplicatedStrings(int[] arr){
        ArrayList<Integer> dupContainer = new ArrayList<>();

        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if(arr[i] == arr[j] && !dupContainer.contains(arr[i])) dupContainer.add(arr[i]);
            }
        }
        return dupContainer;
    }
    ////////////////////////////////TASK04/////////////////////////////

    public static ArrayList<String> duplicatedStrings(String[] arr){
        ArrayList<String> dupContainer = new ArrayList<>();
        ArrayList<String> solution = new ArrayList<>();

        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if(arr[i].equalsIgnoreCase(arr[j]) && !dupContainer.contains(arr[i].toLowerCase())){

                    dupContainer.add(arr[i].toLowerCase());
                    solution.add(arr[i]);

                }
            }
        }
        return solution;
    }

    ////////////////////////////////TASK05/////////////////////////////

    public static void reversedArray(String[] arr){
        /*
        WAY: 1 -> Max Iteration
        ArrayList<String> reversedList = new ArrayList<>();
        for (int i = arr.length-1; i >= 0 ; i--) {
            reversedList.add(arr[i]);
        }
        System.out.println(reversedList);
         */

        /*
        Second way:
        arr = a, b, c, d, e, f
        i: 0
        startingIndex: 0 1 2 3
        endingIndex: 5 4 3 2
        Storage: a, b, c
        first i: f, b, c, d, e, a
        sec i:   f, e, c, d, b, a
        third i: f, e, d, c, b, a
        int startingIndex = 0;
        int endingIndex = arr.length-1;
        String storage;
        while(startingIndex < endingIndex){
            storage = arr[startingIndex];
            arr[startingIndex] = arr[endingIndex];
            arr[endingIndex] = storage;
            startingIndex++;
            endingIndex--;
        }
        System.out.println(Arrays.toString(arr));
         */
/*
Way 3:
        Collections.reverse(Arrays.asList(arr));
                System.out.println(Arrays.toString(arr));

                }

////////////////////////////////TASK06/////////////////////////////

public static String reveredString(String str){
        String reveredStr = "";

        String[] strAsArr = str.trim().split("\\s+");

        for (String s : strAsArr) {
        reveredStr += new StringBuffer(s).reverse() + " ";

        }

        return reveredStr.trim();

        }
 */