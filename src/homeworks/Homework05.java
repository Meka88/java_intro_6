package homeworks;

import utilities.ScannerHelper;

public class Homework05 {
    public static void main(String[] args) {
        System.out.println("\n--------------Task1--------------\n");
        /*
        Write a program that prints all the numbers that are
        dividable by 7 starting from 1 to 100 (1 and 100 are
        included)
        NOTE: Result must be in one line with space and
        dash separators
        Expected Output:
        7 – 14 – 21 - 28 . . . 77 - 84 - 91 - 98
         */

        for(int i = 1; i <= 100; i++){
            if(i % 7 == 0) System.out.print(i + " - ");
        }


        System.out.println("\n--------------Task2--------------\n");
        /*
        Write a program that prints all the numbers that are
        dividable by both 2 and 3 starting from 1 to 50 (1 and
        50 are included)
        NOTE: Result must be in one line with space and
        dash separators
        Expected Output:
        6 – 12 – 18 . . . 36 – 42 - 48
         */

        for(int i = 1; i <= 50; i++){
            if(i % 2 == 0 && i % 3 == 0) System.out.print(i + " - ");
        }


        System.out.println("\n--------------Task3--------------\n");
        /*
        Write a program that prints all the numbers that are
        dividable by 5 starting from 100 to 50 (100 and 50 are
        included)
        NOTE: Result must be in one line with space and
        dash separators
        Expected Output:
        100 – 95 – 90 . . . - 60 - 55 - 50
         */

        for (int i = 100; i >= 50; i--){
            if(i % 5 == 0) System.out.print(i + " - ");
        }

        System.out.println("\n--------------Task4--------------\n");
        /*
        Write a program that prints the squares of all numbers
        starting from 0 to 7 (0 and 7 are included)
        NOTE: Use loop!!!
        Expected Output:
        The square of 0 is =  0
        The square of 1 is =  1
        The square of 2 is =  4
        The square of 3 is =  9
        The square of 4 is =  16
        The square of 5 is =  25
        The square of 6 is =  36
        The square of 7 is =  49
         */

        for(int i = 0; i <= 7; i++){
            System.out.println("The square of " + i + " is = " + i * i);
        }


        System.out.println("\n--------------Task5--------------\n");
        /*
        Write a program that finds sum of the numbers starting
        from 1 to 10
        Calculation => 1+2+3+4+5+6+7+8+9+10
        NOTE: Use loop!!!
        Expected Output:
        55
         */

        int sum = 0;
        for(int i = 1; i <= 10; i++){
            sum += i;
        }
        System.out.println(sum);


        System.out.println("\n--------------Task6--------------\n");
        /*
        Write a program that asks user to enter a positive number
        And find the factorial of the number
        0! = 1
        1! = 1
        2! = 1*2 = 2
        3! = 1*2*3 = 6

        Test Data 1:
        5
        Expected Output 1:
        120
        NOTE:
        5! = 1*2*3*4*5 = 120

        Test Data 2:
        7
        Expected Output 2:
        5040
        NOTE:
        5! = 1*2*3*4*5*6*7 = 5040
         */

        int num = ScannerHelper.getNumber();
        int fact = 1;
        for(int i = 1; i <= num; i++){
            fact = fact * i;
        }
        System.out.println(fact);


        System.out.println("\n--------------Task7--------------\n");
        /*
        Write a program that asks user to enter their first and last
        name
        And count how many vowel letters they have in their first and
        last name
        Vowel letters = a, e, i, o, u
        Example program1:
        Program: Please enter your full name
        User : Chris Thompson
        Program: There are 3 vowel letters in this full name
        Example program2:
        Program: Please enter your full name
        User : Alexander George
        Program: There are 7 vowel letters in this full name
         */

        String fullName = ScannerHelper.getFullName().toLowerCase();
        int count = 0;
        for(int i = 0; i < fullName.length(); i++){
            if(fullName.charAt(i) == 'a' || fullName.charAt(i) == 'e' || fullName.charAt(i) == 'i' || fullName.charAt(i) == 'o' || fullName.charAt(i) == 'u') count++;
        }
        System.out.println("There are " + count + " vowel letters in this full name");

        System.out.println("\n--------------Task8--------------\n");
        /*
        Write a program that asks user to enter a name
        If name starts with j or J, then finish the program
        But, if the name does not start with j or J, then keep asking
        until user gives a name that starts with j or J.
        Example program1:
        Program: Please enter a name
        User : Jessie
        Program: End of the program
        Example program2:
        Program: Please enter a name
        User : Alexander
        Program: Please enter a name
        User : Chris
        Program: Please enter a name
        User : Jordan
        Program: End of the program
         */

        while(true){
            String str = ScannerHelper.getFirstName().toUpperCase();
            if(str.startsWith("J")){
                System.out.println("End of program");
                break;
            }
        }

    }
}
