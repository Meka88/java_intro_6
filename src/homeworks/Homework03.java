package homeworks;

import java.util.Scanner;

public class Homework03 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("\n----------------Task1----------------\n");

        System.out.println("Please enter 2 numbers: ");
        int num1 = input.nextInt();
        int num2 =input.nextInt();

        System.out.println("The difference between numbers is = " + Math.abs((num1 - num2)));


        System.out.println("\n----------------Task2----------------\n");

        System.out.println("Please enter 5 numbers: ");
        int number1 = input.nextInt();
        int number2 = input.nextInt();
        int number3 = input.nextInt();
        int number4 = input.nextInt();
        int number5 = input.nextInt();

        int max1 = Math.max(number1, number2);
        int max2 = Math.max(number3, number4);
        int max3 = Math.max(max1, max2);

        int min1 = Math.min(number1, number2);
        int min2 = Math.min(number3, number4);
        int min3 = Math.min(min1, min2);

        System.out.println("Max value = " + Math.max(max3, number5));
        System.out.println("Min value = " + Math.min(min3, number5));


        System.out.println("\n----------------Task3----------------\n");

        double random1 = (Math.random() * 51) + 50;
        double random2 = (Math.random() * 51) + 50;
        double random3 = (Math.random() * 51) + 50;

        System.out.println("Number 1 = " + (int) random1);
        System.out.println("Number 2 = " + (int) random2);
        System.out.println("Number 3 = " + (int) random3);
        System.out.println("The sum of numbers is = " + (int) (random1 + random2 + random3));


        System.out.println("\n----------------Task4----------------\n");

        double alexHas = 125, mikeHas = 220;

        System.out.println("Alex's money: $" + (alexHas - 25.5));
        System.out.println("Mike's money: " + (mikeHas + 25.5));


        System.out.println("\n----------------Task5----------------\n");

        double needToSave = 390;
        double savePerDay = 15.60;

        System.out.println("David has to save " + (int) (needToSave / savePerDay) + " days.");


        System.out.println("\n----------------Task6----------------\n");

        String s1 = "5", s2 = "10";
        int b1 = Integer.parseInt(s1);
        int b2 = Integer.parseInt(s2);

        System.out.println("Sum of " + b1 + " and " + b2 + " is = " + (b1 + b2));
        System.out.println("Product of " + b1 + " and " + b2 + " is = " + (b1 * b2));
        System.out.println("Division of " + b1 + " and " + b2 + " is = " + (b1 / b2));
        System.out.println("Subtraction of " + b1 + " and " + b2 + " is = " + (b1 - b2));
        System.out.println("Remainder of " + b1 + " and " + b2 + " is = " + (b1 % b2));


        System.out.println("\n----------------Task7----------------\n");

        String n1 = "200", n2 = "-50";
        int d1 = Integer.parseInt(n1);
        int d2 = Integer.parseInt(n2);

        System.out.println("The greatest value is = " + Math.max(d1, d2));
        System.out.println("The smallest value is = " + Math.min(d1, d2));
        System.out.println("The average is = " + ((d1 + d2) / 2));
        System.out.println("The absolute difference is = " + Math.abs(d1 - d2));


        System.out.println("\n----------------Task8----------------\n");

        double quarter = 0.75, dime = 0.10, nickel = 0.10, penny = 0.01;
        double oneDaySave = quarter + dime + nickel + penny;

        System.out.println((int) (24 / oneDaySave) + " days");
        System.out.println((int) (168 / oneDaySave) + " days");
        System.out.println("$" + (5 * 30) * oneDaySave);


        System.out.println("\n----------------Task9----------------\n");

        double jessieNeeds = 1250, jessieSaves = 62.5;

        System.out.println((int)(jessieNeeds / jessieSaves));


        System.out.println("\n----------------Task10----------------\n");

        double newCar = 14265, option1 = 475.50, option2 = 951;

        System.out.println("Option 1 will take " + (int)(newCar / option1) + " months");
        System.out.println("Option 2 will take " + (int)(newCar / option2) + " months");


        System.out.println("\n----------------Task11----------------\n");

        System.out.println("Please enter two numbers:");
        int userNumber1 = input.nextInt();
        int userNumber2 = input.nextInt();

        System.out.println((double)userNumber1 / (double) userNumber2);


        System.out.println("\n----------------Task12----------------\n");

        int randomNum1 = (int) (Math.random() * 101);
        int randomNum2 = (int) (Math.random() * 101);
        int randomNum3 = (int) (Math.random() * 101);

        if(randomNum1 <= 25 && randomNum2 <= 25 && randomNum3 <= 25) {
            System.out.println("False");
        } else {
            System.out.println("True");
        }


        System.out.println("\n----------------Task13----------------\n");

        System.out.println("Please enter a number from 1 to 7, both included:");

        int dayNum = input.nextInt();

        switch (dayNum) {
            case 1:
                System.out.println("MONDAY");
                break;
            case 2:
                System.out.println("TUESDAY");
                break;
            case 3:
                System.out.println("WEDNESDAY");
                break;
            case 4:
                System.out.println("THURSDAY");
                break;
            case 5:
                System.out.println("FRIDAY");
                break;
            case 6:
                System.out.println("SATURDAY");
                break;
            case 7:
                System.out.println("SUNDAY");
                break;
            default:
                System.out.println("Invalid number");
        }


        System.out.println("\n----------------Task14----------------\n");

        System.out.println("Tell me your exam results?");
        int result1 = input.nextInt();
        int result2 = input.nextInt();
        int result3 = input.nextInt();
        int average = (result1 + result2 + result3) / 3;

        if(average >= 70) {
            System.out.println("YOU PASSED!");
        } else {
            System.out.println("YOU FAILED!");
        }


        System.out.println("\n----------------Task15----------------\n");

        System.out.println("Please enter 3 numbers:");
        int userNum1 = input.nextInt();
        int userNum2 = input.nextInt();
        int userNum3 = input.nextInt();

        if(userNum1 == userNum2 && userNum2 == userNum3) {
            System.out.println("TRIPLE MATCH");
        } else if (userNum1 == userNum2 || userNum2 == userNum3 || userNum1 == userNum3) {
            System.out.println("DOUBLE MATCH");
        } else {
            System.out.println("NO MATCH");
        }

    }
}
