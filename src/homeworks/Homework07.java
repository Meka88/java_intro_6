package homeworks;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Homework07 {
    public static <Char> void main(String[] args) {
        System.out.println("\n--------------Task1--------------\n");
        /*
        Requirement:
        -Create an ArrayList and store below numbers
        10, 23, 67, 23, 78
        THEN:
        -Print element at index of 3
        -Print element at index of 0
        -Print element at index of 2
        -Print the entire list
        Expected Result:
        23
        10
        67
        [10, 23, 67, 23, 78]
         */

        ArrayList<Integer> numbers = new ArrayList<>(Arrays.asList(10, 23, 67, 23, 78));
        System.out.println(numbers.get(3));
        System.out.println(numbers.get(0));
        System.out.println(numbers.get(2));
        System.out.println(numbers);


        System.out.println("\n--------------Task2--------------\n");
        /*
        Requirement:
        -Create an ArrayList and store below colors
        Blue, Brown, Red, White, Black, Purple
        THEN:
        -Print element at index of 1
        -Print element at index of 3
        -Print element at index of 5
        -Print the entire list
        Expected Result:
        Brown
        White
        Purple
        [Blue, Brown, Red, White, Black, Purple]
         */

        ArrayList<String> colors = new ArrayList<>(Arrays.asList("Blue", "Brown", "Red", "White", "Black", "Purple"));
        System.out.println(colors.get(1));
        System.out.println(colors.get(3));
        System.out.println(colors.get(5));
        System.out.println(colors);


        System.out.println("\n--------------Task3--------------\n");
        /*
        Requirement:
        -Create an ArrayList and store below numbers
        23, -34, -56, 0, 89, 100
        THEN:
        -Print the entire list
        -Print the entire list sorted in ascending order
        Expected Result:
        [23, -34, -56, 0, 89, 100]
        [-56, -34, 0, 23, 89, 100]
         */

        ArrayList<Integer> numbers1 = new ArrayList<>(Arrays.asList(23, -34, -56, 0, 89, 100));
        System.out.println(numbers1);
        Collections.sort(numbers1);
        System.out.println(numbers1);

        System.out.println("\n--------------Task4--------------\n");
        /*
        Requirement:
        -Create an ArrayList and store below cities
        Istanbul, Berlin, Madrid, Paris
        THEN:
        -Print the entire list
        -Print the entire list sorted lexicographically
        Expected Result:
        [Istanbul, Berlin, Madrid, Paris]
        [Berlin, Istanbul, Madrid, Paris]
         */

        ArrayList<String> cities = new ArrayList<>(Arrays.asList("Istanbul", "Berlin", "Madrid", "Paris"));
        System.out.println(cities);
        Collections.sort(cities);
        System.out.println(cities);

        System.out.println("\n--------------Task5--------------\n");
        /*
        Requirement:
        -Create an ArrayList and store Marvel characters below
        Spider-Man, Iron Man, Black Panter, Deadpool, Captain America
        THEN:
        -Print the entire list
        -Then, check if it contains Wolwerine
        if it contains Wolwerine, then print true
        if it does not contain Wolwerine, print false
        Expected Result:
        [Spider-Man, Iron Man, Black Panter, Deadpool, Captain America]
        false
         */

        ArrayList<String> heroes = new ArrayList<>(Arrays.asList("Spider Man", "Iron Man", "Black Panter", "Deadpool", "Captain America"));
        System.out.println(heroes);
        boolean isContains = false;
        for (String hero : heroes) {
            if(hero.contains("Wolwerine")) isContains = true;

        }
        System.out.println(isContains);


        System.out.println("\n--------------Task6--------------\n");
        /*
        Requirement:
        -Create an ArrayList and store Avengers characters
        below
        Hulk, Black Widow, Captain America, Iron Man
        THEN:
        -Print the entire list sorted lexicographically
        -Then, check if it contains Hulk and Iron Man
        if it contains both, then print true
        if it does not contain both, print false
        Expected Result:
        [Black Widow, Captain America, Hulk, Iron Man]
        true
         */

        ArrayList<String> avengers = new ArrayList<>(Arrays.asList("Hulk", "Black Widow", "Captain America", "Iron Man"));
        Collections.sort(avengers);
        System.out.println(avengers);
        boolean haveBoth = false;
        for (int i = 0; i < avengers.size(); i++) {
            if(avengers.contains("Hulk") && avengers.contains("Iron Man")) haveBoth = true;
        }
        System.out.println(haveBoth);

        System.out.println("\n--------------Task7--------------\n");
        /*
        Requirement:
        -Create an ArrayList and store characters below
        A, x, $, %, 9, *, +, F, G
        THEN:
        -Print the entire list
        -Print each element
        Expected Result:
        [A, x, $, %, 9, *, +, F, G]
        A
        x
        $
        %
        9
        *
        +
        F
        G
         */

        ArrayList<Character> characters = new ArrayList<>(Arrays.asList('A', 'x', '$', '%', '9', '*', '+', 'F', 'G'));
        System.out.println(characters);
        for (Character character : characters) {
            System.out.println(character);
        }

        System.out.println("\n--------------Task8--------------\n");
        /*
        Requirement:
        -Create an ArrayList and store below objects
        Desk, Laptop, Mouse, Monitor, Mouse-Pad, Adapter
        THEN:
        -Print the entire list
        -Print the entire list sorted lexicographically
        -Count objects that starts with M or m
        -Count objects that does not have A or a or E or e
        Expected Result:
        [Desk, Laptop, Mouse, Monitor, Mouse-Pad, Adapter]
        [Adapter, Desk, Laptop, Monitor, Mouse, Mouse-Pad]
        3
        1
         */

        ArrayList<String> list = new ArrayList<>(Arrays.asList("Desk", "Laptop", "Mouse", "Monitor", "Mouse-Pad", "Adapter"));
        System.out.println(list);
        Collections.sort(list);
        System.out.println(list);
        int countM = 0;
        int countAandE = 0;
        for (String s : list) {
            if(s.toLowerCase().startsWith("m")) countM++;
            if(s.toLowerCase().startsWith("a") || s.toLowerCase().startsWith("e")) countAandE++;
        }
        System.out.println(countM);
        System.out.println(countAandE);


        System.out.println("\n--------------Task9--------------\n");
        /*
        Requirement:
        -Create an ArrayList and store below kitchen objects
        Plate, spoon, fork, Knife, cup, Mixer
        THEN:
        -Print the entire list
        -Print how many elements starts with uppercase
        -Print how many elements starts with lowercase
        -Print how many elements has P or p
        -Print how many elements starts or ends with P or p
        Expected Result:
        [Plate, spoon, fork, Knife, cup, Mixer]
        Elements starts with uppercase = 3
        Elements starts with lowercase = 3
        Elements having P or p= 3
        Elements starting or ending with P or p = 2
         */

        ArrayList<String> kitchenList = new ArrayList<>(Arrays.asList("Plate", "spoon", "fork", "Knife", "cup", "Mixer"));
        System.out.println(kitchenList);
        int countUpper = 0, countLower = 0, countP = 0, startAndEndP = 0;

        for (String s : kitchenList) {
            if(Character.isUpperCase(s.charAt(0))) countUpper++;
            else countLower++;
            if(s.toLowerCase().startsWith("p") || s.toLowerCase().endsWith("p")){
                countP++;
                startAndEndP++;
            } else if(s.toLowerCase().contains("p")) countP++;
        }
        System.out.println("Elements starts with uppercase = " + countUpper);
        System.out.println("Elements starts with lowercase = " + countLower);
        System.out.println("Elements having P or p = " + countP);
        System.out.println("Elements starting or ending with P or p = " + startAndEndP);


        System.out.println("\n--------------Task10--------------\n");
        /*
        Requirement:
        -Create an ArrayList and store below numbers
        3, 5, 7, 10, 0, 20, 17, 10, 23, 56, 78
        THEN:
        -Print the entire list
        -Print how many element can be divided by 10
        -Print how many even numbers are greater than 15
        -Print how many odd numbers are less than 20
        -Print how many elements are less than 15 or greater
        than 50
        Expected Result:
        [3, 5, 7, 10, 0, 20, 17, 10, 23, 56, 78]
        Elements that can be divided by 10 = 4
        Elements that are even and greater than 15 = 3
        Elements that are odd and less than 20 = 4
        Elements that are less than 15 or greater than 50 = 8
         */

        ArrayList<Integer> numbers2 = new ArrayList<>(Arrays.asList(3, 5, 7, 10, 0, 20, 17, 10, 23, 56, 78));
        System.out.println(numbers2);
        int countBy10 = 0, countEven = 0, countOdd = 0, count = 0;

        for (Integer integer : numbers2) {
            if(integer % 10 == 0) countBy10++;
            if(integer % 2 == 0 && integer > 15) countEven++;
            if(integer % 2 != 0 && integer < 20) countOdd++;
            if(integer < 15 || integer > 50) count++;
        }
        System.out.println("Elements that can be divided by 10 = " + countBy10);
        System.out.println("Elements that are even and greater than 15 = " + countEven);
        System.out.println("Elements that are odd and less than 20 = " + countOdd);
        System.out.println("Elements that are less than 15 or greater than 50 = " + count);
    }
}
