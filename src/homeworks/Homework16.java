package homeworks;

import java.util.HashMap;
import java.util.Map;

public class Homework16 {
    public static void main(String[] args) {
        System.out.println("\n--------------Task1--------------\n");


        System.out.println(parseData("{104}LA{101}Paris{102}Berlin{103}Chicago{100}London"));

        System.out.println("\n--------------Task2--------------\n");

//        Map<String, Double> prices = new HashMap<>();
//        prices.put("Apple", 2.00);
//        prices.put("Orange", 3.29);
//        prices.put("Mango", 4.99);
//        prices.put("Pineapple", 5.25);
//
//        Map<String, Integer> items = new HashMap<>();
//        items.put("Apple", 3);
//        items.put("Mango", 1);

        //System.out.println(calculateTotalPrice1(items, prices)); // Output: 10.99

//        items.clear();
//        items.put("Apple", 2);
//        items.put("Pineapple", 1);
//        items.put("Orange", 3);

        //System.out.println(calculateTotalPrice1(items, prices));

        // Bilals solution
        HashMap<String, Integer> map = new HashMap<>();
        map.put("Apple", 3);
        map.put("Mango", 1);

        System.out.println(calculateTotalPrice1(map));

        System.out.println("\n--------------Task3--------------\n");

//        Map<String, Integer> items1 = new HashMap<>();
//        items1.put("Apple", 3);
//        items1.put("Mango", 5);
//
//        System.out.println(calculateTotalPrice2(items1)); // Output: 24.96
//
//        Map<String, Integer> items2 = new HashMap<>();
//        items2.put("Apple", 4);
//        items2.put("Mango", 8);
//        items2.put("Orange", 3);

        //System.out.println(calculateTotalPrice2(items2)); // Output: 45.81

        HashMap<String, Integer> map1 = new HashMap<>();
        map1.put("Apple", 3);
        map1.put("Mango", 5);

        System.out.println(calculateTotalPrice2(map1));
    }
    /** Task 1
     * Requirement:
     * Write a method called as parseData() which takes a
     * String has some keys in {} and values after between }{
     * and returns a collection that has all the keys and values
     * as entries.
     * NOTE: The keys should be sorted!
     * Test Data:
     * {104}LA{101}Paris{102}Berlin{103}Chicago{100}London
     * Expected Output:
     * {100=London, 101=Paris, 102=Berlin, 103=Chicago, 104=LA}
     */
    public static Map<Integer, String> parseData(String str){
        HashMap<Integer, String> result = new HashMap<>();
        String[] arr = str.split("\\{");
        for (int i = 1; i < arr.length; i++) {
            result.put(Integer.parseInt(arr[i].replaceAll("[^0-9]", "")), arr[i].replaceAll("[^A-Za-z]", ""));
        }
        return result;
    }


    /** Task 2
     * Requirement:
     * Write a method called as calculateTotalPrice1() which takes
     * a Map of some shopping items with their amounts and
     * calculates the total prices as double. Item prices are given
     * below
     * Apple = $2.00
     * Orange = $3.29
     * Mango = $4.99
     * Pineapple = $5.25
     * Test Data 1:
     * {Apple=3, Mango = 1}
     * Expected Output 1:
     * 10.99
     * Test Data 2:
     * {Apple=2, Pineapple = 1, Orange=3}
     * Expected Output 2:
     * 19.12
     */

   /* public static double calculateTotalPrice1(Map<String, Integer> items, Map<String, Double> prices){
        double finalPrice = 0.0;
        for (Map.Entry<String, Integer> entry : items.entrySet()) {
            String item = entry.getKey();
            int amount = entry.getValue();

            if (prices.containsKey(item)) {
                double itemPrice = prices.get(item);
                finalPrice += itemPrice * amount;
            }
        }

        return finalPrice;
    }*/
// Bilal's solution, better and easier
    public static double calculateTotalPrice1(HashMap<String, Integer> map){
        double total = 0.0;
        for(String s : map.keySet()){
            switch (s){
                case "Apple":{
                    total += map.get(s) * 2.00;
                    break;
                }
                case "Mango":{
                    total += map.get(s) * 4.99;
                    break;
                }
                case "Orange":{
                    total += map.get(s) * 3.29;
                    break;
                }
                case "Pineapple":{
                    total += map.get(s) * 5.25;
                    break;
                }
            }
        }
        return total;
    }

    /** Task 3
     * Requirement:
     * Write a method calculateTotalPrice2() which takes a Map of some
     * shopping items with their amounts and calculates the total prices as
     * double. Item prices are given below
     * Apple = $2.00
     * Orange = $3.29
     * Mango = $4.99
     * BUT there will be some discounts as below
     * There will be %50 discount for every second Apple
     * There will be 1 free Mango if customer gets 3. So, fourth one is free.
     * Test Data 1:
     * {Apple=3, Mango = 5}
     * Expected Output 1:
     * 24.96
     * Test Data 2:
     * {Apple=4, Mango = 8, Orange=3}
     * Expected Output 2:
     * 45.81
     */

    public static double calculateTotalPrice2(HashMap<String, Integer> map){
        double total = 0.0;

        for (String s : map.keySet()) {
            switch (s){
                case "Apple":{
                    int disApple = map.get(s) / 2;
                    total += (map.get(s) * 2.00 - disApple);
                    break;
                }
                case "Mango":{
                    int freeMangos = map.get(s) / 4;
                    total += map.get(s) * 4.99 - (freeMangos * 4.99);
                    break;
                }
                case "Orange":{
                    total += map.get(s) * 3.29;
                    break;
                }
            }


        }
        return total;
    }

    /*public static double calculateTotalPrice2(Map<String, Integer> items) {
        double totalPrice = 0.0;

        Map<String, Double> itemPrices = new HashMap<>();
        itemPrices.put("Apple", 2.00);
        itemPrices.put("Orange", 3.29);
        itemPrices.put("Mango", 4.99);

        for (Map.Entry<String, Integer> entry : items.entrySet()) {
            String item = entry.getKey();
            int amount = entry.getValue();

            if (itemPrices.containsKey(item)) {
                double itemPrice = itemPrices.get(item);

                if (item.equals("Apple")) {
                    int discountQuantity = amount / 2;
                    int remainingQuantity = amount % 2;
                    totalPrice += (itemPrice * (amount - discountQuantity - remainingQuantity * 0.5));
                } else if (item.equals("Mango")) {
                    int freeQuantity = amount / 3;
                    int remainingQuantity = amount % 3;
                    totalPrice += (itemPrice * (amount - freeQuantity - remainingQuantity));
                } else {
                    totalPrice += itemPrice * amount;
                }
            }
        }
        return totalPrice;
    }*/
}
