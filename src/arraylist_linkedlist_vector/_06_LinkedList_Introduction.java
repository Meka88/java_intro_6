package arraylist_linkedlist_vector;

import java.util.Arrays;
import java.util.LinkedList;

public class _06_LinkedList_Introduction {
    public static void main(String[] args) {
        LinkedList<String> cities = new LinkedList<>(Arrays.asList("Berlin", "Rome", "Kyev", "Ankara", "Madrid", "Chicago"));
        System.out.println(cities.size());
        System.out.println(cities.contains("Miami")); // false

        System.out.println(cities.getFirst()); // Berlin
        System.out.println(cities.getLast()); // Chicago

        System.out.println(cities.removeFirst());
        System.out.println(cities.removeLast());

        System.out.println(cities);
    }
}
