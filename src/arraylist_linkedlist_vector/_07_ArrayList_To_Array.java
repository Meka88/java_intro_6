package arraylist_linkedlist_vector;

import java.util.ArrayList;
import java.util.Arrays;

public class _07_ArrayList_To_Array {
    public static void main(String[] args) {
        int[] arr = {3, 5, 7, 3, 5};
        System.out.println(uniques(arr));
        System.out.println(uniques(new int[]{10, 10, 10, 10}));
        System.out.println(uniques(new int[]{13, 20, 20, 13}));
        System.out.println(uniques(new int[]{}));
    }
    /*
    Write a method (uniques) that takes some numbers in an int array and returns the unique numbers back
    Return type must be an array
    [3, 5, 7, 3, 5]         -> [3, 5, 7]
    [10, 10, 10, 10]        -> [10]
    [13, 20, 20, 13]        -> [13, 20]
    []                      -> []
     */

    public static Object[] uniques(int[] arr){
        ArrayList<Integer> arrList = new ArrayList<>();
        for (int i : arr) {
            if(!arrList.contains(i)) arrList.add(i);
        }
        return arrList.toArray();
    }
}
