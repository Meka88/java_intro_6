package arraylist_linkedlist_vector;

import java.util.ArrayList;
import java.util.Arrays;

public class Exercise02_CountNumbers {
    public static void main(String[] args) {
        System.out.println("\n-------------countEven------------\n");
        ArrayList<Integer> numbers1 = new ArrayList<>(Arrays.asList(2, 3, 5));
        ArrayList<Integer> numbers2 = new ArrayList<>(Arrays.asList(10, 20, 30));
        ArrayList<Integer> numbers3 = new ArrayList<>();
        ArrayList<Integer> numbers4 = new ArrayList<>(Arrays.asList(-1, 3, 17, 25));
        System.out.println(countEven(numbers1));
        System.out.println(countEven(numbers2));
        System.out.println(countEven(numbers3));
        System.out.println(countEven(numbers4));

        System.out.println("\n-------------more15------------\n");
        more15(numbers1);
        more15(numbers2);
        more15(numbers3);
        more15(numbers4);
    }

    /*
    public static int countEven(ArrayList<Integer> numbers){
        int countEven = 0;
        for (Integer number : numbers) {
            if(number % 2 == 0) countEven++;
        }
        return countEven;
    }

     */
    public static int countEven(ArrayList<Integer> numbers){
        return (int) numbers.stream().filter(element -> element % 2 == 0).count();
    }

    public static void more15(ArrayList<Integer> list){
        System.out.println(list.stream().filter(x -> x > 15).count());

        int count = 0;
        for (Integer integer : list) {
            if(integer > 15) count++;
        }
        System.out.println(count);
    }

    public static int no3(ArrayList<Integer> list){
        //return (int) list.stream().filter(e -> e.toString().contains("3")).count();

        int count = 0;
        for (Integer integer : list) {
            if(String.valueOf(integer).contains("3")) count++;
            //if(integer.toString().contains("3")) count++;
        }
        return count;
    }
}
