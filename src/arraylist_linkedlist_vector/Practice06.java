package arraylist_linkedlist_vector;



import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;


public class Practice06 {
    public static void main(String[] args) {
        System.out.println("\n------------------------Task-1------------------------\n");

        System.out.println(Arrays.toString(double1(new int[]{3, 2, 5, 7, 0})));
        System.out.println(Arrays.toString(double1(new int[]{-2, 0, 3, 10, 100})));

        System.out.println("\n------------------------Task-2------------------------\n");

        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(2, 3, 7, 1, 1, 7, 1));
        ArrayList<Integer> list1 = new ArrayList<>(Arrays.asList(5, 7, 2, 2, 10, 10));

        System.out.println(secondMax(list));
        System.out.println(secondMax(list1));

        System.out.println("\n------------------------Task-3------------------------\n");

        ArrayList<Integer> list2 = new ArrayList<>(Arrays.asList(2, 3, 7, 1, 1, 7, 1));
        ArrayList<Integer> list3 = new ArrayList<>(Arrays.asList(5, 7, 2, 2, 10, 10));
        System.out.println(secondMin(list2));
        System.out.println(secondMin(list3));

        System.out.println("\n------------------------Task-4------------------------\n");

        ArrayList<String> list4 = new ArrayList<>(Arrays.asList("Tech", "Global", null, "", "", "School"));
        ArrayList<String> list5 = new ArrayList<>(Arrays.asList("", "", ""));
        System.out.println(removeEmpty(list4));
        System.out.println(removeEmpty(list5));

        System.out.println("\n------------------------Task-5------------------------\n");

        ArrayList<Integer> list6 = new ArrayList<>(Arrays.asList(200, 5, 100, 99, 101, 75));
        ArrayList<Integer> list7 = new ArrayList<>(Arrays.asList(-12, -123, -5, 1000, 500, 0));
        System.out.println(remove3orMore(list6));
        System.out.println(remove3orMore(list7));

        System.out.println("\n------------------------Task-6------------------------\n");

        String str = "TechGlobal School";
        String str1 = "Star Light Star Bright";
        System.out.println(uniqueWords(str));
        System.out.println(uniqueWords(str1));
    }

    /*
        Write a method called as
        double
         to double each element
        in an int array and return it back.
        NOTE: The return type is an array.
        Test data 1:
        {3, 2, 5, 7, 0}
        Expected output 1:
        [6, 4, 10, 14, 0]
        Test data 2:
        [-2, 0, 3, 10, 100]
        Expected output 2:
        [-4, 0, 6, 20, 200]
      */
    public static int[] double1(int[] arr){
        for (int i = 0; i < arr.length; i++) {
            arr[i] *= 2;
        }
        return arr;
    }

    /*
    Write a method called as
    secondMax
     to find and return
    the second max number in an ArrayList
    Test data 1:
    {2, 3, 7, 1, 1, 7, 1}
    Expected output 1:
    3
    Test data 2:
    [5, 7, 2, 2, 10, 10]
    Expected output 2:
    7
     */

    public static int secondMax(ArrayList<Integer> list){
        /* 1 way
        Collections.sort(list);//1, 1, 1, 2, 3, 7, 7

        for (int i = list.size()-2; i >= 0 ; i--) {
            if(list.get(i) < list.get(list.size() - 1)) return list.get(i);
        }
            return -1;
         */

        /*
        //WAY2
        for (Integer n : list) {
            if(n > max) {
                secondMax = max;
                max = n;
            }
        }
         */

        int max = Integer.MIN_VALUE; // assign to min value
        int secondMax = Integer.MIN_VALUE;

        for (Integer n : list) {
            if(n > max){
                secondMax = max;
                max = n;
            }
        }
        return secondMax;
    }

    /*
    Write a method called as
    secondMin
     to find and return
    the second min number in an ArrayList
    Test data 1:
    {2, 3, 7, 1, 1, 7, 1}
    Expected output 1:
    2
    Test data 2:
    [5, 7, 2, 2, 10, 10]
    Expected output 2:
    5
     */

    public static int secondMin(ArrayList<Integer> list) {
//        int min = Integer.MAX_VALUE;
//        int secondMin = Integer.MAX_VALUE;
//
//        for (Integer x : list) {
//            if(x < min){
//                secondMin = min;
//                min = x;
//            }
//        }
//        return secondMin;

        // using sorting method
        Collections.sort(list); // sort the list
        for (int i = 1; i < list.size(); i++) { // we start looping from second element in a list
            if (list.get(i) > list.get(0)) return list.get(i); // we compare that to the fist element
        }
        return -1;
    }
    /*
    Write a method called as
    removeEmpty
     to find and
    remove all the elements in an ArrayList that are empty or
    null.
    Then, return the modified ArrayList back.
    Test data 1:
    ["Tech", "Global", "", null, "", "School"]
    Expected output 1:
    ["Tech", "Global", "School"]
    Test data 2:
    ["", "", ""]
    Expected output 2:
    []
     */

    public static ArrayList<String> removeEmpty(ArrayList<String> list){
       /* ArrayList<String> result = new ArrayList<>();
        for (String s : list) {
            if(s != null){
                if(!s.isEmpty()) result.add(s);
            }
        }
        return result;
        */

        list.removeIf(e -> e == null || e.isEmpty());
        return list;
    }

    /*
        Write a method called as remove3orMore
        to find and remove all
        the elements in an ArrayList that are more than 2 digits.
        Then, return the modified ArrayList back.
        NOTE: - sign should not count as a digit when it is negative
        number.
        Test data 1:
        [200, 5, 100, 99, 101, 75]
        Expected output 1:
        [5, 99, 75]
        Test data 2:
        [-12, -123, -5, 1000, 500, 0]
        Expected output 2:
        [-12, -5, 0]
     */

    public static ArrayList<Integer> remove3orMore(ArrayList<Integer> list){
        // with loop
//        ArrayList<Integer> newList = new ArrayList<>();
//        for (Integer x : list) {
//            if(Math.abs(x) < 100) newList.add(x);
//
//        }
//        return newList;

        list.removeIf(e -> Math.abs(e) >= 100);
        return list;
    }

    /*
    Write a method called as
    uniquesWords
     to find and return all the
    unique words in a String.
    NOTE: The return type is an ArrayList.
    NOTE: Assume that you will not be given extra spaces.
    Test data 1:
    "TechGlobal School”
    Expected output 1:
    ["TechGlobal", "School"]
    Test data 2:
    "Star Light Star Bright"
    Expected output 2:
    ["Star", "Light", "Bright"]
     */

    public static ArrayList<String> uniqueWords(String str){
        ArrayList<String> myList = new ArrayList<>();
        String [] arr = str.split(" ");
        for (String s : arr) {
            if(!myList.contains(s)) myList.add(s);
        }
        return myList;
    }

}
