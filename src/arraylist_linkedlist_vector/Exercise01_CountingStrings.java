package arraylist_linkedlist_vector;

import java.util.ArrayList;
import java.util.Arrays;

public class Exercise01_CountingStrings {
    public static void main(String[] args) {
        ArrayList<String> list1 = new ArrayList<>(Arrays.asList("Hello", "Hi", "School", "Computer"));
        ArrayList<String> list2 = new ArrayList<>(Arrays.asList());
        ArrayList<String> list3 = new ArrayList<>(Arrays.asList("Object", "Laptop"));

        System.out.println(countO(list1));
        System.out.println(countO(new ArrayList<>(Arrays.asList("abc", "xyz"))));

        System.out.println(more3(list1));
        System.out.println(more3(new ArrayList<>(Arrays.asList("abc", "xyz"))));
        System.out.println(more3(list2));
        System.out.println(more3(list3));

    }
    public static int countO(ArrayList<String> list){
        int count0 = 0;
        for (String s : list) {
            if(s.toLowerCase().contains("o")) count0++;
        }
        return count0;
    }

    public static int more3(ArrayList<String> list){
        int count3 = 0;
        for (String s : list) {
            if(s.length() >= 3) count3++;
        }
        return count3;
    }
}
