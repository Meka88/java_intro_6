package recursion;

public class TestRecursion {
    public static void main(String[] args) {
        System.out.println(sum1ToNIterative(4));
        System.out.println(sum1ToNIterative(5));
        System.out.println(sum1ToNIterative(10));

        System.out.println(sum1To5Recursive(4));
        System.out.println(sum1To5Recursive(5));
        System.out.println(sum1To5Recursive(10));

        System.out.println(factorial(4));
        System.out.println(factorial(5));
        System.out.println(factorial(10));
    }

    public static int sum1ToNIterative(int n){
        int sum = 0;
        for (int i = 1; i < n; i++) {
            sum += i;
        }
        return sum;
    }

    public static int sum1To5Recursive(int n){
        if(n != 1) return n + sum1To5Recursive(n-1);
        else return 1;
    }

    public static int factorial(int num){
        if(num != 1) return num * factorial(num - 1);
        return 1;
    }

    public static int fibonacci(int num){
        if(num != 2) return fibonacci(num - 1) + fibonacci(num - 2);
        return 1;
    }
}
