package recursion;

public class ReverseAString {
    public static void main(String[] args) {
        System.out.println(reverseStrRecursive("hello"));
        System.out.println(reverseStrRecursive("TechGlobal"));
        System.out.println(reverseStrRecursive("Java"));
    }

    public static String reverseStrRecursive(String str){
        if(str.length() <= 1) return str;
        else return reverseStrRecursive(str.substring(1)) + str.charAt(0);
    }
}
