package practices;

import utilities.ScannerHelper;

public class Exercise01_StringMethods {
    public static void main(String[] args) {

        System.out.println("\n----------------------Task1---------------------------\n");

        String str = ScannerHelper.getString();
        System.out.println("The string given is = " + str);

        System.out.println("\n----------------------Task2---------------------------\n");

        if(str.isEmpty()) System.out.println("The string given is empty");
        else System.out.println("The length is = " + str.length());

        System.out.println("\n----------------------Task3---------------------------\n");

        if(str.isEmpty()) System.out.println("There is no character in this String");
        else System.out.println("The first character = " + str.charAt(0));

        System.out.println("\n----------------------Task4---------------------------\n");

        if(str.isEmpty()) System.out.println("There is no character in this String");
        else System.out.println("The first character = " + str.charAt(str.length() - 1));


        System.out.println("\n----------------------Task5---------------------------\n");

        str = str.toLowerCase();
        if(str.contains("a") || str.contains("e") || str.contains("i") || str.contains("u") || str.contains("o")) System.out.println("The string has vowel");
        else System.out.println("This string does not have vowel");
    }
}
