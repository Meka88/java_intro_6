package variables;

public class CreatingMultipleVariables {
    public static void main(String[] args) {
        int age1;
        int age2;
        int age3;
        int age4, age5, age6;
        age4 = 5;
        age6 = 8; // only initialized variable will compile
    }
}
