package variables;

public class UnderstandingVariables {
    public static void main(String[] args) {
        String name = "Jane"; // declaring and initializing the variable

        int age; // declaring the variable without a value

        age = 50; // initializing the variable


        System.out.println(age);
    }
}
