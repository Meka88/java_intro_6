package variables;

public class VariableNamingConvention {
    public static void main(String[] args) {
        // unwritten rules
        boolean isMale = false; // camelCase, meaningful, don't start with Capital letters

        // written rules
        int n1 = 5; // don't start with  numbers

        double s2_$ = 45; // only two special characters are allowed anywhere
        // don't use reserved words like class, variables and public
    }
}
