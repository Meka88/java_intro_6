package casting;

public class Exercise02 {
    public static void main(String[] args) {
        /*
        Phone --> $899.99
        $50 per day

        How many days later you can buy this phone?
         */

        double price = 900;
        double dailySaveAmount = 50;

        System.out.println("You can buy the phone after " + (int)(price / dailySaveAmount) + " days.");
    }
}
