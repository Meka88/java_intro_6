package casting;

public class ExplicitCasting {
    public static void main(String[] args) {
        /*
        Explicit casting is storing bigger data types into smaller data types
        it doesn't happen automatically and programmer have to resolve the compiler issue
        also known as narrowing or dow-casting

        long -> byte
        int  -> short
        double -> float
        double -> int
         */

        long num1 = 273645237;
        byte num2 = (byte) num1;
    }
}
