package collections;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class _01_List {
    public static void main(String[] args) {
        /*
        list is an interface, and it has some class implementations:
        1. ArrayList
        2. LinkedList
        3. Vector

        Common features:
        - They keep insertion order
        - they allow duplicates
        - they allow null element
         */

        ArrayList<String> list = new ArrayList<>();
        list.add("Carmela");
        list.add("Zel");
        list.add("Okan");

        System.out.println(list);

        System.out.println("\n-----------ArrayList and LinkedList in the shape of List---------\n");
        List<Integer> numbers1 = new ArrayList<>();
        List<Integer> numbers2 = new LinkedList<>();
    }
}
