package first_package;

public class FirstProgram {
    public static void main(String[] args) {
        // our code will always be inside main method
        /* multiline comments */
        System.out.println("This is TechGlobal");
        System.out.println("I am learning Java");
        System.out.println("My name is Meerim");
        // more practice
        System.out.println("This class is fun");
        System.out.println("Today is gloomy weather");
        System.out.println("This winter is warm. ");
        System.out.println("My PC is slow. ");
        System.out.println("I don't know why. ");

    }
}
