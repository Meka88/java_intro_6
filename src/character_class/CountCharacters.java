package character_class;

import utilities.ScannerHelper;

import javax.swing.text.AttributeSet;

public class CountCharacters {
    public static void main(String[] args) {
        /*
        Write a method that asks user to enter a String
        Count letters
        Count digits
        Examples
        "123 Street Chicago"    -> This string has 3 digits and 13 letters
        "ABC"                   -> This string has 0 digits and 3 letters
        "12345"                 -> This string has 5 digits and 0 letters
        "     "                 -> This string has 0 digits and 0 letters
        PSEUDO
        Get characters of the String one by one
        Check if the character is letter -> if so increase the count of letters by one
        Check if the character is digit  -> if so increase the count of digits by one
         */
        String str = ScannerHelper.getString();
        int letters = 0;
        int digits = 0;
        for(int i = 0; i < str.length(); i++){
            if(Character.isLetter(str.charAt(i)))letters++;
            else if(Character.isDigit(str.charAt(i)))digits++;
        }
        System.out.println("This string has " + digits + " digits and " + letters + " letters");


        System.out.println("\n---------------------------Task 2---------------------------\n");
        /*
        Write a method that asks user to enter a String
        Uppercase letters
        Lowercase letters
        Examples
        "123 Street Chicago"    -> This string has 2 uppercase letters and 11 lowercase letters
        "ABC"                   -> This string has 3 uppercase letters and 0 lowercase letters
        "12345"                 -> This string has 0 uppercase letters and 0 lowercase letters
        "     "                 -> This string has 0 uppercase letters and 0 lowercase letters
         */

        String str1 = ScannerHelper.getString();
        int upperCount = 0, lowerCount = 0;

        for(int i = 0; i < str1.length(); i++){
            char ch = str1.charAt(i);
            if(Character.isUpperCase(ch)) upperCount++;
            else if(Character.isLowerCase(ch)) lowerCount++;
        }
        System.out.println("This string has " + upperCount + " upperscase letters and " + lowerCount + " lowercase letters");

        System.out.println("\n---------------------------Task 3---------------------------\n");
        /*
         Write a program that asks user to enter a String
         Count how many special characters you have in the String
         "Hello World"      -> 0
         "Hello!"           -> 1
         "abc@gmail.com"    -> 2
         "!@#$%&"           -> 6
          */

        String str2 = ScannerHelper.getString();
        int count = 0;
        for(int i = 0; i < str2.length(); i++){
            char x = str2.charAt(i);
            if(!Character.isLetterOrDigit(x) && !Character.isWhitespace(x)) count++;
        }
        System.out.println(count);

        System.out.println("\n---------------------------Task 4---------------------------\n");
        /*
        Putting all together
        Write a program that asks user to enter a String
        Count letters
        Count uppercase letters
        Count lowercase letters
        Count digits
        Count spaces
        Count specials
        Example:
        The price of the iPhone is $1,000.
        EXPECTED OUTPUT:
        Letters = 21
        Uppercase letters = 2
        Lowercase letters = 19
        Digits = 4
        Spaces = 6
        Specials = 3
         */

        String str3 = ScannerHelper.getString();
        int dig =0, upCase = 0, loCase = 0, space = 0, special = 0;
        for(int i = 0; i < str3.length(); i++){
            char y = str3.charAt(i);
            if(Character.isLowerCase(y)) loCase++; // count letters as well
            else if(Character.isDigit(y)) dig++;
            else if(Character.isUpperCase(y)) upCase++; // count letters as well
            else if(Character.isWhitespace(y)) space++;
            else special++;
        }
        System.out.println("Letters = " + (upCase + loCase));
        System.out.println("Uppercase letter = " + upCase);
        System.out.println("Lowercase letter = " + loCase);
        System.out.println("Digits = " + dig);
        System.out.println("Spaces = " + space);
        System.out.println("Specials = " + special);


        System.out.println("\n---------------------------Task 5---------------------------\n");
        /*
         Write a program that asks user to enter a String
         Count how many words you have in the String

         "I like Java"
         words = spaces + 1
         3 words
          */
    }
}
