package escape_sequences;

public class Exercise03 {
    public static void main(String[] args) {
        System.out.println("\n------------Task1----------\n");
        System.out.println("Monday\\Tuesday\\Wednesday");

        System.out.println("\n------------Task2----------\n");
        System.out.println("Good \\\\\\morning ");

        System.out.println("\n------------Task3----------\n");
        System.out.println("");
    }
}
