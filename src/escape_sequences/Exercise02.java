package escape_sequences;

public class Exercise02 {
    public static void main(String[] args) {

        System.out.println("\n------------Task1----------\n");
        System.out.println("Steve Balmer replaced Gates as CEO in 2000, and later envisioned a \"devices and services\" strategy.");

        System.out.println("\n------------Task2----------\n");
        System.out.println("I like \"Sunday\" and apple");

        System.out.println("\n------------Task3----------\n");
        System.out.println("My fav fruits are \"Kiwi\" and \"Orange\"");

    }
}
