package mathClass;

public class FindMax {
    public static void main(String[] args) {
        // if two integers
        int num1 = 10;
        int num2 = 15;
        System.out.println(Math.max(num1, num2)); // only if two integers to compare

        // if more integers need to go by pairs and do one at the time
        int num3 = 4, num4 = 14, num5 = 8, num6 = 30;
        int max1 = Math.max(num3, num4);
        int max2 = Math.max(num5, num6);
        int finalMax = Math.max(max1, max2);
        System.out.println(finalMax);



    }
}
