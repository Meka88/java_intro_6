package mathClass;

public class FindAbs {
    public static void main(String[] args) {
        int num = -100;
        // Math.abs converts negative to positive numbers
        System.out.println(Math.abs(num));
        System.out.println(Math.abs(-49));

    }
}
