package mathClass;

import java.util.Scanner;

public class Practice {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("-----------------------Task1-----------------------");

        int randomNum = (int) (Math.random() * 51); // range 0 - 50 included
        System.out.println(randomNum);
        System.out.println("The random number * 5 = " + randomNum * 5);

        System.out.println("-----------------------Task2-----------------------");

        int num1 = (int) (Math.random() * 10 + 1); // gives us a range between 1 - 10 included
        int num2 = (int) (Math.random() * 10 + 1);

        System.out.println("Min number = " + Math.min(num1, num2));
        System.out.println("Max number = " + Math.max(num1, num2));
        System.out.println("Difference = " + Math.abs(num1 - num2));

        System.out.println("-----------------------Task3-----------------------");

        int randomNumber = (int) (Math.random() * 51 + 50);// range of 50 to 100 included

        System.out.println(randomNumber);
        System.out.println("The random number % 10 = " + randomNumber % 10);

        System.out.println("-----------------------Task4-----------------------");

        System.out.println("Please enter 5 numbers between 1 and 10.");

        int userNum1 = input.nextInt() * 5;
        int userNum2 = input.nextInt() * 4;
        int userNum3 = input.nextInt() * 3;
        int userNum4 = input.nextInt() * 2;
        int userNum5 = input.nextInt();


        //int result = (userNum1 * 5) + (userNum2 * 4) + (userNum3 * 3) + (userNum4 * 2) + (userNum5);

        System.out.println("" +(userNum1  + userNum2 + userNum3  + userNum4 + userNum5));

    }
}
