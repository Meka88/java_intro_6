package utilities;

import java.util.Scanner;

public class ScannerHelper {
    // write a method that ask and return a first name
    static Scanner input = new Scanner(System.in);

    public static String getFirstName(){
        System.out.println("Please enter your first name:");
        return input.nextLine();
    }

    public static String getLastName(){
        System.out.println("Please enter your last name:");
        return input.nextLine();
    }

    public static int getAge(){
        System.out.println("Please enter your age:");
        int age = input.nextInt();
        input.nextLine();

        return age;
    }

    public static int getNumber(){
        System.out.println("Please enter a number:");
        int num1 = input.nextInt();
        input.nextLine();
        return num1;
    }

    public static String getString(){
        System.out.println("Please enter a String");
        String str = input.nextLine();

        return str;
    }

    public static String getFavBook(){
        System.out.println("Please enter your favorite book");
        return input.nextLine();
    }

    public static String getFavQuote(){
        System.out.println("Please enter your favorite quote");
        return input.nextLine();
    }

    public static String getAddress(){
        System.out.println("Please enter your address");
        return input.nextLine();
    }

    public static String getFavCountry(){
        System.out.println("Please enter your favorite country");
        return input.nextLine();
    }

    public static String getSentence(){
        System.out.println("Please enter a sentence");
        return input.nextLine();
    }
    public static String getFullName(){
        System.out.println("Please enter your full name");
        return input.nextLine();
    }
}
