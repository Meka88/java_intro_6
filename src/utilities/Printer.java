package utilities;

public class Printer {

    // write a static method that prints Good morning -> printGM
    public static void printGM(){
        System.out.println("Good Morning");
    }

    public static void helloName(String name){
        System.out.println("Hello " + name);
    }

    public void printTechGlobal(){
        System.out.println("TechGlobal");
    }


}
