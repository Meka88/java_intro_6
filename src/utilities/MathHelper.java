package utilities;

public class MathHelper {
    // write a public static method named max() and returns the greatest number of 3 numbers
    public static int getMax(int num1, int num2, int num3){ // method signature = name + arguments
        return Math.max(Math.max(num1, num2), num3);
    }

    public static int sum(int number1, int number2){
        return number1 + number2;
    }

    public static int sum(int a, int b, int c){
        return sum(a, b) + c;
    }

    public static double sum(double a, double b){
        return a + b;
    }

    public static long sum(long a, long b){
        return a + b;
    }

    public static int product(int number3, int number4){
        return number3 * number4;
    }

    public static int square(int number5){
        return number5 * number5;
    }
}
