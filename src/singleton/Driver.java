package singleton;

public class Driver {
    private Driver(){} // private constructor

    public static Driver driver; // one object

    public static Driver getDriver(){ // get method
        if(driver == null) driver = new Driver();
        return driver;
    }
}
