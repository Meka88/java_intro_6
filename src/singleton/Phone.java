package singleton;

public class Phone {

    private Phone(){}

    public static Phone phone;

    public static Phone getPhone(){
        if(phone == null) phone = new Phone();
        return phone;
    }

}
