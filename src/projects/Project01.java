package projects;

public class Project01 {
    public static void main(String[] args) {

        System.out.println("-------------------Task-1-------------------");

        String name = "Meerim"; //
        System.out.println("My name is " + name);

        System.out.println("-------------------Task-2-------------------");

        char nameCharacter1 = 'M';
        char nameCharacter2 = 'e';
        char nameCharacter3 = 'e';
        char nameCharacter4 = 'r';
        char nameCharacter5 = 'i';
        char nameCharacter6 = 'M';

        System.out.println("Name letter 1 is" + " " + nameCharacter1);
        System.out.println("Name letter 2 is" + " " + nameCharacter2);
        System.out.println("Name letter 3 is" + " " + nameCharacter3);
        System.out.println("Name letter 4 is" + " " + nameCharacter4);
        System.out.println("Name letter 5 is" + " " + nameCharacter5);
        System.out.println("Name letter 6 is" + " " + nameCharacter6);

        System.out.println("-------------------Task-3-------------------");

        String myFavMovie, myFavSong, myFavCity, myFavActivity, myFavSnack;
        myFavMovie = "Gone with the Wind";
        myFavSong = "Bohemian Rhapsody";
        myFavCity = "New York";
        myFavActivity = "daydreaming in the park";
        myFavSnack = "roasted sunflower seeds";

        System.out.println("My favorite movie is" + " " + myFavMovie + ".");
        System.out.println("My favorite song is" + " " + myFavSong + ".");
        System.out.println("My favorite city is" + " " + myFavCity + ".");
        System.out.println("My favorite activity is" + " " + myFavActivity + ".");
        System.out.println("My favorite snack is" + " " + myFavSnack + ".");

        System.out.println("-------------------Task-4-------------------");

        int myFavNumber = 7;
        int numberOfStatesIVisited = 7;
        int numberOfCountriesIVisited = 4;

        System.out.println("My favorite number is" + " " + myFavNumber);
        System.out.println("The number of states I visited is" + " " + numberOfStatesIVisited);
        System.out.println("The number of countries I visited is" + " " + numberOfCountriesIVisited);

        System.out.println("-------------------Task-5-------------------");

        boolean amIAtSchoolToday = false;
        System.out.println("I am at school today =" + " " + amIAtSchoolToday);

        System.out.println("-------------------Task-6-------------------");

        boolean isWeatherNiceToday = false;
        System.out.println("Weather is nice today =" + " " + isWeatherNiceToday);
    }
}
