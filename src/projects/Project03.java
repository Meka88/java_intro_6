package projects;

public class Project03 {
    public static void main(String[] args) {
        System.out.println("\n-------------------Task-1-------------------\n");

         /*
        TASK-1
        -Assume that you are given below Strings
        String s1 = “24”, s2 = “5”;
        -Find the sum, subtraction, division, multiplication and remainder of the s1 and s2.
        Output:
        The sum of 24 and 5 = 29
        The subtraction of 24 and 5 = 19
        The division of 24 and 5 = 4.8
        The multiplication of 24 and 5 = 120
        The remainder of 24 and 5 = 4
        */

        String s1 = "24", s2 = "5";
        int b1 = Integer.parseInt(s1);
        int b2 = Integer.parseInt(s2);

        System.out.println("The sum of " + b1 + " and " + b2 + " = " + (b1 + b2));
        System.out.println("The subtraction of " + b1 + " and " + b2 + " = " + (b1 - b2));
        System.out.println("The division of " + b1 + " and " + b2 + " = " + ((double)b1 / (double)b2));
        System.out.println("The multiplication of " + b1 + " and " + b2 + " = " + (b1 * b2));
        System.out.println("The remainder of " + b1 + " and " + b2 + " = " + (b1 % b2));

        System.out.println("-------------------Task-2-------------------");

        /*
        TASK-2
        -Write a program that generates a random number between 1 to 35 (1 and 35 are
        included)
        -Find if the given number is a Prime Number
        -If random number generated is a prime one, then print “{NUMBER} IS A PRIME NUMBER”.
        -Otherwise, print “{NUMBER} IS NOT A PRIME NUMBER”
        Prime numbers - 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31
         */

        int num = (int) (Math.round(Math.random() * 35) + 1);
        System.out.println(num);

        if (num == 2 || num == 3 || num == 5 || num == 7 || num == 11 ||
                num == 17 || num == 19 || num == 23 || num == 29 || num == 31) {
            System.out.println(num + " IS A PRIME NUMBER");
        } else {
            System.out.println(num + " IS NOT A PRIME NUMBER");
        }
//        // option 2
//        int ranNum = 1 + (int)(Math.random() * 35);
//        // if the number is divisible by 2 (unless it is 2), then it is not prime number
//        if (ranNum == 2 || ranNum == 3 || ranNum == 5) {
//            System.out.println(ranNum + " IS A PRIME NUMBER");
//        } else if (ranNum == 1 || ranNum % 2 == 0 || ranNum % 3 == 0 || ranNum % 5 == 0) { // ranNum = 24 { 2 % 2 = 0
//            System.out.println(ranNum + " IS NOT A PRIME NUMBER");
//        } else System.out.println(ranNum + " IS A PRIME NUMBER");

        // option 3 using for loop
//        boolean isPrime = true;
//        for(int i = 2; i <= ranNum; i++) {
//            System.out.println(ranNum + " being divided by " + i);
//            if (ranNum % i == 0) {
//                isPrime = false;
//                break;
//            }
//        }
//
//        if (isPrime) System.out.println(ranNum + " IS A PRIME NUMBER");
//        else System.out.println(ranNum + " IS NOT A PRIME NUMBER");


        System.out.println("-------------------Task-3-------------------");
        /*
        TASK-3
        -Write a program that generates 3 random numbers between 1 to 50 (1 and 50 are
        included)
        -Find and print each number in an ascending order
        -Ascending order is ordering from lowest to greatest
         */
        int ranNum1 = 1 + (int)(Math.random() * 50);
        int ranNum2 = 1 + (int)(Math.random() * 50);
        int ranNum3 = 1 + (int)(Math.random() * 50);

        int lowest = Math.min(Math.min(ranNum1, ranNum2), ranNum3);
        int greatest = Math.max(Math.max(ranNum1, ranNum2), ranNum3);
        int middle = ranNum1 + ranNum2 + ranNum3 - lowest - greatest;

        System.out.println("Lowest number is = " + lowest);
        System.out.println("Middle number is = " + middle);
        System.out.println("Greatest number is = " + greatest);

            // option 2 longest and not very efficient
//        int num1 = (int) (Math.random() * 50 + 1);
//        int num2 = (int) (Math.random() * 50 + 1);
//        int num3 = (int) (Math.random() * 50 + 1);
//        System.out.println(num1 + "," + num2 + "," + num3);
//
//        if(num1 >= num2 && num2 >= num3){
//            System.out.println("Greatest number is = " + num1 + "\nMiddle number is = " + num2 + "\nLowest number is = " + num3);
//        }
//        else if(num2 >= num1 && num1 >= num3){
//            System.out.println("Greatest number is = " + num2 + "\nMiddle number is = " + num1 + "\nLowest number is = " + num3);
//        }
//        else if(num3 >= num1 && num1 >= num2){
//            System.out.println("Greatest number is = " + num3 + "\nMiddle number is = " + num1 + "\nLowest number is = " + num2);
//        }
//        else if(num1 >= num3 && num3 >= num2){
//            System.out.println("Greatest number is = " + num1 + "\nMiddle number is = " + num3 + "\nLowest number is = " + num2);
//        }
//        else if(num2 >= num3 && num3 >= num1){
//            System.out.println("Greatest number is = " + num2 + "\nMiddle number is = " + num3 + "\nLowest number is = " + num1);
//        }
//        else if(num3 >= num2 && num2 >= num1){
//            System.out.println("Greatest number is = " + num3 + "\nMiddle number is = " + num2 + "\nLowest number is = " + num1);
//        }

        System.out.println("-------------------Task-4-------------------");
        /*
        TASK-4 (Find if given char is lowercase or uppercase)
        -Assume you are given a single character. (It will be hard-coded)
        -First, check if given char is a letter but not digit or special character.
        -If the given char is not a letter, then print “Invalid character detected!!!”.
        -If it is a letter, then find out if it is an uppercase or a lowercase letter.
        -If the letter is uppercase, then print “The letter is uppercase”, else print “The
        letter is lowercase”.
        NOTE: You need to use ASCII table and casting for this task
        Char = '5', 'a', 'R'
         */

        char c1 = 'R';

        boolean isLower = (c1 >= 'a' && c1 <= 'z');
        boolean isUpper = (c1 >= 65 && c1 <= 90);

        if (isLower || isUpper) { // character is a letter
            if (isLower) System.out.println("The letter is lowercase"); // the letter lowercase
            else System.out.println("The letter is uppercase"); // the letter is uppercase
        } else System.out.println("Invalid character detected!!!");

//        char c = '6';
//        if((c >= 65 && c <= 90) || (c >= 97 && c <= 122)){
//            if(c >= 65 && c <= 90){
//                System.out.println("The letter is uppercase");
//            } else if(c >= 97 && c <= 122) {
//                System.out.println("The letter is lowercase");
//            }
//        }
//            else {
//            System.out.println("Invalid character detected!!!");
//        }

        System.out.println("-------------------Task-5-------------------");
         /*
        TASK-5 (Find if given char is vowel or consonant)
        -Assume you are given a single character. (It will be hard-coded)
        -First, check if given char is a letter but not digit or special character.
        -If it is not a letter, then print “Invalid character detected!!!”.
        -If it is a letter, then find out whether it is a vowel or a consonant.
        -If the letter is vowel, then print “The letter is vowel”, else print “The letter is
        consonant”.
        -Vowel letters: a, e, i o, u, A, E, I, O, U
        NOTE: You need to use ASCII table and casting for this example
        Char = '#', 'e', 'R'
         */

        char c2 = 'R'; // 65 + 32 = 97 -> 'a'

        boolean isLower2 = (c2 >= 'a' && c2 <= 'z');
        boolean isUpper2 = (c2 >= 65 && c2 <= 90); //  (c2 >= 'A' && c2 <= 'Z')

        if (isLower2 || isUpper2) { // is it a letter?
            if (isUpper2) c2 += 32; // if it's an uppercase letter, then convert to lowercase
            if (c2 == 'a' || c2 == 'e' || c2 == 'i' || c2 == 'o' || c2 == 'u') System.out.println("The letter is vowel");
            else System.out.println("The letter is a consonant");
        } else System.out.println("Invalid character detected!!!");

        // option 2
        //Vowel letters: a, e, i o, u, A, E, I, O, U
//        char b = 'I';
//
//        if((b >= 65 && b <= 90) || (b >= 97 && b <= 122)) {
//            if(b == 65 || b == 69 || b == 73 || b == 79 || b == 85 || b == 97 || b == 101 || b == 105 || b == 111 || b == 117){
//                System.out.println("The letter is vowel");
//            } else {
//                System.out.println("The letter is consonant");
//            }
//        } else{
//            System.out.println("Invalid character detected!!!");
//        }

        System.out.println("-------------------Task-6-------------------");
         /*
        TASK-6 (Find if given char is special character or not)
        -Assume you are given a single character. (It will be hard-coded)
        -First, check if the given char is a special character but not a digit or not a letter.
        -If it is not a special character, then print “Invalid character detected!!!”.
        -If it is a special character, then print “Special character is =
        {givenCharacter}”.
        NOTE: You need to use ASCII table and casting for this example
        Char = '8', '*'
         */
        char c3 = '*';

        boolean isDigit = (c3 >= 48 && c3 <= 57);
        boolean isLetter = (c3 >= 'a' && c3 <= 'z') || (c3 >= 'A' && c3 <= 'Z');

        if (isDigit || isLetter) System.out.println("Invalid character detected!!!");
        else System.out.println("Special character is = " + c3);

        // option 2
//        char a = 'g';
//        if((a >= 0 && a <= 47) || (a >= 58 && a <= 64) || (a >= 91 && a <= 96) || a >= 124 && a <= 127){
//            System.out.println("Special character is = " + a);
//        } else{
//            System.out.println("Invalid character detected!!!");
//        }


        System.out.println("-------------------Task-7-------------------");
        /*
        TASK-7 (Find if given char is a letter or digit or special character)
        -Assume you are given a single character. (It will be hard-coded)
        -If given char is a letter, then print “Character is a letter”
        -If given char is a digit, then print “Character is a digit”
        -Otherwise, print “Character is a special character”
         */
        char c4 = '!';

        boolean isDigit2 = (c4 >= 48 && c4 <= 57);
        boolean isLetter2 = (c4 >= 'a' && c4 <= 'z') || (c4 >= 'A' && c4 <= 'Z');

        if (isDigit2) System.out.println("Character is a digit");
        else if (isLetter2) System.out.println("Character is a letter");
        else System.out.println("Character is a special character");

//        char t = '=';
//        if((t >= 65 && t <= 90) || (t >= 97 && t <= 122)){
//            System.out.println("Character is a letter");
//        } else if(t >= 48 && t <= 57){
//            System.out.println("Character is a digit");
//        } else {
//            System.out.println("Character is a special character");
//        }

    }
}
