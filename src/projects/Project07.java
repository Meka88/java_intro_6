package projects;

import java.util.ArrayList;
import java.util.Arrays;

public class Project07 {
    public static void main(String[] args) {
        System.out.println("\n------------------------Task-1--------------------------\n");

        String[] arr = {"foo", "", " ", "foo bar", "java is fun", " ruby "};
        System.out.println(countMultipleWords(arr));

        System.out.println("\n------------------------Task-2--------------------------\n");

        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(2, -5, 6, 7, -10, -78, 0, 15));
        System.out.println(removeNegatives(list));

        System.out.println("\n------------------------Task-3--------------------------\n");

        String password = "Abcd123!";
        System.out.println(validatePassword(password));

        System.out.println("\n------------------------Task-4--------------------------\n");

        String email = "ab@cd@gm.ail.com";
        System.out.println(validateEmailAddress(email));
    }
    /*
    TASK-1 - countMultipleWords() method
    •Write a method that takes a String[] array as an argument and counts
    how many strings in the array has multiple words.
    •This method will return an int which is the count of elements that have
    multiple words.
    •NOTE: be careful about these as they are not multiple words ->“”,    “   “,
    “    abc”,  “abc   “
    Test data:
    [“foo”, “”, “ “, “foo bar”, “java is fun”, “ ruby ”]
    Expected output:
    2
     */
    public static int countMultipleWords(String[] arr){
        int count = 0;
        for (String s : arr) {
            if(s.trim().contains(" "))
                count++;
        }
        return count;
    }

    /*
    TASK-2 - removeNegatives() method
    •Write a method that takes an “ArrayList<Integer> numbers” as an
    argument and removes all negative numbers from the given list if there
    are any.
    •This method will return an ArrayList with removed negatives.
    Test data 1:
    [2, -5, 6, 7, -10, -78, 0, 15]
    Expected output 1:
    [2, 6, 7, 0, 15]
     */
    public static ArrayList<Integer> removeNegatives(ArrayList<Integer> list){
        list.removeIf(e -> e < 0);
        return list;
    }

    /*
    TASK-3 - validatePassword() method
    •Write a method that takes a “String password” as an argument and
    checks if the given password is valid or not.
    •This method will return true if given password is valid, or false if given
    password is not valid.
    •A VALID PASSWORD:
    -should have length of 8 to 16 (both inclusive).
    -should have at least 1 digit, 1 uppercase, 1 lowercase and 1 special
    char.
    -should NOT have any space.
    Test data 1:
    Expected output 1:
    false
    Test data 2:
    "abcd"
    Expected output 2:
    false
    Test data 3:
    "abcd1234"
    Expected output 3:
    false
    Test data 4:
    "Abcd1234"
    Expected output 4:
    false
    Test data 5:
    "Abcd123!"
    Expected output 5:
    true
     */

    public static boolean validatePassword(String password){
        int digits = 0, upCase = 0, loCase = 0, specChar = 0;
        for (int i = 0; i < password.length(); i++) {
            if(Character.isDigit(password.charAt(i))) digits++;
            else if(Character.isUpperCase(password.charAt(i))) upCase++;
            else if(Character.isLowerCase(password.charAt(i))) loCase++;
            else specChar++;
        }
        return (password.length() >= 8 && password.length() <= 16) &&
                (digits > 0 && upCase > 0 && loCase > 0 && specChar > 0) &&
                !(password.contains(" "));
    }
    /* Second way
    public static boolean validatePassword(String password){

        if (password.length() < 8 || password.length() > 16 || password.contains(" ")){
            return false;
        }else {
            int digit=0;
            int special=0;
            int upCount=0;
            int loCount=0;
            for(int i =0;i<password.length();i++){
                char c = password.charAt(i);
                if(Character.isUpperCase(c)){
                    upCount++;
                }
                else if(Character.isLowerCase(c)){
                    loCount++;
                }
                else if(Character.isDigit(c)){
                    digit++;
                }
                else if(!Character.isLetterOrDigit(c)){
                    special++;
                }
            }
            return special >= 1 && loCount >= 1 && upCount >= 1 && digit >= 1;
    }
}*/

    /*
    TASK-4 - validateEmailAddress() method
    •Write a method that takes a “String email” as an argument and checks if
    the given email is valid or not.
    •This method will return true if given email is true, or false if given email is
    not valid.
    •A VALID EMAIL:
    -should NOT have any space.
    -should not have more than one “@” character.
    -should be in the given format <2+chars>@<2+chars>.<2+chars>
    Test data 1:
    a@gmail.com
    Expected output 1:
    false
    Test data 2:
    abc@g.com
    Expected output 2:
    false
    Test data 3:
    abc@gmail.c
    Expected output 3:
    false
    Test data 4:
    abc@@gmail.com
    Expected output 4:
    false
    Test data 5:
    abcd@gmail.com
    Expected output 5:
    true
     */
    
    public static boolean validateEmailAddress(String email){
        int count = 0;
        for (int i = email.indexOf("@") + 1; i < email.length(); i++) {
           if(email.charAt(i) == '.') count++;
        }
        return (count == 1 && ((email.indexOf("@") == email.lastIndexOf("@"))
                && email.contains("@")) && email.lastIndexOf("@") >= 2 &&
                (email.lastIndexOf(".") - email.lastIndexOf("@")) >= 2 && (email.length() - email.lastIndexOf(".")) >= 2);
    }

    /* second way
    public static boolean validateEmailAddress(String email){
        int countAtSign = 0;
        int countDot = 0;
        if(email.contains(" ")){
            return false;
        }
        else if(!email.contains("@")){
            return false;
        }
        else if(!email.contains(".")){
            return false;
        }
        for(int i = 0; i < email.length(); i++){
            if(email.charAt(i) == 64){
                countAtSign++;
            }
            else if(email.charAt(i) == 46){
                countDot++;
            }
        }
        if(countAtSign == 0 || countAtSign > 1){
            return false;
        }
        if(countDot == 0 || countDot > 1){
            return false;
        }
        String[] arr1 = email.split("\\.", 2);
        String[] arr2 = arr1[0].split("@" , 2);

        if(arr1[0].length() >= 2 && arr1[1].length() >= 2 && arr2[0].length() >= 2 && arr2[1].length() >= 2){
            return true;
        }
        else{
            return false;
        }
    } */
}
