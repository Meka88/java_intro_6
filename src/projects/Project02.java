package projects;


import java.util.Scanner;

public class Project02 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("---------------------TASK1---------------------");

        System.out.println("Please enter three numbers: ");
        int userNum1 = input.nextInt();
        int userNum2 = input.nextInt();
        int userNum3 = input.nextInt();

        System.out.println("The product of the numbers entered is = "  + (userNum1 * userNum2 * userNum3));

        System.out.println("---------------------TASK2---------------------");

        System.out.println("Please enter your name:");
        String firstName = input.next();
        input.nextLine();

        System.out.println("Please enter your last name:");
        String lastName = input.next();

        System.out.println("Please enter your year of birth:");
        int userAgeYear = input.nextInt();

        input.nextLine();

        System.out.println(firstName + " " + lastName + "'s age is = " + (2023 - userAgeYear) + ".");

        System.out.println("---------------------TASK3---------------------");

        System.out.println("Please enter your full name:");
        String fullName = input.nextLine();


        System.out.println("What is your weight in kg?");
        double weightKg = input.nextDouble();
        input.nextLine();

        System.out.println(fullName + "'s weight is = " + (weightKg * 2.205) + " lbs.");

        System.out.println("---------------------TASK4---------------------");

        System.out.println("What is your full name?");
        String student1FullName = input.nextLine();
        System.out.println("What is your age?");
        int student1Age = input.nextInt();
        input.nextLine();

        System.out.println("What is your full name?");
        String student2FullName = input.nextLine();
        System.out.println("What is your age?");
        int student2Age = input.nextInt();
        input.nextLine();

        System.out.println("What is your full name?");
        String student3FullName = input.nextLine();
        System.out.println("What is your age?");
        int student3Age = input.nextInt();
        input.nextLine();

        System.out.println(student1FullName + "'s age is " + student1Age + ".");
        System.out.println(student2FullName + "'s age is " + student2Age + ".");
        System.out.println(student3FullName + "'s age is " + student3Age + ".");

        System.out.println("The average age is " + ((student1Age + student2Age + student3Age) / 3) + ".");
        System.out.println("The eldest age is " + (Math.max(Math.max(student1Age, student2Age), student3Age)) + ".");
        System.out.println("The youngest age is " + (Math.min(Math.min(student1Age, student2Age), student3Age)) + ".");
    }
}
