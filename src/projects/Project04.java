package projects;

import utilities.ScannerHelper;

public class Project04 {
    public static void main(String[] args) {
        System.out.println("\n-------------------Task-1-------------------\n");
        /*
        Write a program that asks user to enter a String, and swaps first and last 4
        characters of this String and print the modified String
        NOTE: Write a program that handles any String
        NOTE: First check if the length of String is at least 8, if the String’s length is
        less than 8, then print message “This String does not have 8 characters”
        Test data 1:
        Hello
        Expected output 1:
        This String does not have 8 characters
        Test data 2:
        TechGlobal
        Expected output 2:
        obalGlTech
        Test data 3:
        Computer Science
        Expected output 3:
        enceuter SciComp
         */

        String str1 = ScannerHelper.getString();
        int middle = str1.length()/2;
        if(str1.length() < 8)  System.out.println("This String does not have 8 characters");
        else System.out.println(str1.substring(str1.length() - 4) + str1.substring(middle - 1, middle + 1) + str1.substring(0, 4));


        System.out.println("\n-------------------Task-2-------------------\n");
        /*
        Write a program that asks user to enter a sentence, and swaps first and last
        words of this sentence and print the modified sentence
        NOTE: Write a program that handles any sentence
        NOTE: First check if the sentence has at least 2 words, if the sentence has
        only one or no word, then print message “This sentence does not have 2 or
        more words to swap”
        Test data 1:
        TechGlobal
        Expected output 1:
        This sentence does not have 2 or more words to swap
        Test data 2:
        TechGlobal School
        Expected output 2:
        School TechGlobal
        Test data 3:
        Java is a programming language
        Expected output 3:
        language is a programming Java
         */

        String str2 = ScannerHelper.getSentence();
        int spaceFirst = str2.indexOf(" ");
        int spaceLast = str2.lastIndexOf(" ");
        System.out.println(spaceFirst);
        System.out.println(spaceLast);
        if(spaceFirst >= 1) System.out.println(str2.substring(spaceLast + 1, str2.length()) + " " + str2.substring(spaceFirst + 1, spaceLast) + " " + str2.substring(0, spaceFirst));
        else System.out.println("This sentence does not have 2 or more words to swap");



        System.out.println("\n-------------------Task-3-------------------\n");
        /*
        Assume that you are given some Strings as below, and you want to replace
        bad words with good words.
        NOTE: Write a program that handles any String
        String str1 = “These books are so stupid”;
        String str2 = “I like idiot behaviors”;
        String str3 = “I had some stupid t-shirts in the past and also some idiot
        look shoes”;
        Write a Java program that replaces bad words like “stupid” and “idiot” with
        “nice” keyword.

        Expected output:
        These books are so nice
        I like nice behaviors
        I had some nice t-shirts in the past and also some nice look shoes
         */

        String str3 = "These books are so stupid";
        String str4 = "I like idiot behaviors";
        String str5 = "I had some stupid t-shirts in the past and also some idiot look shoes";

        System.out.println(str3.replace("stupid", "nice"));
        System.out.println(str4.replace("idiot", "nice"));
        System.out.println(str5.replace("stupid", "nice").replace("idiot", "nice"));


        System.out.println("\n-------------------Task-4-------------------\n");
        /*
        Write a program that asks user to enter their name.
        First check if length of name is more than 2 and is even or odd
        If length of name is less than 2, then print “Invalid input!!!”
        If length of name is odd, then print middle character from the name
        If length of name is even, then print middle 2 characters from the name
        Test data:
        J
        Expected output:
        Invalid input!!!
        Test data:
        Lionel
        Expected output:
        on
        Test data:
        James
        Expected output:
        m
         */

        String name = ScannerHelper.getFirstName();
        int mid = name.length()/2;
        if(name.length() < 2) System.out.println("Invalid input!!!");
        else{
            if(name.length() % 2 != 0){
                System.out.println(name.substring(mid, mid + 1));
            } else{
                System.out.println(name.substring(mid - 1, mid + 1));
            }
        }


        System.out.println("\n-------------------Task-5-------------------\n");
        /*
        Write a program that asks user to enter a country.
        First check if length of country is more than 5. If not, then print “Invalid
        input!!!”
        If length of country is more than 5, then print country name without first 2
        and last 2 characters
        Test data:
        Peru
        Expected output:
        Invalid input!!!
        Test data:
        Brazil
        Expected output:
        az
         */

        String country = ScannerHelper.getFavCountry();
        if(country.length() < 5) System.out.println("Invalid input!!!");
        else System.out.println(country.substring(2, country.length() - 2));

        System.out.println("\n-------------------Task-6-------------------\n");
        /*
        Write a program that asks user to enter their full address.
        Replace all letter ‘a’ or ‘A’ with ‘*’
        Replace all letter ‘e’ or ‘E’ with ‘#’
        Replace all letter ‘i’ or ‘I’ with ‘+’
        Replace all letter ‘u’ or ‘U’ with ‘$’
        Replace all letter ‘o’ or ‘O’ with ‘@’
        Then, print result after replacements
        Test data:
        2860 S River Rd Suite 350, Des Plaines IL 60018
        Expected output:
        2860 S R+v#r Rd S$+t# 350, D#s Pl*+n#s +L 60018
         */

        String address = ScannerHelper.getAddress();
        address = address.toLowerCase();
        System.out.println(address.replace("a", "*").replace("e", "#").replace("i", "+").replace("u","$").replace("o", "@"));

        System.out.println("\n-------------------Task-7-------------------\n");
        /*
        Write a program that asks user to enter a sentence as a String, and count
        how many words that sentence has, and print it with given below message.
        NOTE: Write a program that handles any String
        NOTE: First check if the given sentence has 2 words at least. If not, then just
        print “This sentence does not have multiple words”.
        Test data 1:
        Java is fun
        Expected output 1:
        This sentence has 3 words.
        Test data 2:
        Hello World
        Expected output 2:
        This sentence has 2 words.
        Test data 3:
        Hi
        Expected output 3:
        This sentence does not have multiple words
         */
        String sentence = "Java is fun"; // the given string

        // using split() method to split the string into words
        String[] words = sentence.split(" ");

        // getting the length of the array to find the number of words
        int wordCount = words.length;

        // displaying the result
        System.out.println("The total number of words in the string is: " + wordCount);


//        String sentence = ScannerHelper.getSentence();
//        if(sentence == null || sentence.isEmpty()) {
//            System.out.println(0);
//        } else{
//            String[] words = sentence.split("\\s+");
//            System.out.println(words.length);
//        }
    }
}
