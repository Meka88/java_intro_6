package projects;

import utilities.ScannerHelper;

public class Project05 {
    public static void main(String[] args) {
        System.out.println("\n-------------------Task-1-------------------\n");
        /*
        Write a program that asks user to enter a sentence as a String, and count how many words that sentence has, and print it with given below message.
        NOTE: Write a program that handles any String
        NOTE: First check if the given sentence has 2 words at least. If not, then just print “This sentence does not have multiple words”.

        Test data 1:
        Hi

        Expected output 1:
        This sentence does not have multiple words.

        Test data 2:
        Java is fun

        Expected output 2:
        This sentence has 3 words.


        Test data 3:
        Hello World

        Expected output 3:
        This sentence has 2 words.
         */

        String str = ScannerHelper.getString();
        int space = 0;

        for (int i = 0; i < str.length(); i++) {
            char x = str.charAt(i);
            if (Character.isWhitespace(x)) space++;
        }
        if (space == 0) System.out.println("This sentence does not have multiple words");
        else System.out.println("This sentence has " + (space + 1) + " words.");


        System.out.println("\n-------------------Task-2-------------------\n");
        /*
        Write a program that generates 2 random numbers between 0 and 25 (0 and
        25 are included), Then print all numbers between 2 random numbers that
        cannot be divided by 5 in ascending order.
        Test data 1:
        int randomNumber1 = 12;
        int randomNumber1 = 4;
        Expected output 1:
        4 – 6 – 7 – 8 – 9 – 11 – 12
        Test data 2:
        int randomNumber1 = 9;
        int randomNumber1 = 17;
        Expected output 2:
        9 – 11 – 12 – 13 – 14 – 16 - 17
         */

        int num1 = (int) (Math.random() * 26);
        int num2 = (int) (Math.random() * 26);
        System.out.println(num1 + " - " + num2); // just to see random numbers

        for (int i = Math.min(num1, num2); i <= Math.max(num1, num2); i++) {
            if (i % 5 != 0) System.out.print(i + " - ");
            ;
        }

        System.out.println("\n-------------------Task-3-------------------\n");
        /*
        Write a program that asks user to enter a sentence as a String and count
        how many a or A letters that sentence has and print it with given below
        message.
        NOTE: Write a program that handles any String
        NOTE: First check if the given sentence has 1 character at least. If not, then
        just print “This sentence does not have any characters”.
        Test data 1:
        Expected output 1:
        This sentence does not have any characters.
        Test data 2:
        Apple is one of the largest IT companies.
        Expected output 2:
        This sentence has 3 a or A letters.
         */

        String str1 = ScannerHelper.getString();
        int count = 0;

        for (int i = 0; i < str1.length(); i++) {
            if (str1.charAt(i) == 'A' || str1.charAt(i) == 'a') {
                count++;
            }
        }
        if (str1.isEmpty()) System.out.println("This sentence does not have any characters");
        else System.out.println("This sentence has " + count + " a or A letters.");


        System.out.println("\n-------------------Task-4-------------------\n");
        /*
        Write a program that asks user to enter a word, and check if it is palindrome
        or not.
        Palindrome: It is a word that is read the same backward as forward
        •EX/ kayak, civic, madam
        NOTE: Write a program that handles any String
        NOTE: First check if the given String has at least 1 character, if the String
        does not have at least one character, then print message “This word does
        not have 1 or more characters”
        Test data 1:
        T
        Expected output 1:
        This word is palindrome
        Test data 2:
        civic
        Expected output 2:
        This word is palindrome
        Test data 3:
        Madam
        Expected output 3:
        This word is not palindrome
        Test data 4:
        Hello
        Expected output 4:
        This word is not palindrome
        Test data 5:
        Expected output 5:
        This word does not have 1 or more characters
         */
        String str3 = ScannerHelper.getString();

        String reverse = "";

        for (int i = 0; i < str3.length(); i++) {
            reverse = str3.charAt(i) + reverse;
        }
        if (str3.isEmpty()) System.out.println("This word does not have 1 or more characters");
        else if (str3.equals(reverse)) System.out.println("This word is palindrome");
        else System.out.println("This word is not palindrome");


        System.out.println("\n-------------------Task-5-------------------\n");
        /*
        Write a program that prints the below shape.
                        *
                      * * *
                    * * * * *
                  * * * * * * *
                * * * * * * * * *
              * * * * * * * * * * *
            * * * * * * * * * * * * *
          * * * * * * * * * * * * * * *
        * * * * * * * * * * * * * * * * *

         */
//        int rows = 9;
//        for (int i = 1; i <= rows; i++) {
//
//            for (int j = rows; j >= i; j--) {
//                System.out.print( "  ");
//            }
//            for (int m = 1; m <= i; m++) {
//                System.out.print(  "* ");
//            }
//            for (int z= 1; z <= i - 1; z++) {
//                System.out.print("* ");
//            }
//
//            System.out.println();

        for (int i = 1; i <= 9; i++) {
            for (int j = 1; j <= 9 - i; j++)
                System.out.print("  ");

            for (int j = 1; j <= i * 2 - 1; j++) {
                System.out.print("* ");
            }
            System.out.println();
        }
    }
}