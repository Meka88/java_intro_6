package projects;

import java.util.Arrays;

public class Project06 {
    public static void main(String[] args) {
        System.out.println("\n-------------------Task-1-------------------\n");
        /*
        TASK-1 - findGreatestAndSmallestWithSort() method
        Write a method that takes an int array that has at least one element. Find
        the greatest and smallest elements from the array and print them.
        Complete task using sort() method.
        Test data:
        [10, 7, 7, 10, -3, 10, -3]
        Expected output:
        Smallest = -3
        Greatest = 10
         */
        int [] numbers = {10, 7, 7, 10, -3, 10, -3};
        findGreatestAndSmallestWithSort(numbers);

        System.out.println("\n-------------------Task-2-------------------\n");
        /*
        TASK-2 findGreatestAndSmallest() method
        Write a method that takes an int array that has at least one element. Find
        the greatest and smallest elements from the array and print them. DO NOT
        sort the array and complete task without sorting.
        Test data:
        [10, 7, 7, 10 -3, 10, -3]
        Expected output:
        Smallest = -3
        Greatest = 10
         */
        int[] arr1 = {10, 7, 7, 10 -3, 10, -3};
        findGreatestAndSmallest(arr1);

        System.out.println("\n-------------------Task-3-------------------\n");
        /*
        TASK-3 - findSecondGreatestAndSmallestWithSort() method
        Write a method that takes an int array that has at least one element. Find
        the second greatest and second-smallest elements from the array and print
        them. Complete task using sort() method.
        Test data:
        [10, 5, 6, 7, 8, 5, 15, 15]
        Expected output:
        Second Smallest = 6
        Second Greatest = 10
         */
        int[] arr2 = {10, 5, 6, 7, 8, 5, 15, 15};
        findSecondGreatestAndSmallestWithSort(arr2);

        System.out.println("\n-------------------Task-4-------------------\n");
        /*
        TASK-4 - findSecondGreatestAndSmallest() method
        Write a method that takes an int array that has at least one element. Find
        the second greatest and second-smallest elements from the array and print
        them. DO NOT sort the array and complete task without sorting.
        Test data:
        [10, 5, 6, 7, 8, 5, 15, 15]
        Expected output:
        Second Smallest = 6
        Second Greatest = 10
         */
        int[] arr3 = {10, 5, 6, 7, 8, 5, 15, 15};
        findSecondGreatestAndSmallest(arr3);

        System.out.println("\n-------------------Task-5-------------------\n");
        /*
        TASK-5 - findDuplicatedElementsInAnArray() method
        Write a method that takes a String array. Find all duplicated elements and
        print them.
        NOTE: It is case-sensitive!
        Test data:
        [“foo”, “bar”, “Foo”, “bar”, “6”, “abc”, “6”, “xyz”]
        Expected output:
        bar
        6
         */
        String[] arr = {"foo", "bar", "Foo", "bar", "6", "abc", "6", "xyz"};
        findDuplicatedElementsInAnArray(arr);

        System.out.println("\n-------------------Task-6-------------------\n");
        /*
        TASK-6 findMostRepeatedElementInAnArray() method
        Write a method that takes a String array. Find the most repeated element
        and print it.
        Test data:
        [“pen”, “eraser”, “pencil”, “pen”, “123”, “abc”, “pen”, “eraser”]
        Expected output:
        pen
         */

        String[] words = {"pen", "eraser", "pencil", "pen", "123", "abc", "pen", "eraser"};
        findMostRepeatedElementInAnArray(words);
    }

    // TASK 1
    public static void findGreatestAndSmallestWithSort(int[] numbers){
        if (numbers.length >= 1){
            Arrays.sort(numbers);
            System.out.println("Smallest = " + numbers[0]);
            System.out.println("Greatest = " + numbers[numbers.length-1]);
        } else System.out.println("Array is empty!");

    }

    // TASK 2
    public static void findGreatestAndSmallest(int[] arr1){
        if(arr1.length >= 1){
            int min = 0;
            int max = 0;
            for (int i = 1; i < arr1.length; i++) {

                if (arr1[i] < min) {
                    min = arr1[i];
                }
                if (arr1[i] > max) {
                    max = arr1[i];
                }
            }
            System.out.println("Smallest = " + min);
            System.out.println("Greatest = " + max);
        } else System.out.println("Array is empty!");
    }

    // TASK 3
    public static void findSecondGreatestAndSmallestWithSort(int[] arr2) {

        Arrays.sort(arr2);
        int min = arr2[0], max = arr2[arr2.length - 1];
        int secondMin = Integer.MAX_VALUE, secondMax = Integer.MIN_VALUE;

        for (int j : arr2) {
            if (j != max && j > secondMax) secondMax = j;
            if (j != min && j < secondMin) secondMin = j;
        }
        System.out.println("Second Smallest = " + secondMin);
        System.out.println("Second Greatest = " + secondMax);
    }


    // TASK 4
    public static void findSecondGreatestAndSmallest(int[] arr3) {
        int min = arr3[0], max = arr3[0];
        for (int k : arr3) {
            if (k > max) max = k;
            if (k < min) min = k;
        }
        int secondMax = Integer.MIN_VALUE, secondMin = Integer.MAX_VALUE;
        for (int j : arr3) {
            if (j != max && j > secondMax) secondMax = j;
            if (j != min && j < secondMin) secondMin = j;
        }
        System.out.println("Second Smallest = " + secondMin);
        System.out.println("Second Greatest = " + secondMax);
    }
    // TASK 5
    public static void findDuplicatedElementsInAnArray(String[] arr) {
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if(arr[i].equals(arr[j]) && (i != j)){
                    System.out.println(arr[i]);
                }
            }
        }

    }
    // TASK 6
    public static void findMostRepeatedElementInAnArray (String[] words){
        String repeat = "";
        int counter = 0;
        int newCounter = 0;
        for (int i = 0; i < words.length - 1; i++) {
            for (int j = i + 1; j < words.length; j++) {
                if (words[i].equals(words[j])) {
                    newCounter++;
                    if (newCounter > counter) {
                        repeat = words[i];
                        counter = newCounter;
                    }
                }
            }
            newCounter = 0;
        }
        System.out.println(repeat);
    }
}
