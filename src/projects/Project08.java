package projects;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Project08 {
    public static void main(String[] args) {
        System.out.println("\n------------------------Task-1--------------------------\n");

        int[] arr = {4, 8, 7, 15};
        System.out.println(findClosestDistance(arr));

        System.out.println("\n------------------------Task-2--------------------------\n");

        int[] arr1 = {5, 3, -1, 3, 5, 7, -1};
        System.out.println(findSingleNumber(arr1));

        System.out.println("\n------------------------Task-3--------------------------\n");

        System.out.println(findFirstUniqueCharacter("abc abc d"));

        System.out.println("\n------------------------Task-4--------------------------\n");

        int[] arr2 = {4, 7, 8, 6};
        System.out.println(findMissingNumber(arr2));
    }
    /*
    TASK-1 - findClosestDistance() method
    •Write a method that takes an int[] array as an argument and returns the
    closest difference between the numbers.
    •This method will return an int which is the closest difference between 2
    elements in the array
    •NOTE: if array does not have at least 2 elements, then return -1.
    Test data1:
    [4]
    Expected output:
    -1
    The reason the result is -1 for above example is because the array length is
    less than 2, and we return -1 in this case.
    Test data2:
    [4, 8, 7, 15]
    Expected output:
    1
    The reason the result is 1 for above example is that 8 and 7 are the closest
    elements in the array and the difference between them is 1.
    Test data2:
    [10, -5, 20, 50, 100]
    Expected output:
    10
    The reason the result is 1 for above example is that 10 and 20 are
    the closest elements in the array and the difference between them is 10.
     */

    public static int findClosestDistance(int[] arr){
        int result = Integer.MAX_VALUE;

        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = i+1; j < arr.length; j++) {
                if(Math.abs((arr[i] - arr[j])) < result){
                    result = Math.abs((arr[i] - arr[j]));
                }
            }
        }
        return result;
    }

    /*
    TASK-2 – findSingleNumber() method
    Write a method that takes an int[] array as an argument and returns the
    element occurs only once.
    You will be given a non-empty array in which all the elements appear at
    least twice except for one.
    Test data 1:
    [2]
    Expected output 1:
    2
    Test data 2:
    [5, 3, -1, 3, 5, 7, -1]
    Expected output 2:
    7
     */

    public static int findSingleNumber(int[] arr){
        for (int i = 0; i < arr.length; i++) {
            int count = 0;
            for (int j = 0; j < arr.length; j++) {
                if(arr[i] == arr[j]) count++;
            }
            if(count == 1) return arr[i];
        }
        return -1;
    }

    /*
    TASK-3 - findFirstUniqueCharacter() method
    Write a method that takes a non-empty String as an argument and
    returns the first unique character in the String.
    This method returns a char.
    If there is no unique character in the String, then return empty String.
    This task is case-sensitive.
    Test data 1:
    Hello
    Expected output 1:
    H
    Test data 2:
    abc abc d
    Expected output 2:
    d
    Test data 3:
    abab
    Expected output 3:
     */

    public static char findFirstUniqueCharacter(String str){
        char unique = ' ';
        for (int i = 0; i < str.length(); i++) {
            if(str.indexOf(str.charAt(i), str.indexOf(str.charAt(i)) + 1) == -1){
                unique = str.charAt(i);
                break;
            }
        }
        return unique;
    }

    /*
    TASK-4 – findMissingNumber() method
    Write a method that takes an int[] array as an argument and returns the
    missing element.
    You will be given an array length of 2 at least.
    Array elements will be representing a sequence of numbers that increases
    by 1.
    Assume, there will always be a missing number from given array.
    Test data 1:
    [2, 4]
    Expected output 1:
    3
    Test data 2:
    [2, 3, 1, 5]
    Expected output 2:
    4
    Test data 3:
    [4, 7, 8, 6]
    Expected output 2:
    5
     */
    public static int findMissingNumber(int[] arr2){
        /*int num = arr2.length;
        int sum = (num*(num + 1)) /2;
        int sum2 = 0;
        for (int i = 0; i < arr2.length; i++) sum2 += arr2[i];
        return sum - sum2;*/
        /*int total = 1;
        for (int i = 2; i < arr2.length + 1; i++) {
            total += i;
            total -= arr2[i - 2];
        }
        return total;*/
//        int total = (arr2.length + 1) * (arr2.length+2) / 2;
//        for (int i = 0; i < arr2.length; i++) {
//            total -= arr2[i];
//        }
//        return total;

        Arrays.sort(arr2);
        int missingNo = 0;
        for(int i = 0; i < arr2.length; i++ ){
            if(arr2[i + 1] - arr2[i] > 1 ){
                missingNo = arr2[i] + 1;
                break;
            }
        }
        return missingNo;
    }
}
