package conditional_statements;
import java.util.Scanner;

public class IfElseSyntax {
    public static void main(String[] args) {

        boolean condition = true;

        if(condition) {
            //This is if block and this will execute when condition is true
            System.out.println("A");
        } else {
            //This is else block and this will execute when condition is false
            System.out.println("B");
        }
        System.out.println("End of the program!");


    }
}
