package conditional_statements;

import java.util.Scanner;

public class SwitchPractice {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Please enter a number from 1 to 12");
        int num1 = input.nextInt();

        if(num1 == 1){
            System.out.println("Jan");
        } else if(num1 == 2){
            System.out.println("Feb");
        } else if(num1 == 3) {
            System.out.println("Mar");
        } else if(num1 == 4) {
            System.out.println("Apr");
        } else if(num1 == 5) {
            System.out.println("May");
        } else if(num1 == 6) {
            System.out.println("Jun");
        } else {
            System.out.println("This is not a number from 1 to 6");
        }


        switch (num1) {
            case 1:
                System.out.println("January");
                break;
            case 2:
                System.out.println("February");
                break;
            case 3:
                System.out.println("March");
                break;
            case 4:
                System.out.println("April");
                break;
            case 5:
                System.out.println("May");
                break;
            case 6:
                System.out.println("June");
                break;
            case 7:
                System.out.println("July");
                break;
            case 8:
                System.out.println("August");
                break;
            case 9:
                System.out.println("September");
                break;
            case 10:
                System.out.println("October");
                break;
            case 11:
                System.out.println("November");
                break;
            case 12:
                System.out.println("December");
                break;
            default:
                System.out.println("Invalid number");
        }

    }
}
