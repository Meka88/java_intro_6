package conditional_statements;

public class Exercise07_DaysOfTheWeek {
    public static void main(String[] args) {

        /*
        Write a program that generates random numbers between 0 and 6
        Based on the number generated, it will print out the day
        0 -> Sunday
        1 -> Monday
        2 -> Tuesday
        3 -> Wednesday
        4 -> Thursday
        5 -> Friday
        6 -> Saturday
         */

        int randomNum = (int) (Math.random() * 7);

        System.out.println(randomNum);

        // if-else statement
        if(randomNum == 0) System.out.println("Sunday");
        else if(randomNum == 1) System.out.println("Monday");
        else if(randomNum == 2) System.out.println("Tuesday");
        else if(randomNum == 3) System.out.println("Wednesday");
        else if(randomNum == 4) System.out.println("Thursday");
        else if(randomNum == 5) System.out.println("Friday");
        else System.out.println("Saturday");

        // switch statement method
        switch(randomNum) {
            case 0:
                System.out.println("Sunday");
                break;
            case 1:
                System.out.println("Monday");
                break;
            case 2:
                System.out.println("Tuesday");
                break;
            case 3:
                System.out.println("Wednesday");
                break;
            case 4:
                System.out.println("Thursday");
                break;
            case 5:
                System.out.println("Friday");
                break;
            case 6:
                System.out.println("Saturday");
        }

    }
}
