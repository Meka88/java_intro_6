package conditional_statements;

public class Exercise06_CheckAll10 {
    public static void main(String[] args) {
        int randomNum1 = (int) (Math.random() * 2) + 10;
        int randomNum2 = (int) (Math.random() * 2) + 10;

        System.out.println(randomNum1 == 10 && randomNum2 == 10);// 1st option
//        if(randomNum1 != 10 || randomNum2 != 10){ // if else option
//            System.out.println("False");
//        }else{
//            System.out.println("True");
//        }

    }
}
