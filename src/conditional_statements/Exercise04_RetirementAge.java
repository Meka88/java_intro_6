package conditional_statements;

import java.util.Scanner;

public class Exercise04_RetirementAge {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Please enter your age?");
        int age = input.nextInt();

        if(age >= 55) {
            System.out.println("It is your time to get retired!");
        } else {
            System.out.println("You have " + (55 - age) + " years to be retired");
        }
    }

}
