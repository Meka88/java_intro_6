package methods;

import utilities.ScannerHelper;

public class CalculateAge {
    public static void main(String[] args) {
        int age = ScannerHelper.getAge();
        int num1 = ScannerHelper.getNumber();

        System.out.println("Age will be " + (age + num1) + " after " + num1 + " years.");
    }
}
