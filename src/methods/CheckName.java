package methods;

import utilities.ScannerHelper;

public class CheckName {
    public static void main(String[] args) {
        // I can invoke static method with class name
        // I can call non-static methods with objects

//        ScannerHelper = new ScannerHelper();
//
//        String name = scannerHelper.getFirstName();
//        System.out.println("The name entered by user = " + name);
//
//        String lastName = scannerHelper.getLastName();
//        System.out.println("The users last name = " + lastName);

        String firstName = ScannerHelper.getFirstName();
        String lastName = ScannerHelper.getFirstName();

        System.out.println("The users full name is " + firstName + " " + lastName);


    }

}
