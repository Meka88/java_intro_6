package methods;

import utilities.MathHelper;
import utilities.ScannerHelper;

public class FindMaxMin {
    public static void main(String[] args) {
        int num1 = ScannerHelper.getNumber();
        int num2 = ScannerHelper.getNumber();
        int num3 = ScannerHelper.getNumber();

        System.out.println("The max = " + MathHelper.getMax(num1, num2, num3));
        // one line option below
        //System.out.println("The max = " + MathHelper.getMax(ScannerHelper.getNumber(), ScannerHelper.getNumber(), ScannerHelper.getNumber()));

        //Write a public static method named sum() and returns the sum of 2 numbers



        //Write a public static method named product() and returns the product of 2 numbers


        //Write a public static method named square() and returns the square of a number
    }
}
